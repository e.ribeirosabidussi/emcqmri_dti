import numpy as np
import pickle
import os
import nibabel as nii

from EMCqMRI.core.utilities import image_utilities

def get_z_index(data):
    index = np.nonzero(data)
    return index[-1][0]


if __name__ == '__main__':

    # file_path = './HCP/data.nii'

    # data_ = nii.load(file_path)
    # data_fi = np.array(data_.dataobj)

    # print(np.shape(data_fi))

    # transp_data = np.moveaxis(data_fi, -1, 0)[:69,...,50]
    # image_utilities.imagebrowse_slider(transp_data)

    # dest_path = './HCP/2d/data_2d.nii'

    # nii_data = nii.Nifti1Image(transp_data, affine=np.eye(4))
    # try:
    #     nii.save(nii_data, dest_path)
    # except:
    #     print("Couldn't save file")


    path = "./training/9_mri_sim_py/neuroRIM_noBackground_correct_bvec_rotation/"
    dest_path = "./training/9_mri_sim_py/neuroRIM_noBackground_correct_bvec_rotation_2d/"


    img_files = [f for f in os.listdir(path) if f.endswith(".pkl")]

    for file_ in img_files:
        try:
            with open(path+file_, 'rb') as f:
                data = pickle.load(f)
                index = get_z_index(data)
                data_2d = data[...,index]

            with open(dest_path+file_, 'wb') as f:
                pickle.dump(data_2d, f)
                print("Saved file {} to: {}".format(file_, dest_path))
        except :
            print("File {} couldn't be opened".format(file_))


