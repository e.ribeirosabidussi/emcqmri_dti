import os
import sys

sys.path.insert(0, "../..")

from custom_signal_model import dti_model
from custom_likelihood_model import rician_model
from utilities import all_utils
from custom_inference_model import mrtrix_dti
from custom_dataset.utilities import getGradientDirectionsBvalue
from EMCqMRI.core.utilities import image_utilities

from dipy.reconst.dti import fractional_anisotropy, color_fa, mean_diffusivity
import torch
import numpy as np

import matplotlib.pyplot as plt

import pickle

dti_model_obj = dti_model.DTI([])
# dti_deriv_fun = all_utils.LogFun([])
rician_model_fun = rician_model.Rician([])
# mrtrix_model = mrtrix_dti.MRTrix([])




def get_fa(constant=True, val=None, range=[], step=[]):
    if constant:
        return val
    else:
        pass

def get_md(constant=True, val=None, range=[], step=[]):
    if constant:
        return val
    else:
        pass


def get_diffusion_tensor_from_fa_md(fa, md, eigen_vals):

    a = fa / torch.sqrt(3-2 * fa * fa)
    dr = md * (1.0 - a)
    dp = md * (1.0 + 2.0 * a)

    tensor = torch.zeros(3, 3, 10, 10)
    
    # print(((dp - dr) * eigen_vals[0]* eigen_vals[0]).shape, dp.shape, eigen_vals[0].shape)
    tensor[0][0] = dr + (dp - dr) * eigen_vals[0] * eigen_vals[0]
    tensor[1][1] = dr + (dp - dr) * eigen_vals[1] * eigen_vals[1]
    tensor[2][2] = dr + (dp - dr) * eigen_vals[2] * eigen_vals[2]
    tensor[0][1] = tensor[1][0] = (dp - dr) * eigen_vals[0] * eigen_vals[1]
    tensor[0][2] = tensor[2][0] = (dp - dr) * eigen_vals[0] * eigen_vals[2]
    tensor[1][2] = tensor[2][1] = (dp - dr) * eigen_vals[1] * eigen_vals[2]

    return tensor


def get_diffusion_signal(tensor, s0, bvecs, bvals):
    dti_model_obj.b_vecs = bvecs
    dti_model_obj.b_vals = bvals
    theta = [s0, tensor[0][0], tensor[0][1], tensor[0][2], tensor[1][1], tensor[1][2], tensor[2][2]]
    
    dti_signal = dti_model_obj.forward(theta)

    return dti_signal


def add_rician_noise(data, std):
    noisy_data = rician_model_fun.apply_noise(data, std)
    return noisy_data


def get_eigen_val():
    eig = torch.zeros(3, 10, 10)
    eig[0,:,:] = 1
    eig[1,:,:] = 0
    eig[2,:,:] = 0

    return eig

def compute_snr(signal, std):
    print("SNR: {}".format(torch.mean(signal/std)))
    return signal/std


def generate_data_sim_exp_1(grad_path, dest_path):
    snr_range = np.linspace(4,30,14)
    bvals, bvecs = getGradientDirectionsBvalue(grad_path)
    eig_vals = get_eigen_val()
    fa = get_fa(constant=True, val=torch.ones_like(eig_vals[0])*0.8, range=[], step=[])
    md = get_md(constant=True, val=torch.ones_like(eig_vals[0])*0.8, range=[], step=[])
    s0 = torch.ones_like(eig_vals[0])
    base_filename = "experiment_1_snr_"
    n_realizations = 100

    diffusion_tensor = get_diffusion_tensor_from_fa_md(fa, md, eig_vals)
    diffusion_signal = get_diffusion_signal(diffusion_tensor, s0, bvecs, bvals)

    data = {}
    for s_, snr_ in enumerate(snr_range):
        for r_ in range(0, n_realizations):
            std_n = 1/snr_
            diffusion_signal_noisy = add_rician_noise(diffusion_signal, std_n)
            filename = "snr_" + str(int(snr_)) + "/" + base_filename + str(snr_) + "_repeat_" + str(r_) +".pkl"


            data['image'] = diffusion_signal_noisy#.float()
            data['filename'] = filename
            data['label'] = diffusion_signal_noisy#.float()
            data['mask'] = []

            image_utilities.saveDataPickle(data, dest_path, filename)



def generate_data_sim_exp_2(grad_path, dest_path):
    snr = 30
    bval = 1.0
    n_range = np.arange(6,126,6)
    print(n_range)
    eig_vals = get_eigen_val()
    fa = get_fa(constant=True, val=torch.ones_like(eig_vals[0])*0.8, range=[], step=[])
    md = get_md(constant=True, val=torch.ones_like(eig_vals[0])*0.8, range=[], step=[])
    s0 = torch.ones_like(eig_vals[0])
    base_filename = "experiment_2_N_"
    n_realizations = 1000


def generate_data_sim_exp_4(grad_path, dest_path):
    snr = 30
    bvals, bvecs = getGradientDirectionsBvalue(grad_path)
    bval_range = [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2]

    eig_vals = get_eigen_val()
    fa = get_fa(constant=True, val=torch.ones_like(eig_vals[0])*0.8, range=[], step=[])
    md = get_md(constant=True, val=torch.ones_like(eig_vals[0])*0.8, range=[], step=[])
    s0 = torch.ones_like(eig_vals[0])
    base_filename = "experiment_4_bval_"
    n_realizations = 1000

    diffusion_tensor = get_diffusion_tensor_from_fa_md(fa, md, eig_vals)

    data = {}
    for b_, bval in enumerate(bval_range):
        for r_ in range(0, n_realizations):
            std_n = 1/snr
            curr_bval = np.array(bvals) * bval
            diffusion_signal = get_diffusion_signal(diffusion_tensor, s0, bvecs, curr_bval)
            diffusion_signal_noisy = add_rician_noise(diffusion_signal, std_n)

            filename = "bval_" + str(bval) + "/" + base_filename + str(bval) + "_repeat_" + str(r_) +".pkl"

            data['image'] = diffusion_signal_noisy#.float()
            data['filename'] = filename
            data['label'] = diffusion_signal_noisy#.float()
            data['mask'] = []

            image_utilities.saveDataPickle(data, dest_path, filename)










if __name__ == '__main__':
    grad_path = '../training/training_bvecs_bvals/quaternion_rotation_norm/sample_ 126_rotated_grad.txt'
    dest_path = '/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_1/correct_bvec_rotation/'
    # generate_data_sim_exp_1(grad_path, dest_path)
    # noisy_dti_data = generate_data_sim_exp_4(grad_path, dest_path)

    generate_data_sim_exp_1(grad_path, dest_path)


    # estimated_tensors = mrtrix_model.forward_local(noisy_dti_data.unsqueeze(0), dti_model_obj, grad_path)
    # e_val, e_vec = dti_deriv_fun.from_tensor_to_eig_v(estimated_tensors[1:])
        
    # fa_map = fractional_anisotropy(e_val)
    # md_map = mean_diffusivity(e_val)

    # fig, ax = plt.subplots(1,2)
    # ax[0].imshow(estimated_tensors[0], vmin=0, vmax=1.5)
    # ax[1].imshow(md_map, vmin=0, vmax=0.0005)
    # plt.show()

    # print(estimated_tensors.shape)



















