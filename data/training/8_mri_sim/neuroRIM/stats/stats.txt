Fill fraction of sphere: 0.80138
Total overlap fraction (in sphere): 0.693036

Segment overlap fraction min: 0
Segment overlap fraction max: 1
Segment overlap fraction median: 0
Segment overlap fraction 2.5th percentile: 0
Segment overlap fraction 97.5th percentile: 0.713535
Segment overlap fraction 1st quartile: 0
Segment overlap fraction 3rd quartile: 0
Segment overlap fraction standard deviation: 0.680971

Angle min: 0
Angle max: 178.376
Angle median: 8.65865
Angle 2.5th percentile: 0.170132
Angle 97.5th percentile: 31.2994
Angle 1st quartile: 4.45188
Angle 3rd quartile: 14.1897
Angle average: 10.8078
Angle standard deviation: 8.84133

Length max: 0.653172
Length min: 0.000953872
Length median: 0.0200765
Length 2.5th percentile: 0.0133127
Length 97.5th percentile: 0.0280277
Length 1st quartile: 0.0193956
Length 3rd quartile: 0.02143
Length average: 0.0204369
Length standard deviation: inf

Radius of curvature min: 0.00132997
Radius of curvature max: inf
Radius of curvature median: 0.136834
Radius of curvature 2.5th percentile: 0.0386608
Radius of curvature 97.5th percentile: 6.75202
Radius of curvature 1st quartile: 0.0840751
Radius of curvature 3rd quartile: 0.263846
Radius of curvature average: 1e+30
Radius of curvature standard deviation: inf

Total grid subvoxels in sphere: 14945616
Net grid subvoxels filled (in sphere): 11977122
Gross grid subvoxels filled (in sphere): 39018009

