fov 3.25
cost_repulsion_weight 200
cost_bending_weight 1
cost_length_weight 20
cost_repulsion_power 1
cost_bending_power 2
cost_length_power 2
relative_isotropic_repulsion_weight 100
sample_density 100
grid_reference_size 40
initial_step_size 1
step_tol 0.1
optimization_tol 0.01
jitter_dev 0.01
jitter_seed 1193125662
max_iterations 100
iterations_save_freq 1
