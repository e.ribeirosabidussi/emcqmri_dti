from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import os
package_path = os.path.abspath(__file__ + "/../../")
sys.path.insert(0, package_path)

import logging
from EMCqMRI.core.engine import build_model as core_build
from utilities import all_utils as utils
import torch
torch.autograd.set_detect_anomaly(True)

def override_configuration(config_object):

    # DATASET MODEL
    if config_object.args.engine.datasetModel == 'custom_dti':
        from custom_dataset import dti_dataset
        config_object.args.engine.dataset_model = dti_dataset.DatasetModel(config_object)
        logging.info('Loading custom dataset model: {}'.format(config_object.args.engine.datasetModel))
        
    # SIGNAL MODEL
    if config_object.args.engine.signalModel == 'custom_dti':
        from custom_signal_model import dti_model
        config_object.args.engine.signal_model = dti_model.DTI(config_object)
        logging.info('Loading custom signal model: {}'.format(config_object.args.engine.signalModel))


    # INFERENCE MODEL
    if config_object.args.engine.inferenceModel == 'custom_mle':
        from custom_inference_model import mle
        config_object.args.engine.inference_model = mle.Mle(config_object)
        config_object.args.engine.inference_model.__name__ = 'MLE'
        logging.info('Loading custom inference model: {}'.format(config_object.args.engine.inferenceModel))

    elif config_object.args.engine.inferenceModel == 'custom_rim':
        from custom_inference_model import rim
        config_object.args.engine.inference_model = rim.Rim(config_object)
        config_object.args.engine.inference_model.__name__ = 'RIM'
        logging.info('Loading custom inference model: {}'.format(config_object.args.engine.inferenceModel))

    elif config_object.args.engine.inferenceModel == 'dipy_dti':
        from custom_inference_model import dipy_dti
        config_object.args.engine.inference_model = dipy_dti.DipyDTI(config_object)
        config_object.args.engine.inference_model.__name__ = 'DiPy'
        logging.info('Loading custom inference model: {}'.format(config_object.args.engine.inferenceModel))

    elif config_object.args.engine.inferenceModel == 'fsl_dti':
        from custom_inference_model import fsl_dti
        config_object.args.engine.inference_model = fsl_dti.Fsl(config_object)
        config_object.args.engine.inference_model.__name__ = 'FSL'
        logging.info('Loading custom inference model: {}'.format(config_object.args.engine.inferenceModel))

    elif config_object.args.engine.inferenceModel == 'mrtrix_dti':
        from custom_inference_model import mrtrix_dti
        config_object.args.engine.inference_model = mrtrix_dti.MRTrix(config_object)
        config_object.args.engine.inference_model.__name__ = 'MRTrix3'
        logging.info('Loading custom inference model: {}'.format(config_object.args.engine.inferenceModel))


    # TRAINING LOSS
    if config_object.args.engine.lossFunction == 'custom_dti':
        from custom_loss import dwi_loss
        config_object.args.engine.objective_fun = dwi_loss.dwiLoss(config_object)
        logging.info('Loading custom loss function model: {}'.format(config_object.args.engine.lossFunction))


    # LIKELIHOOD MODEL
    if config_object.args.engine.likelihoodModel == 'custom_rician':
        from custom_likelihood_model import rician_model
        config_object.args.engine.likelihood_model = rician_model.Rician(config_object)
        logging.info('Loading custom likelihood model: {}'.format(config_object.args.engine.likelihoodModel))

    elif config_object.args.engine.likelihoodModel == 'custom_gaussian':
        from custom_likelihood_model import gaussian
        config_object.args.engine.likelihood_model = gaussian.Gaussian(config_object)
        logging.info('Loading custom likelihood model: {}'.format(config_object.args.engine.likelihoodModel))


    # MOTION MODEL
    if config_object.args.dataset.applyMotion:
        from custom_dataset import motion_model
        config_object.args.engine.motion_operator = motion_model.DenseDeformationField(config_object)
        logging.info('Loading custom motion model: {}'.format(config_object.args.engine.motion_operator))


    # VIZUALIZATION
    if config_object.args.engine.logging:
        config_object.args.engine.log_training_fun = utils.TensorboardLog(config_object)
        logging.info('Using Tensorboard logging function: {}'.format(config_object.args.engine.log_training_fun))
    else:
        config_object.args.engine.log_training_fun = utils.LogFun(config_object)
        logging.info('Loading local logging function: {}'.format(config_object.args.engine.log_training_fun))


    # EXTRA CONFIGURATIONS
    config_object.args.engine.prepare_batch = utils.custom_prep_batch
    config_object.args.engine.compute_loss = True

    return config_object


if __name__=='__main__':
    configurationObj = core_build.make('testing', override_configuration)
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))

    configurationObj.args.engine.estimator.run()

    


    