from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from custom_dataset.utilities import getGradientDirectionsBvalue

from EMCqMRI.core.base import base_signal_model
import numpy as np
import os
import torch




class DTI(base_signal_model.SignalModel):
    """
        Class implementing the forward model of a Look Locker based acquisition.
        Contains methods:
            - setTau: set inversion times of acquisition
            - forwardModel: Defines the signal model
            - generateWeightedImages: wrapper to generate N weighted images from A, B and T1 maps
            - gradients: Uses autograd to automatically compute gradients w.r.t. the likelihood function
            - initializeParameters: Initialises all tissue parameters 
    """
    def __init__(self, configObject):
        super(DTI, self).__init__()
        self.__name__ = 'DTI'
        self.__nParameters__ = 6
        if configObject:
            self.args = configObject.args
        self.b_vecs = []
        self.b_vals = []

    def __forwardModel__(self, so, d_vals, b_val, b_vec):
        D = (b_vec[0]**2)*d_vals[0] + \
            (b_vec[1]**2)*d_vals[3] + \
            (b_vec[2]**2)*d_vals[5] + \
            (2*b_vec[0]*b_vec[1])*d_vals[1] + \
            (2*b_vec[0]*b_vec[2])*d_vals[2] + \
            (2*b_vec[1]*b_vec[2])*d_vals[4]
        return so * torch.exp(-b_val*D)

    def forward(self, kappa, *extra_args):
        weightedImages = []
        for b_vec, b_val in zip(self.b_vecs, self.b_vals):
            dw_image = self.__forwardModel__(kappa[0], kappa[1:], b_val, b_vec)

            weightedImages.append(dw_image)
        return torch.stack(weightedImages)

        
    def initialize_parameters(self, signal):       
        if self.args.dataset.load_fixed_grad_dirs:
            self.b_vals, self.b_vecs = getGradientDirectionsBvalue(self.args.dataset.locationGradient)
        else:
            self.args.dataset.locationGradient = self.args.dataset.inst_filename_[:-4] + "_grad_dir.txt"
            self.b_vals, self.b_vecs = getGradientDirectionsBvalue(self.args.dataset.locationGradient)