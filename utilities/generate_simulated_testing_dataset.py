from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import sys
sys.path.append('../')
from EMCqMRI.core.engine import build_model as core_build
from EMCqMRI.core.engine import estimate
from EMCqMRI.core.utilities import image_utilities
from utilities import all_utils as utils

import matplotlib.pyplot as plt

from pathlib import Path
import ntpath
import numpy as np

def override_model(config_object):
    if config_object.args.engine.datasetModel == 'custom_dti':
        from custom_dataset import dti_dataset
        config_object.args.engine.dataset_model = dti_dataset.DatasetModel(config_object)
        logging.info('Loading custom dataset model: {}'.format(config_object.args.engine.datasetModel))
    elif config_object.args.engine.datasetModel == 'custom_dti_virtual_phantom':
        from custom_dataset import dti_dataset_virtual_phantom
        config_object.args.engine.dataset_model = dti_dataset_virtual_phantom.DatasetModel(config_object)
        logging.info('Loading custom dataset model: {}'.format(config_object.args.engine.datasetModel))


    if config_object.args.engine.signalModel == 'custom_dti':
        from custom_signal_model import dti_model
        config_object.args.engine.signal_model = dti_model.DTI(config_object)
        logging.info('Loading custom signal model: {}'.format(config_object.args.engine.signalModel))

    
    if config_object.args.engine.inferenceModel == 'custom_rim':
        from custom_inference_model import rim
        config_object.args.engine.inference_model = rim.Rim(config_object)
        logging.info('Loading custom inference model: {}'.format(config_object.args.engine.inferenceModel))

    

    if config_object.args.engine.lossFunction == 'custom_dti':
        from custom_loss import dwi_loss
        config_object.args.engine.objective_fun = dwi_loss.dwiLoss(config_object)
        logging.info('Loading custom loss function model: {}'.format(config_object.args.engine.lossFunction))


    if config_object.args.engine.likelihoodModel == 'custom_rician':
        from custom_likelihood_model import rician_model
        config_object.args.engine.likelihood_model = rician_model.Rician(config_object)
        logging.info('Loading custom likelihood model: {}'.format(config_object.args.engine.likelihoodModel))

    
    if config_object.args.dataset.applyMotion:
        from custom_dataset import motion_model
        config_object.args.engine.motion_operator = motion_model.DenseDeformationField(config_object)
        logging.info('Loading custom motion model: {}'.format(config_object.args.engine.motion_operator))

    
    config_object.args.engine.prepare_batch = utils.custom_prep_batch
    config_object.args.engine.compute_loss = True
    config_object.args.engine.log_training_fun = None

    return config_object


def generate_sim_data(configObject):

    for d, data in enumerate(configObject.args.engine.dataloader):
        signal = data['image']
        filename = data['filename']
        label = data['label']
        mask = data['mask']

        signal = signal.squeeze(0)
        label = label.squeeze(0)

        path_to_save = configObject.args.engine.saveResultsPath

        # line_signal = np.transpose(np.moveaxis(np.array(signal), 0, 1)[50])

        # plt.figure()
        # plt.imshow(line_signal, vmin=0, vmax=1)
        # plt.show()

        # image_utilities.imagebrowse_slider(signal)
        image_utilities.imagebrowse_slider(signal, vmin_=0, vmax_=1.5)
        
        # filename_ = filename[0] + '_repeat_' + str(d)
        # image_utilities.saveDataPickle(data, path_to_save, filename_)

        
        
        # for patch in range(len(signal)):
            
        #     filename_ = filename[0] + 'patch_' + str(patch)

        #     # image_utilities.calculate_snr(signal[patch], mask[patch,-1], configObject.args.task.sigmaNoise)
        #     image_utilities.imagebrowse_slider(signal)
            # image_utilities.saveDataPickle(data, configObject.args, filename_)



        # break


if __name__=='__main__':
    configurationObj = core_build.make('testing', override_model)
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))

    generate_sim_data(configurationObj)