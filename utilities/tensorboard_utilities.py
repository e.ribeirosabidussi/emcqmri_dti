

from torch.utils.tensorboard import SummaryWriter

import torchvision
import torch
import io
import PIL.Image
from torchvision.transforms import ToTensor
import matplotlib.pyplot as plt

class TensorboardLog(object):
    def __init__(self, config_object):
        self.args = config_object.args
        self.writer = SummaryWriter('log_training/' + self.args.engine.experimentName)

    def read_convert_buffer_image(self, buffer):
        image = PIL.Image.open(buffer)
        image = ToTensor()(image)
        return image

    def get_plot_buffer(self, est, lab):
        plt.figure(figsize=(7,7))
        plt.plot(est, lab, 'xk', markersize=4, alpha=0.1)
        plt.plot(lab, lab, 'k--', linewidth=0.2)
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close()
        buf.seek(0)
        return buf

    def get_img_buffer(self, image):
        plt.figure(figsize=(7,7))
        plt.imshow(image, cmap="CMRmap", vmin=0.0, vmax=1.0)
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close()
        buf.seek(0)
        return buf

    def __call__(self, data, loss, sample_it, epoch):
        estimate = torch.Tensor(data['estimated'])[0][-1]
        signal = torch.Tensor(data['signal'][0]).detach().numpy()
        labels = torch.Tensor(data['label'][0]).detach().numpy()

        predicted_dwi = self.args.engine.signal_model.forward(estimate).detach().numpy()

        iter_sample = (sample_it+1) + (epoch*len(self.args.engine.dataloader))
        self.writer.add_scalar('Sample Loss', loss, iter_sample)

        if sample_it % len(self.args.engine.dataloader) == 0:
            buffer = self.get_img_buffer(predicted_dwi[2])
            estimated_dwi_buf = self.read_convert_buffer_image(buffer)
            buffer = self.get_img_buffer(signal[2])
            label_dwi_buf = self.read_convert_buffer_image(buffer)

            buffer = self.get_plot_buffer(predicted_dwi[2], signal[2])
            plot_dwi = self.read_convert_buffer_image(buffer)

            signal = torch.stack([estimated_dwi_buf, label_dwi_buf])
            plots = plot_dwi

            gridestimated = torchvision.utils.make_grid(signal, nrow=2, scale_each=True)
            gridplots = torchvision.utils.make_grid(plots, nrow=1)

            self.writer.add_image('Signal/estimate_label', gridestimated, epoch)
            self.writer.add_image('Signal/plots', gridplots, epoch) 