import numpy as np

from dipy.reconst.dti import fractional_anisotropy, color_fa, mean_diffusivity
from dipy.reconst.dti import lower_triangular, eig_from_lo_tri
from dipy.core.gradients import gradient_table
from dipy.viz import window, actor
import dipy.data as dpd

from matplotlib.colors import LinearSegmentedColormap
from mpl_toolkits.axes_grid1 import make_axes_locatable

import torch
import matplotlib.pyplot as plt

class LogFun(object):
    def __init__(self, config_object):
        self.args = config_object.args
        colors = ['blue', 'black', 'red']
        self.cm = LinearSegmentedColormap.from_list('error_map', colors, N=100)
        self.plot_odf = True

    def build_d_mat(self, d_tensors):
        d_mat = np.array([[d_tensors[0], d_tensors[1], d_tensors[2]],
                        [d_tensors[1], d_tensors[3], d_tensors[4]],
                        [d_tensors[2], d_tensors[4], d_tensors[5]]])

        d_mat_perm = np.transpose(d_mat, (-1, -2, 0, 1))
        return d_mat_perm

    def viz_direction_map(self, e_vecs, fa):
        color_bar = color_fa(fa, e_vecs)
        plt.figure()
        plt.imshow(color_bar)
        plt.show()

    def from_tensor_to_eig_v(self, tensors):
        d_mat = self.build_d_mat(np.stack(tensors.detach().numpy()))
        l_triang = lower_triangular(d_mat)
        eig_v = eig_from_lo_tri(l_triang, min_diffusivity=1e-6)
        e_val = eig_v[...,:3]
        e_vec = eig_v[...,3:12]
        sh_tensor = tensors.shape[-2:]
        e_vec = e_vec.reshape(*sh_tensor, 3, 3)

        return e_val, e_vec

    def viz_fa_direction(self, e_val, e_vec):
        fa_map = fractional_anisotropy(e_val)
        self.viz_direction_map(e_vec, fa_map)

    def viz_fa_md(self, e_val):
        fa_map = fractional_anisotropy(e_val)
        md_map = mean_diffusivity(e_val)

        mask = np.ones_like(fa_map)
        mask[fa_map>0.0] = 0
        masked_fa = np.ma.array(fa_map, mask=mask)
        fa_map_ = np.reshape(masked_fa, (-1, 1))

        md_map_ = np.reshape(md_map, (-1, 1))

        fig, ax = plt.subplots(1,4)
        ax[0].imshow(masked_fa, cmap='gray', vmin=0, vmax=1)
        ax[1].hist(fa_map_, bins=100, density=True, histtype='stepfilled')
        ax[2].imshow(md_map, cmap='gray', vmin=0, vmax=1)
        ax[3].hist(md_map_, bins=100, density=True, histtype='stepfilled')
        plt.show()


    def viz_odf(self, e_val, e_vec):
        fa_map = fractional_anisotropy(e_val)
        color_bar = color_fa(fa_map, e_vec)

        e_val = e_val[:,:,None,...]
        e_vec = e_vec[:,:,None,...]
        color_bar = color_bar[:,:,None,...]

        sphere = dpd.default_sphere
        ren = window.Scene()
        ren.add(actor.tensor_slicer(e_val, e_vec, scalar_colors=color_bar, sphere=sphere,
                                    scale=0.6))

        window.show(ren)

    def viz_residual(self, signal, predicted_signal):
        residual = signal - predicted_signal

        std_sig = self.get_std_signal(signal)
        std_pred = self.get_std_signal(predicted_signal)

        sos_residuals = np.sum(residual**2, 0)

        dwi = 5
        fig, ax = plt.subplots(1,3)
        ax[0].imshow(signal[dwi], cmap='gray', vmin=0.0, vmax=0.75)
        ax[1].imshow(predicted_signal[dwi], cmap='gray', vmin=0.0, vmax=0.75)
        ax_res = ax[2].imshow(residual[dwi], cmap=self.cm, vmin=-.05, vmax=0.05)

        divider = make_axes_locatable(ax[2])
        cax = divider.append_axes('right', size='5%', pad=0.05)

        fig.colorbar(ax_res, cax=cax)
        plt.show()

        dwi = 0
        fig, ax = plt.subplots(1,3)
        ax[0].imshow(signal[dwi], cmap='gray', vmin=0.0, vmax=1.25)
        ax[1].imshow(predicted_signal[dwi], cmap='gray', vmin=0.0, vmax=1.25)
        ax_res = ax[2].imshow(residual[dwi], cmap=self.cm, vmin=-.05, vmax=0.05)

        divider = make_axes_locatable(ax[2])
        cax = divider.append_axes('right', size='5%', pad=0.05)

        fig.colorbar(ax_res, cax=cax)
        plt.show()

    def get_std_signal(self, signal):
        std_signal = np.std(signal[1:], 0)
        return std_signal

    def __call__(self, processed_data, loss, sample, ep):
        if self.args.engine.inference_model.__name__ == 'RIM':
            tensor_estimate = processed_data['estimated'][-1][0]
        else:
            tensor_estimate = processed_data['estimated']

        signal = processed_data['signal'][0]

        predicted_dwi = self.args.engine.signal_model.forward(tensor_estimate)
        processed_data["predicted_signal"] = predicted_dwi

        e_val, e_vec = self.from_tensor_to_eig_v(tensor_estimate[1:])

        self.viz_residual(signal.detach().numpy(), predicted_dwi.detach().numpy())
        self.viz_fa_direction(e_val, e_vec)
        # self.viz_odf(e_val, e_vec)
        # self.viz_fa_md(e_val)