import numpy as np
import numpy.ma as ma

from dipy.reconst.dti import fractional_anisotropy, color_fa, mean_diffusivity, radial_diffusivity, axial_diffusivity
from dipy.reconst.dti import lower_triangular, eig_from_lo_tri
from dipy.core.gradients import gradient_table
from dipy.viz import window, actor
import dipy.data as dpd

from matplotlib.colors import LinearSegmentedColormap
from mpl_toolkits.axes_grid1 import make_axes_locatable

import torch
import matplotlib.pyplot as plt

from torch.utils.tensorboard import SummaryWriter
import torchvision
import io
import PIL.Image
from torchvision.transforms import ToTensor
from EMCqMRI.core.utilities import image_utilities
from scipy.spatial.transform import Rotation as R

class LogFun(object):
    def __init__(self, config_object):
        self.args = config_object.args
        colors = ['blue', 'black', 'red']
        self.cm = LinearSegmentedColormap.from_list('error_map', colors, N=100)
        self.plot_odf = True

    def build_d_mat(self, d_tensors):
        d_mat = np.array([[d_tensors[0], d_tensors[1], d_tensors[2]],
                        [d_tensors[1], d_tensors[3], d_tensors[4]],
                        [d_tensors[2], d_tensors[4], d_tensors[5]]])

        d_mat_perm = np.transpose(d_mat, (-2, -1, 0, 1))
        return d_mat_perm

    def from_tensor_to_eig_v(self, tensors):
        d_mat = self.build_d_mat(np.stack(tensors.detach().numpy()))
        l_triang = lower_triangular(d_mat)
        eig_v = eig_from_lo_tri(l_triang, min_diffusivity=1e-6)
        e_val = eig_v[...,:3]
        e_vec = eig_v[...,3:12]
        sh_tensor = tensors.shape[-2:]
        e_vec = e_vec.reshape(*sh_tensor, 3, 3)

        return e_val, e_vec

    def viz_direction_map(self, e_vecs, fa, mask_):
        color_bar = color_fa(fa*mask_, e_vecs)
        color_bar_t = np.transpose(color_bar, axes=[1,0,2])
        fa_t = np.transpose(fa*mask_, axes=[1,0])

        plt.figure()
        plt.imshow(fa_t, origin='lower', cmap='gray', alpha=0.8)
        plt.imshow(color_bar_t, origin='lower', alpha=0.8)
        plt.show()
 
    def viz_fa_direction(self, e_val, e_vec, mask_):
        fa_map = fractional_anisotropy(e_val)
        self.viz_direction_map(e_vec, fa_map, mask_)

    def viz_fa_md(self, fa_map, md_map, mask):
        masked_fa = fa_map[mask == True]
        masked_md = md_map[mask == True]

        fa_map_ = np.reshape(masked_fa, (-1, 1))
        md_map_ = np.reshape(masked_md, (-1, 1))

        fig, ax = plt.subplots(1,4)
        ax[0].imshow(fa_map*mask, cmap='gray', vmin=0, vmax=1)
        ax[1].hist(fa_map_, bins=100, density=True, histtype='stepfilled')
        ax[2].imshow(md_map*mask, cmap='gray', vmin=0, vmax=1)
        ax[3].hist(md_map_, bins=100, density=True, histtype='stepfilled')
        plt.show()

        return fa_map_, md_map_

    def viz_odf(self, e_val, e_vec, mask):
        fa_map = fractional_anisotropy(e_val)*mask
        fa_map[fa_map<0.15]=0
        color_bar = color_fa(fa_map, e_vec)
        e_val = e_val[:,:,None,...]
        e_vec = e_vec[:,:,None,...]
        color_bar = color_bar[:,:,None,...]
        sphere = dpd.default_sphere
        ren = window.Scene()
        ren.add(actor.tensor_slicer(e_val, e_vec, scalar_colors=color_bar, sphere=sphere,
                                    scale=0.6))
        window.show(ren)

    def viz_residual(self, signal, predicted_signal):
        residual = signal - predicted_signal

        std_sig = self.get_std_signal(signal)
        std_pred = self.get_std_signal(predicted_signal)

        sos_residuals = np.sum(residual**2, 0)

        dwi = 5
        fig, ax = plt.subplots(1,3)
        ax[0].imshow(signal[dwi], cmap='gray', vmin=0.0, vmax=0.75)
        ax[1].imshow(predicted_signal[dwi], cmap='gray', vmin=0.0, vmax=0.75)
        ax_res = ax[2].imshow(residual[dwi], cmap='seismic', vmin=-.02, vmax=.02)

        divider = make_axes_locatable(ax[2])
        cax = divider.append_axes('right', size='5%', pad=0.05)

        fig.colorbar(ax_res, cax=cax)
        plt.show()

        dwi = 0
        fig, ax = plt.subplots(1,3)
        ax[0].imshow(signal[dwi], cmap='gray', vmin=0.0, vmax=1.25)
        ax[1].imshow(predicted_signal[dwi], cmap='gray', vmin=0.0, vmax=1.25)
        ax_res = ax[2].imshow(residual[dwi], cmap=self.cm, vmin=-.05, vmax=0.05)

        divider = make_axes_locatable(ax[2])
        cax = divider.append_axes('right', size='5%', pad=0.05)

        fig.colorbar(ax_res, cax=cax)
        plt.show()

    def __call__(self, processed_data, loss, sample, ep):

        # Here, you can prepare your data to be saved or vizualized

        if self.args.engine.inference_model.__name__ == 'RIM':
            tensor_estimate = processed_data['estimated'][-1][0]
        else:
            tensor_estimate = processed_data['estimated']

        tensor_estimate = torch.nan_to_num(tensor_estimate, 0.0)
        
        e_val, e_vec = self.from_tensor_to_eig_v(tensor_estimate[1:])
        predicted_dwi = self.args.engine.signal_model.forward(tensor_estimate)
        scaled_predicted_dwi = self.args.engine.dataset_model.denormalize_data(predicted_dwi)

        fa_map = fractional_anisotropy(e_val)
        md_map = mean_diffusivity(e_val)

        label = processed_data['label'][0][0]
        signal = processed_data['signal'][0]

        processed_data["predicted_signal"] = scaled_predicted_dwi
        processed_data["fa_map"] = fa_map
        processed_data["md_map"] = md_map
        processed_data["diffusion_tensors"] = tensor_estimate
        mask_ = np.ones_like(fa_map)

        viz_odf = False
        if viz_odf:
            self.viz_odf(e_val, e_vec, mask_)
        
        viz_fa_md = True
        if viz_fa_md:
            image_utilities.imagebrowse_slider(np.array([fa_map, md_map]), vmin_=0, vmax_=1)
        
        print_fa_md_central=True
        if print_fa_md_central:
            print("FA={}, MD={}".format(fa_map[4,4], md_map[4,4]))
    
    def rotate_tensor(self, tensor):
        tensor_ = tensor[1:]
        reconstructed_tensor = np.zeros([10,10,3,3])
        reconstructed_tensor[:,:,0,0] = tensor_[0].detach().numpy()
        reconstructed_tensor[:,:,0,1] = tensor_[1].detach().numpy()
        reconstructed_tensor[:,:,0,2] = tensor_[2].detach().numpy()
        reconstructed_tensor[:,:,1,0] = tensor_[1].detach().numpy()
        reconstructed_tensor[:,:,1,1] = tensor_[3].detach().numpy()
        reconstructed_tensor[:,:,1,2] = tensor_[4].detach().numpy()
        reconstructed_tensor[:,:,2,0] = tensor_[2].detach().numpy()
        reconstructed_tensor[:,:,2,1] = tensor_[4].detach().numpy()
        reconstructed_tensor[:,:,2,2] = tensor_[5].detach().numpy()

        rotation_degrees_z = -15
        rotation_radians = np.radians(rotation_degrees_z)
        rotation_axis = np.array([0, 0, 1])

        rotation_vector = rotation_radians * rotation_axis
        rotation = R.from_rotvec(rotation_vector)

        rotated_tensor = np.zeros([10,10,3,3])
        for ti, tensor_i in enumerate(reconstructed_tensor):
            for tj, tensor_j in enumerate(tensor_i):
                rotated_tensor[ti, tj] = np.matmul(rotation.as_matrix(), np.matmul(tensor_j, np.transpose(rotation.as_matrix())))

        compressed_tensor = [tensor[0].detach().numpy(), rotated_tensor[..., 0, 0], rotated_tensor[..., 0, 1], rotated_tensor[..., 0, 2], rotated_tensor[..., 1, 1], rotated_tensor[..., 1, 2], rotated_tensor[..., 2, 2]]
        self.rotation_happened = True
        return torch.Tensor(list(compressed_tensor))

class TensorboardLog(object):
    def __init__(self, config_object):
        self.args = config_object.args
        self.writer = SummaryWriter('log_training/' + self.args.engine.experimentName)
        self.epoch_loss = 0
        self.initial_likelihood_loss = 0

        self.internal_iter = 0

    def read_convert_buffer_image(self, buffer):
        image = PIL.Image.open(buffer)
        image = ToTensor()(image)
        return image

    def get_plot_buffer(self, est, lab, title_=''):
        plt.figure(figsize=(7,7))
        plt.plot(est, lab, 'xk', markersize=4, alpha=0.1)
        plt.plot(lab, lab, 'k--', linewidth=0.2)
        buf = io.BytesIO()
        plt.title(title_)
        plt.savefig(buf, format='png')
        plt.close()
        buf.seek(0)
        return buf

    def get_img_buffer(self, image, vmin_ = 0.0, vmax_= 1.0, cmap_="gray", title_=""):
        plt.figure(figsize=(7,7))
        plt.imshow(image, cmap=cmap_, vmin=vmin_, vmax=vmax_)
        buf = io.BytesIO()
        plt.title(title_)
        plt.savefig(buf, format='png')
        plt.close()
        buf.seek(0)
        return buf

    def __call__(self, data, loss, sample_it, epoch):
        
        if self.args.engine.inference_model.__name__ == 'RIM':
            tensor_estimate = data['estimated'][-1][0]
        else:
            tensor_estimate = data['estimated']

        scaled_estimates = torch.stack([tensor_estimate[0]*self.args.dataset.g_std, *tensor_estimate[1:]])
        predicted_dwi = self.args.engine.signal_model.forward(scaled_estimates).detach().cpu().numpy()
        
        labels = data['label'][0][0].detach().cpu().numpy()
        signal = data['signal'][0].detach().cpu().numpy()

        self.epoch_loss += loss

        # Plot loss - mean per epoch
        if sample_it % len(self.args.engine.dataloader) == 0 and epoch>0:
            mean_epoch_loss = self.epoch_loss/len(self.args.engine.dataloader)
            self.writer.add_scalar('mean_loss/epoch', mean_epoch_loss, epoch)
            self.epoch_loss = 0

        # # Plot figures - every 50 epochs
        if epoch % 50 == 0:
            iter_sample = (self.internal_iter) + (epoch*len(self.args.engine.dataloader))
            self.internal_iter += 1
            buffer = self.get_img_buffer(predicted_dwi[2], title_="Predicted S_0", vmin_=0, vmax_=2)
            estimated_dwi_buf = self.read_convert_buffer_image(buffer)
            buffer = self.get_img_buffer(labels[2], title_="Label S_0", vmin_=0, vmax_=2)
            label_dwi_buf = self.read_convert_buffer_image(buffer)
            buffer = self.get_img_buffer(labels[2]-predicted_dwi[2], vmin_=-0.5, vmax_=0.5, cmap_='seismic', title_="Diff pred-label")
            diff_label_dwi_buf = self.read_convert_buffer_image(buffer)

            buffer = self.get_img_buffer(signal[2]*self.args.dataset.g_std.detach().numpy(), title_="Signal S_2", vmin_=0, vmax_=2)
            signal_dwi_buf = self.read_convert_buffer_image(buffer)
            buffer = self.get_img_buffer(signal[2]*self.args.dataset.g_std.detach().numpy()-predicted_dwi[2], vmin_=-0.5, vmax_=0.5, cmap_='seismic', title_="Diff pred-signal")
            diff_signal_dwi_buf = self.read_convert_buffer_image(buffer)

            buffer = self.get_img_buffer(labels[2]-signal[2]*self.args.dataset.g_std.detach().numpy(), vmin_=-0.5, vmax_=0.5, cmap_='seismic', title_="Diff label-signal")
            diff_lab_pred_dwi_buf = self.read_convert_buffer_image(buffer)

            buffer = self.get_plot_buffer(labels[2], predicted_dwi[2], title_='label x predicted')
            plot_label_dwi = self.read_convert_buffer_image(buffer)
            buffer = self.get_plot_buffer(signal[2]*self.args.dataset.g_std.detach().numpy(), predicted_dwi[2], title_='signal x predicted')
            plot_signal_dwi = self.read_convert_buffer_image(buffer)
            buffer = self.get_plot_buffer(labels[2], signal[2]*self.args.dataset.g_std.detach().numpy(), title_='label x signal')
            plot_diff_buf = self.read_convert_buffer_image(buffer)

            signal_rw1 = torch.stack([label_dwi_buf, estimated_dwi_buf, diff_label_dwi_buf, plot_label_dwi]) 
            signal_rw2 = torch.stack([signal_dwi_buf, estimated_dwi_buf, diff_signal_dwi_buf, plot_signal_dwi])
            signal_rw3 = torch.stack([label_dwi_buf, signal_dwi_buf, diff_lab_pred_dwi_buf, plot_diff_buf])

            grid_label = torchvision.utils.make_grid(signal_rw1, nrow=3, scale_each=True)
            grid_signal = torchvision.utils.make_grid(signal_rw2, nrow=3, scale_each=True)
            grid_lab_sig = torchvision.utils.make_grid(signal_rw3, nrow=3, scale_each=True)

            self.writer.add_image('Images/Label-Estimated', grid_label, iter_sample)
            self.writer.add_image('Images/Signal-Estimated', grid_signal, iter_sample)
            self.writer.add_image('Images/Label-Signal', grid_lab_sig, iter_sample)


def custom_prep_batch(batchdata, device, non_blocking=False):
    batchdata['image'].to(device=device, non_blocking=non_blocking)
    batchdata['label'].to(device=device, non_blocking=non_blocking)
    
    return (batchdata['image'], batchdata['label'])
