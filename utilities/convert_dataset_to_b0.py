import numpy as np
import pickle as pkl
import sys

sys.path.append('../')

from EMCqMRI.core.utilities import image_utilities
from custom_dataset.utilities import save_gradient_info, getGradientDirectionsBvalue, save_nii_file, open_pickle_file, save_pickle_file
import os

def include_extra_b0(indexes, data, bvals, bvecs):    
    new_data = np.zeros([len(data)+len(indexes), data.shape[1], data.shape[2]])
    ind_ = 0
    ind_exlc = 0
    for ind_ in range(len(new_data)):
        if ind_ in indexes:
            new_data[ind_] = data[0]
            ind_exlc += 1
        else:
            new_data[ind_] = data[ind_-ind_exlc]

    new_bvecs = np.insert(np.array(bvecs), indexes, bvecs[0], axis=0)
    new_bvals = np.insert(np.array(bvals), indexes, bvals[0], axis=0)

    return np.array(new_data), new_bvals, new_bvecs


if __name__ == '__main__':

    data_dir = "../data/training/9_mri_sim_py/neuroRIM_b0_2d/"
    dest_dir = "../data/training/9_mri_sim_py/neuroRIM_extra_b0/"
    
    data_list = np.sort([f for f in os.listdir(data_dir) if f.endswith('.pkl')])
    bvecs_list = np.sort([f for f in os.listdir(data_dir) if f.endswith('.txt')])

    for dlist, blist in zip(data_list, bvecs_list):
        dwi_data = open_pickle_file(data_dir, dlist)
        bvals, bvecs = getGradientDirectionsBvalue(data_dir+blist)

        num_of_images = len(dwi_data)

        list_ = np.arange(0,num_of_images)
        indexes = list_[8:num_of_images:8]

        b0_added_data, new_bvals, new_bvecs = include_extra_b0(indexes, dwi_data, bvals, bvecs)

        dst_gradient = dest_dir + blist
        save_gradient_info(new_bvecs, new_bvals, dst_gradient)
        save_pickle_file(b0_added_data, dest_dir, dlist)

        image_utilities.imagebrowse_slider(b0_added_data)


