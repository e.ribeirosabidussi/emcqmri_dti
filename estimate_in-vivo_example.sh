# Low SNR Dataset
# python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel custom_rim -testingDataPath ./data/in_vivo_experiments/exp1/original -estimatedSNR 26 -inferenceSteps 12 -loadCheckpointPath ./pre_trained/custom_rim_epoch_150_experiment_bigger_voxel_dataset_noBackground.pth 

# High SNR Dataset
python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel custom_rim -testingDataPath ./data/in_vivo_experiments/exp2/original -estimatedSNR 40 -inferenceSteps 12 -loadCheckpointPath ./pre_trained/custom_rim_epoch_150_experiment_bigger_voxel_dataset_noBackground.pth 
