from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys
import numpy as np

sys.path.insert(0, "..")

from EMCqMRI.core.base import base_inference_model
import subprocess
import logging
import pickle
import torch
import nibabel as nib


class Fsl(base_inference_model.InferenceModel, object):
    """
    """
    def __init__(self, configObject):
        super(Fsl, self).__init__()
        self.__name__ = 'FSL'
        self.__require_initial_guess__ = False
        self.args = configObject.args

    # @ProgressBarWrap
    # def update_bar(self, loss, args):
    #     return -1

    def forward(self, inputs):

        data_path = os.path.join(self.args.engine.testingDataPath, self.args.engine.dataset_model.filename_sample)
        mask_path = data_path[:-4] + "_mask.nii"
        bvec_path = self.args.dataset.locationBvecs
        bval_path = self.args.dataset.locationBvals
        output_path = "data/estimated/fsl/in_vivo/b1000_sub1/ses-c01r1/"
        subprocess.run(["/usr/local/fsl/bin/dtifit", "--data="+data_path, "--out="+output_path, "--mask="+mask_path, "--bvecs="+bvec_path, "--bvals="+bval_path, "--wls", "--save_tensor"])

        output_tensor_path = "data/estimated/fsl/in_vivo/b1000_sub1/ses-c01r1/_tensor.nii.gz"
        s0_path = "data/estimated/fsl/in_vivo/b1000_sub1/ses-c01r1/_S0.nii.gz"
        tensor_ = nib.load(output_tensor_path)
        tensor_data = tensor_.get_fdata()
        tensor_data = torch.movedim(torch.from_numpy(tensor_data), -1, 0)


        s0_ = nib.load(s0_path)
        s0_data = torch.from_numpy(s0_.get_fdata())

        dti_data = torch.stack([s0_data, *tensor_data])

        return dti_data[...,self.args.dataset.z_slice]