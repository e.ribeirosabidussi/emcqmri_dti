from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as f

import matplotlib.pyplot as plt
from EMCqMRI.core.utilities import image_utilities


class InitNet(nn.Module):
    def __init__(self, s0_init = [], dwi_init = [], args_ = None):
        super(InitNet, self).__init__()
        self.args = args_


    def initialize_weights(self,m, init_weight=[]):
        for layer in m:
            if isinstance(layer, nn.Conv2d):
                with torch.no_grad():
                    if init_weight:
                        nn.init.kaiming_uniform_(layer.weight,nonlinearity='relu')
                        layer.weight.copy_(torch.abs(layer.weight))
                    else:
                        nn.init.kaiming_uniform_(layer.weight,nonlinearity='relu')
                    # layer.bias.data.fill_(0.001)

    def forward(self, signal, b_value):
        input_s0 = torch.stack([signal_[0] for signal_ in signal])

        batched_init_variables = []
        for batch_signal in signal:
            input_s0 = batch_signal[0].unsqueeze(0)
            init_s0_ = Variable(input_s0.clone(), requires_grad=True)
            dxx = Variable(torch.ones_like(init_s0_), requires_grad=True)
            dyy = Variable(torch.ones_like(init_s0_), requires_grad=True)
            dzz = Variable(torch.ones_like(init_s0_), requires_grad=True)
            dxy = Variable(torch.zeros_like(init_s0_), requires_grad=True)
            dxz = Variable(torch.zeros_like(init_s0_), requires_grad=True)
            dyz = Variable(torch.zeros_like(init_s0_), requires_grad=True)
            initialized_variables = torch.cat([init_s0_, dxx, dxy, dxz, dyy, dyz, dzz], 0)
            batched_init_variables.append(initialized_variables)
    
        return torch.stack(batched_init_variables)