from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from custom_dataset.utilities import getGradientDirectionsBvalue, save_gradient_info

from EMCqMRI.core.base import base_inference_model
import logging
import nibabel as nib
import numpy as np
import os
from pathlib import Path
import pickle
import sys
import subprocess
import torch


sys.path.insert(0, "..")


class MRTrix(base_inference_model.InferenceModel, object):
    """
    """
    def __init__(self, configObject):
        super(MRTrix, self).__init__()
        self.__name__ = 'MRTrix3'
        self.__require_initial_guess__ = False
        if configObject:
            self.args = configObject.args

    def forward_local(self, inputs, signal_model, location_grad):
        filename = "test.nii"
        output_path = "./tests/mrtrix/"
        output_tensors = output_path + filename
        output_s0 = output_path + "s0_prediction.nii"
        output_signal =  output_path + "signal_prediction.nii"

        tmp_out_file = output_path + "temp/tmp_signal.nii"

        pickle_ = True
        signal_data = np.transpose(inputs, (2, 3, 0, 1))
        signal_data[signal_data<=0] = 0.001

        new_signal = nib.Nifti1Image(signal_data.detach().numpy(), affine=np.eye(4))
        nib.save(new_signal, tmp_out_file)

        subprocess.run(["/usr/lib/mrtrix3/bin/dwi2tensor", "-iter", "4", "-grad", location_grad, "-b0", output_s0, "-predicted_signal", output_signal, "-force", "-bvalue_scaling", "false", tmp_out_file, output_tensors])

        fa_image = output_path + "fa_prediction.nii"
        md_image = output_path + "md_prediction.nii"

        subprocess.run(["/usr/lib/mrtrix3/bin/tensor2metric", "-force", "-fa", fa_image, output_tensors])

        s0_path = output_path + "filename.nii"

        tensor_ = nib.load(output_tensors)
        tensor_data = tensor_.get_fdata()
        tensor_data = torch.movedim(torch.from_numpy(tensor_data), -1, 0)

        tensor_data_ = torch.stack([tensor_data[0], tensor_data[3], tensor_data[4], tensor_data[1], tensor_data[5], tensor_data[2]])#[...,30]
        tensor_data_ = tensor_data_
        
        s0_ = nib.load(output_s0)

        s0_data = torch.from_numpy(s0_.get_fdata())
        fa_ = nib.load(fa_image)
        fa_data = fa_.get_fdata()

        dti_data = torch.stack([s0_data, *tensor_data_])
        return dti_data.squeeze()


    def forward(self, inputs):
        data_path = os.path.join(self.args.engine.testingDataPath, self.args.engine.dataset_model.filename_sample)
        filename = Path(self.args.engine.dataset_model.filename_list[0]).stem + ".nii"
        
        output_path = "./data/simulation/tests/mrtrix/"
        output_tensors = output_path + filename
        output_s0 = output_path + "s0_prediction.nii"
        output_signal =  output_path + "signal_prediction.nii"

        tmp_out_file = output_path + "temp/tmp_signal.nii"

        pickle_ = True
        signal_data = np.transpose(inputs, (2, 3, 0, 1))
        signal_data[signal_data<=0] = 0.001

        _ = self.args.engine.signal_model.initialize_parameters(signal_data) #Initialize bvals/bvecs

        new_signal = nib.Nifti1Image(signal_data.detach().numpy(), affine=np.eye(4))
        nib.save(new_signal, tmp_out_file)

        if self.args.dataset.load_fixed_grad_dirs:
            self.b_vals, self.b_vecs = getGradientDirectionsBvalue(self.args.dataset.locationGradient)
            self.b_vals = np.array(self.b_vals) * self.args.engine.bvalConfig
        else:
            self.args.dataset.locationGradient = self.args.dataset.inst_filename_[:-4] + "_grad_dir.txt"
            self.b_vals, self.b_vecs = getGradientDirectionsBvalue(self.args.dataset.locationGradient)

        subprocess.run(["/usr/lib/mrtrix3/bin/dwi2tensor", "-iter", "4", "-grad", self.args.dataset.locationGradient, "-b0", output_s0, "-predicted_signal", output_signal, "-force", "-bvalue_scaling", "false", tmp_out_file, output_tensors])

        fa_image = output_path + "fa_prediction.nii"
        md_image = output_path + "md_prediction.nii"
        value_image = output_path + "evalue_prediction.nii"
        vec_image = output_path + "evector_prediction.nii"

        subprocess.run(["/usr/lib/mrtrix3/bin/tensor2metric", "-force", "-fa", fa_image, "-value", value_image, "-num", '1,2,3', "-vector", vec_image, output_tensors])

        s0_path = output_path + "filename.nii"

        tensor_ = nib.load(output_tensors)
        tensor_data = tensor_.get_fdata()
        tensor_data = torch.movedim(torch.from_numpy(tensor_data), -1, 0)

        tensor_data_ = torch.stack([tensor_data[0], tensor_data[3], tensor_data[4], tensor_data[1], tensor_data[5], tensor_data[2]])#[...,30]
        tensor_data_ = tensor_data_
        
        s0_ = nib.load(output_s0)

        s0_data = torch.from_numpy(s0_.get_fdata())
        fa_ = nib.load(fa_image)
        fa_data = fa_.get_fdata()

        eig_val_ = nib.load(value_image)
        eig_val_data = eig_val_.get_fdata()
        eig_vec_ = nib.load(vec_image)
        eig_vec_data = eig_vec_.get_fdata()

        dti_data = torch.stack([s0_data, *tensor_data_])
        return dti_data.squeeze()