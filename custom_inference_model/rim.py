from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from .rnn import Rnn
from .initNet import InitNet

from EMCqMRI.core.base import base_inference_model
from EMCqMRI.core.utilities.core_utilities import ProgressBarWrap
import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn

class Rim(base_inference_model.InferenceModel, nn.Module):
    """
        Class Implementing the RIM model.
        Methods:
            - setOpts
                inputs: a Dict containing the key and value for a new configuration setting
            - forward
                inputs: signal (measured signal); args (options containing, at least args.batchSize and args.device)
                outputs: Estimated parameters
    """

    def __init__(self, configObject):
        super(Rim, self).__init__()
        self.__name__ = 'RIM'
        self.__require_initial_guess__ = True
        self.args = configObject.args
        self.__buildRNN__()
        self.__buildInitNetwork__()


    def __buildRNN__(self):
        """
            Hidden method that instanciates a version of the RNN module based on the configuration file
        """
        self.rnn = Rnn(self.args.inference.inputChannels,
                       self.args.inference.outputChannelsLayer1,
                       self.args.inference.outputChannelsLayer2,
                       self.args.inference.outputChannelsLayer3,
                       self.args.inference.outputChannels,
                       self.args)
                    
    def __buildDeepRNN__(self):

        self.rnn = Rnn(self.args.inference.inputChannels,
                       self.args.inference.outputChannelsLayer1,
                       self.args.inference.outputChannelsLayer2,
                       self.args.inference.outputChannelsLayer3,
                       self.args.inference.outputChannelsLayer4,
                       self.args.inference.outputChannelsLayer5,
                       self.args.inference.outputChannels,
                       self.args)


    def __buildInitNetwork__(self):
        self.initNet = InitNet(args_=self.args)


    def __initHidden__(self, signal):
        """
            Initialises all hidden states in the network
        """
        shape_input = torch.tensor((signal[0].shape)[1:])
        shape_hs1 = [1, len(signal)*int(torch.prod(shape_input)), self.args.inference.outputChannelsLayer1]
        shape_hs2 = [1, len(signal)*int(torch.prod(shape_input)), self.args.inference.outputChannelsLayer3]
        st_1 = Variable(torch.zeros(tuple(shape_hs1)).to(device=self.args.engine.device))
        st_2 = Variable(torch.zeros(tuple(shape_hs2)).to(device=self.args.engine.device))
        return [st_1, st_2]

    def __getGradients__(self, signal, kappa):
        """
            Compute and return gradients of the Likelihood Function w.r.t. parameter maps
        """
        gradientParamBatch = []
        for batch in range(len(signal)):
            clonedMaps = [Variable(pmap.clone(), requires_grad=True) for pmap in kappa[batch]]
            gradientParamBatch.append(self.args.engine.likelihood_model.gradients(signal[batch], clonedMaps))

        paramGrad = torch.stack(gradientParamBatch)
        paramGrad[torch.isnan(paramGrad)] = 0
        paramGrad[torch.isinf(paramGrad)] = 0
        return paramGrad


    def forward(self, signal):
        self.args.engine.signal_model.initialize_parameters(signal)
        kappa = self.initNet.forward(signal, self.args.engine.signal_model.b_vals)
        hidden = self.__initHidden__(signal)
        estimates = []

        params_ = []
        for self.args.engine.inference_step in range(self.args.inference.inferenceSteps):
            paramGrad = self.__getGradients__(signal, kappa)

            input = torch.cat([kappa, paramGrad], 1)

            dx, hidden = self.rnn.forward(input, hidden)
            kappa = kappa + dx
            estimates.append(kappa)

        return torch.stack(estimates)