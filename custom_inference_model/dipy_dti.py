from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from core.base import base_inference_model
from core.utilities.core_utilities import ProgressBarWrap

import dipy.denoise.noise_estimate as ne
import dipy.reconst.dti as dti
import dipy.data as dpd
from dipy.io.image import load_nifti, save_nifti
from dipy.io.gradients import read_bvals_bvecs
from dipy.core.gradients import gradient_table
from dipy.viz import window, actor

import torch

class DipyDTI(base_inference_model.InferenceModel, object):
    """
    """
    def __init__(self, configObject):
        super(DipyDTI, self).__init__()
        self.__name__ = 'DIPY_DTI'
        self.__require_initial_guess__ = False
        self.args = configObject.args

    @ProgressBarWrap
    def update_bar(self, loss, args):
        return -1

    def forward(self, inputs):

        b_vecs = self.args.engine.signal_model.b_vecs
        b_vals = self.args.engine.signal_model.b_vals*1
        gtab = gradient_table(b_vals, b_vecs)

        data = torch.moveaxis(inputs[0], 0, -1).detach().numpy()        
        dti_wls = dti.TensorModel(gtab, fit_method="WLS", return_S0_hat=False)
        fit_wls = dti_wls.fit(data)
        
        
        l_triang_tensors = fit_wls.lower_triangular()
        estimates = torch.moveaxis(torch.Tensor(l_triang_tensors), -1, 0) 
        dxx = estimates[0]
        dyy = estimates[2]
        dzz = estimates[5]
        dxy = estimates[1]
        dxz = estimates[3]
        dyz = estimates[3]
        s0 = inputs[0][0]

        estimates_ = [s0, dxx, dxy, dxz, dyy, dyz, dzz]

        return torch.stack(estimates_)