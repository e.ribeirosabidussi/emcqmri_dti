
from desiutil.plots import prepare_data, init_sky, plot_grid_map, plot_healpix_map, plot_sky_circles, plot_sky_binned
from dipy.reconst.dti import lower_triangular, eig_from_lo_tri
from dipy.reconst.dti import fractional_anisotropy, color_fa, mean_diffusivity
import math as m
import matplotlib.pyplot as plt
# import matplotlib_inline
import matplotlib
import matplotlib.axes as maxes
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import numpy.linalg as linalg
import os
import pickle
from scipy.spatial.transform import Rotation as R
import sys

matplotlib.rc('font', family='serif', serif='cm10')
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']


def convert_tensor_param_to_mat(tensor_param):
    tensor_ = tensor_param
    reconstructed_tensor = np.zeros([10,10,3,3])
    reconstructed_tensor[:,:,0,0] = tensor_[0]#.detach().numpy()
    reconstructed_tensor[:,:,0,1] = tensor_[1]#.detach().numpy()
    reconstructed_tensor[:,:,0,2] = tensor_[2]#.detach().numpy()
    reconstructed_tensor[:,:,1,0] = tensor_[1]#.detach().numpy()
    reconstructed_tensor[:,:,1,1] = tensor_[3]#.detach().numpy()
    reconstructed_tensor[:,:,1,2] = tensor_[4]#.detach().numpy()
    reconstructed_tensor[:,:,2,0] = tensor_[2]#.detach().numpy()
    reconstructed_tensor[:,:,2,1] = tensor_[4]#.detach().numpy()
    reconstructed_tensor[:,:,2,2] = tensor_[5]#.detach().numpy()

    return reconstructed_tensor


def convert_tensor_mat_to_param(tensor_mat, voxel_first = False):
    if voxel_first:
        compressed_tensor = [tensor_mat[..., 0, 0], tensor_mat[..., 0, 1], tensor_mat[..., 0, 2], tensor_mat[..., 1, 1], tensor_mat[..., 1, 2], tensor_mat[..., 2, 2]]
    else:
        compressed_tensor = [tensor_mat[0, 0], tensor_mat[0, 1], tensor_mat[0, 2], tensor_mat[1, 1], tensor_mat[1, 2], tensor_mat[2, 2]]
    
    return np.array(compressed_tensor)



def rotate_tensor(tensor, angle):

    tensor_ = tensor[1:]
    reconstructed_tensor = convert_tensor_param_to_mat(tensor)

    rotation_degrees_z = angle
    rotation_radians = np.radians(rotation_degrees_z)
    rotation_axis = np.array([0, 0, 1])
    rotation_vector = rotation_radians * rotation_axis
    rotation = R.from_rotvec(rotation_vector)

    rotated_tensor = np.zeros([10,10,3,3])
    for ti, tensor_i in enumerate(reconstructed_tensor):
        for tj, tensor_j in enumerate(tensor_i):
            rotated_tensor[ti, tj] = np.dot(rotation.as_matrix(), np.dot(tensor_j,np.transpose(rotation.as_matrix())))

    return rotated_tensor
    # return convert_tensor_mat_to_param(rotated_tensor, voxel_first=True)


def rotate_matrix(mat, angle):
    
    rotation_degrees_z = angle
    rotation_radians = np.radians(rotation_degrees_z)
    rotation_axis = np.array([0, 0, 1])
    rotation_vector = rotation_radians * rotation_axis
    rotation = R.from_rotvec(rotation_vector)

    rotated_tensor = np.zeros([10,10,3,3])
    for ti, tensor_i in enumerate(mat):
        for tj, tensor_j in enumerate(tensor_i):
            rotated_tensor[ti, tj] = np.dot(rotation.as_matrix(), np.dot(tensor_j,np.transpose(rotation.as_matrix())))

    return rotated_tensor


def rotate_vector(vec, angle):
    rotation_degrees_z = angle
    rotation_radians = np.radians(rotation_degrees_z)
    rotation_axis = np.array([0, 0, 1])
    rotation_vector = rotation_radians * rotation_axis
    rotation = R.from_rotvec(rotation_vector)

    rotated_vec = np.dot(rotation.as_matrix(), vec)
    return rotated_vec



def build_d_mat(d_tensors):
    d_mat = np.array([[d_tensors[0], d_tensors[1], d_tensors[2]],
                     [d_tensors[1], d_tensors[3], d_tensors[4]],
                     [d_tensors[2], d_tensors[4], d_tensors[5]]])

    d_mat_perm = np.transpose(d_mat, (-2, -1, 0, 1))
    return d_mat_perm

def from_tensor_to_eig_v(tensors):
    d_mat = build_d_mat(np.stack(tensors))
    l_triang = lower_triangular(d_mat)
    eig_v = eig_from_lo_tri(l_triang, min_diffusivity=1e-6)
    e_val = eig_v[...,:3]
    e_vec = eig_v[...,3:12]
    sh_tensor = tensors.shape[-2:]
    e_vec = e_vec.reshape(*sh_tensor, 3, 3)
    return e_val, e_vec


def get_diffusion_tensor_from_fa_md(fa, md, eigen_vals):

    a = fa / np.sqrt(3-2 * fa * fa)
    dr = md * (1.0 - a)
    dp = md * (1.0 + 2.0 * a)

    tensor = np.zeros([3, 3, 10, 10])
    
    # print(((dp - dr) * eigen_vals[0]* eigen_vals[0]).shape, dp.shape, eigen_vals[0].shape)
    tensor[0][0] = dr + (dp - dr) * eigen_vals[0] * eigen_vals[0]
    tensor[1][1] = dr + (dp - dr) * eigen_vals[1] * eigen_vals[1]
    tensor[2][2] = dr + (dp - dr) * eigen_vals[2] * eigen_vals[2]
    tensor[0][1] = tensor[1][0] = (dp - dr) * eigen_vals[0] * eigen_vals[1]
    tensor[0][2] = tensor[2][0] = (dp - dr) * eigen_vals[0] * eigen_vals[2]
    tensor[1][2] = tensor[2][1] = (dp - dr) * eigen_vals[1] * eigen_vals[2]

    return tensor


def get_ad_rd_from_eigen_vals(fa, md, eig_vals):
    tensor = get_diffusion_tensor_from_fa_md(fa, md, eig_vals)



def get_eigen_val_rotation_exp():
    eig = np.zeros([3, 10, 10])
    eig[0,:,:] = 1
    eig[1,:,:] = 0
    eig[2,:,:] = 0

    return eig


def viz_direction_map(e_vecs, fa):
    color_bar = color_fa(fa, e_vecs)
    plt.figure()
    plt.imshow(fa, origin='lower', cmap='gray', alpha=0.8)
    plt.imshow(color_bar, origin='lower', alpha=0.8)
    plt.show()


def viz_fa_direction(fa_map, e_vec):
    viz_direction_map(e_vec, fa_map)


def create_plot(fig, ax, data):
    pass


def compute_error(data, gt):
    error = gt - data
    return error


def get_results_data(file_path, size_):
    
    with open(file_path, 'rb') as f:
        data = pickle.load(f)

    estimated_1_fa = data['fa_map']#[4,4]
    estimated_1_md = data['md_map']#[4,4]
    estimated_tensors = data['diffusion_tensors'][1:]

    return estimated_1_fa, estimated_1_md, estimated_tensors


def box_plot(data, edge_color, fill_color, positions_, ax):
    bp = ax.boxplot(data, positions=positions_,patch_artist=True, showfliers=False)
    
    for patch in bp['boxes']:
        patch.set(facecolor=fill_color, alpha=0.3)    
        
    for element in ['whiskers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)
   
        
    return bp


def determine_euler_angles(tensor):

    tol = sys.float_info.epsilon * 10
  
    if abs(tensor[0,0])< tol and abs(tensor[1,0]) < tol:
        eul1 = 0
        eul2 = m.atan2(-tensor[2,0], tensor[0,0])
        eul3 = m.atan2(-tensor[1,2], tensor[1,1])
    else:   
        eul1 = m.atan2(tensor[1,0],tensor[0,0])
        sp = m.sin(eul1)
        cp = m.cos(eul1)
        eul2 = m.atan2(-tensor[2,0],cp*tensor[0,0]+sp*tensor[1,0])
        eul3 = m.atan2(sp*tensor[0,2]-cp*tensor[1,2],cp*tensor[1,1]-sp*tensor[0,1])
    

    return np.degrees(eul1), np.degrees(eul2), np.degrees(eul3)


def plot_elipsoid(tensor=[], ax=[], color_=[], alpha_=[], type_=[]):
    A = tensor.copy()

    # find the rotation matrix and radii of the axes
    U, s, rotation = linalg.svd(A)
    radii = 1.0/np.sqrt(s)
    # rotation = A

    # now carry on with EOL's answer
    u = np.linspace(0.0, 2.0 * np.pi, 100)
    v = np.linspace(0.0, np.pi, 100)
    x = radii[0] * np.outer(np.cos(u), np.sin(v))
    y = radii[1] * np.outer(np.sin(u), np.sin(v))
    z = radii[2] * np.outer(np.ones_like(u), np.cos(v))
    for i in range(len(x)):
        for j in range(len(x)):
            [x[i,j],y[i,j],z[i,j]] = np.dot([x[i,j],y[i,j],z[i,j]], rotation)

    # plot
    if type_=='surface':
        ax.plot_surface(x, y, z,  rstride=4, cstride=4, color=color_, alpha=alpha_)
    elif type_=='wire':
        ax.plot_wireframe(x, y, z,  rstride=4, cstride=4, color=color_, alpha=alpha_)

    del x, y, z

    # Adjustment of the axes, so that they all have the same span:
    max_radius = 1.5
    for axis in 'xyz':
        getattr(ax, 'set_{}lim'.format(axis))((-max_radius, max_radius))


def asSpherical(xyz):
    #takes list xyz (single coord)
    x       = xyz[0]
    y       = xyz[1]
    z       = xyz[2]
    r       =  m.sqrt(x*x + y*y + z*z)
    theta   =  m.acos(z/r)*180/ m.pi #to degrees
    phi     =  m.atan2(y,x)*180/ m.pi
    return [r,theta,phi]


def get_spherical_coordinates_from_eigenvector(eig_v):
    main_eig_v = eig_v[:,0]
    r, theta, phi = asSpherical(main_eig_v)

    return theta, phi


def to_dyadic_form(eigenvector):
    main_eig_vec = eigenvector[None]
    dyadic_tensor = np.matmul(main_eig_vec.T, main_eig_vec)
    return dyadic_tensor


def get_angle_between_vectors(vec1, vec2):
    norm_vec1 = (vec1 / np.linalg.norm(vec1))
    norm_vec2 = (vec2 / np.linalg.norm(vec2))
    delta_angle = np.arccos(np.dot(norm_vec1, norm_vec2))
    delta_angle_degree = np.degrees(np.real(delta_angle))
    return delta_angle_degree


def get_eig_from_matrix(mat):
    return linalg.eig(mat)



def rotate_eigen_vals(eig_vals, rotation_matrix):
    x_ind = [3,4,5]
    y_ind = [3,4,5]
    rotated_ev = eig_vals.copy()
    for x in x_ind:
        for y in y_ind:
            rotated_ev[:,x,y] = np.dot(rotation_matrix, eig_vals[:,x,y])

    return rotated_ev


def box_plot(data, edge_color, fill_color, positions_, ax):
    bp = ax.boxplot(data, positions=positions_,patch_artist=True,
    flierprops={'marker': 'x', 'markersize': 4, 'markeredgecolor': fill_color, 'alpha': 0.1})
    
    for patch in bp['boxes']:
        patch.set(facecolor=fill_color, alpha=0.3)    
        
    for element in ['whiskers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)

    return bp


def cart2sph(coord):
    hxy = np.hypot(-coord[:,0], coord[:,1])
    phi_el = np.arctan(coord[:,2]/hxy)
    phi_xy = np.arctan2(-coord[:,0], coord[:,1])

    # phi_el[0] = 0
    # phi_xy[0] = 0

    return phi_xy, phi_el


def plot_mollweide_distribution_qspace(q_space, ax, all_phi, all_rho):
    nonzero = np.unique(np.nonzero(q_space)[0])
    new_qspace = q_space[nonzero]
    phi_qspace, rho_qspace = cart2sph(new_qspace)

    all_phi += list(phi_qspace)
    all_rho += list(rho_qspace)

    return phi_qspace, rho_qspace


def plot_std_circle_in_mollweide(ra_center_, dec_center_, scale, ax_):
    plot_sky_circles(ra_center=ra_center_,
                    dec_center=dec_center_,
                    field_of_view=scale, ax=ax_)




def gyroid(x, y, n, L, t):
    tanım1 = (sin(2*pi*n*x/L) * cos(2*pi*n*y/L) + sin(2*pi*n*y/L) + cos(2*pi*n*x/L))
    return tanım1*tanım1 - t**2

if __name__ == '__main__':


    from numpy import pi, cos, sin, linspace, meshgrid



    fig = plt.figure(figsize=(10, 7))
    fig.suptitle(r"\textbf{Estimation of fiber direction}")
    fig.subplots_adjust(
        top=1.0,
        bottom=0.332,
        left=0.134,
        right=0.837,
        hspace=0.15,
        wspace=0.121
    )
    ax = fig.add_subplot(111, projection="mollweide")
    

    box_plot_position = [[1.], [1.2]]
    fc_colors = ['gray', 'red']
    marker_ = ['o', 'o']
    
    data_path = "/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_8_orientation/correct_b0/3x3/arbitrary_rotation_repeated/"
            
    gt_fa = np.ones([10,10])*0.8
    gt_md = np.ones([10,10])*0.8
    
    gt_evals = get_eigen_val_rotation_exp()
    diffusion_tensor = get_diffusion_tensor_from_fa_md(gt_fa, gt_md, gt_evals)
    tensor_mat = convert_tensor_mat_to_param(diffusion_tensor)
    gt_e_val, gt_e_vec = from_tensor_to_eig_v(tensor_mat)
    # print(gt_e_val, gt_e_vec)
    # First, get the data (iterate each orientation, compute the mean dyadic tensor and compare with the ground-truth, compute the precision of these realizations)

    n_orientations = 10

    images_ = []
    for m_, method in enumerate(['/mrtrix', '/dtiRIM_no_background']):
        per_orientation_est = []
        per_orientation_gt = []
        std_est = []
        for orientation in range(n_orientations):
            folder_ = data_path + "rotation_" + str(orientation+1) + method
            list_dir_estimates = [f for f in (os.listdir(folder_)) if f.endswith(".pkl") and ('matrix') not in f]
            list_dir_rotations = [f for f in (os.listdir(folder_)) if f.endswith(".pkl") and ('matrix') in f]

            estimated_evec = []
            rotated_evec_gt = []
            angle_error_list = []

            dyadic_est = []
            dyadic_gt = []
            for f_est, f_rot in zip(list_dir_estimates, list_dir_rotations):
                path_file_est = folder_ + "/" + f_est
                path_file_rot = folder_ + "/" + f_rot

                est_fa, est_md, est_tensor = get_results_data(path_file_est, method)
                with open(path_file_rot, 'rb') as f:
                    rotation_matrix = pickle.load(f)

                estimated_e_val, estimated_e_vec = from_tensor_to_eig_v(est_tensor.detach().numpy())
                ##### This is to apply same rotation to gt tensors
                rotated_gt_evec = rotate_eigen_vals(gt_e_vec.transpose(-1,-2,0,1)[0], rotation_matrix)[:,4,4]
                est_e_vec = estimated_e_vec[4,4,:,0] * np.sign(estimated_e_vec[4,4,:,0]/rotated_gt_evec)

                dyadic_eig_est = to_dyadic_form(est_e_vec)
                dyadic_est.append(dyadic_eig_est)

                estimated_evec.append(est_e_vec)
                rotated_evec_gt.append(rotated_gt_evec)

            mean_dyadic_est = np.mean(dyadic_est, 0)
            mean_dyadic_eigval_est, mean_dyadic_eigvec_est = get_eig_from_matrix(mean_dyadic_est)
            mean_dy_est = mean_dyadic_eigvec_est[:,0] * np.sign(mean_dyadic_eigvec_est[:,0]/rotated_gt_evec)
            mean_angle_bias = get_angle_between_vectors(rotated_gt_evec, mean_dy_est)

            std_estim_evec = np.std(estimated_evec,0)
            
            all_angle_std = []
            for d_j in dyadic_est:
                eig_val, eig_dyat = get_eig_from_matrix(d_j)
                an_ = get_angle_between_vectors(np.abs(eig_dyat[:,np.argmax(eig_val)]), np.abs(mean_dyadic_eigvec_est[:,0]))
                all_angle_std.append(an_)

            std_estim = np.sqrt(np.mean(np.array(all_angle_std)))
            sorted_diff = np.sort(all_angle_std)
            print("Method: {}, Orientation: {}, Mean error: {}, 95th percentile: {}".format(method, orientation, mean_angle_bias, std_estim))

            per_orientation_est.append(mean_dy_est)
            per_orientation_gt.append(rotated_gt_evec)
            std_est.append(std_estim)
            # MOLLWEIDE
        
        sorted_std_est = np.sort(std_est)

        all_phi, all_rho = [],[]
        phi_qspace, rho_qspace = plot_mollweide_distribution_qspace(np.array(per_orientation_est), ax, all_phi, all_rho)

        for phi_,rho_,std_ in zip(phi_qspace, rho_qspace, std_est):
            r_std = np.radians(3*std_)
            circle1 = plt.Circle((phi_, rho_), r_std, color=fc_colors[m_], alpha=0.3)
            ax.add_patch(circle1)

        im = ax.scatter(phi_qspace, rho_qspace, c=fc_colors[m_], s=2, alpha=1, marker=marker_[m_])

        all_phi_gt, all_rho_gt = [],[]
        phi_qspace, rho_qspace = plot_mollweide_distribution_qspace(np.array(per_orientation_gt), ax, all_phi_gt, all_rho_gt)
        ax.scatter(phi_qspace, rho_qspace, c='black', s=20, alpha=0.3, marker='x')
    
    mrtrix_patch = Line2D(range(1), range(1), color="white", marker='o', markersize=12, markerfacecolor="gray", alpha=0.7, label=r'$\mathit{\mathbf{IRLLS}}$')
    aRIM_patch = Line2D(range(1), range(1), color="white", marker='o', markersize=12, markerfacecolor="red", alpha=0.7, label=r'dti$\mathit{\mathbf{RIM}}$')
    gt_patch = Line2D(range(1), range(1), color="black", marker='x', markersize=4, markerfacecolor="black", label=r'Ground-truth fiber direction', linestyle='None')
    method_legend = plt.legend(handles=[mrtrix_patch, aRIM_patch, gt_patch], bbox_to_anchor=(0.5, -0.115), loc='center', ncol=1)

    # handles, labels = im.legend_elements(prop="sizes", alpha=0.4, num=4, color='red')
    # labels = [str(np.round(np.min(np.array(sorted_std_est)),2)), str(np.round(np.array(sorted_std_est)[30],2)), str(np.round(np.array(sorted_std_est)[60],2)), str(np.round(np.max(np.array(sorted_std_est)),2))]
    # legend = ax.legend(handles, labels, ncol=1,loc="lower center", title=r"Standard deviation $(^{\circ})$", bbox_to_anchor=(1.1, 0.35), frameon=False)
    # plt.gca().add_artist(method_legend)

    ax.grid(color='gray', linestyle='--', linewidth=0.35, alpha=0.35)
    plt.show()
    plt.tight_layout()






    if 0:
        if __name__ == '__main__':

            colors = ['gray', 'red']
            fig_, ax_ = plt.subplots(1, 1, figsize=(4,4))
            fig_.subplots_adjust(
                top=0.9,
                bottom=0.14,
                left=0.09,
                right=0.945,
                hspace=0.2,
                wspace=0.355
            )


            fig = plt.figure(figsize=(15, 7))
            fig.suptitle(r"\textbf{Error over all eigenvalue directions}")
            fig.subplots_adjust(
                top=1.0,
                bottom=0.0,
                left=0.035,
                right=0.97,
                hspace=0.16,
                wspace=0.131
            )
            ax = fig.add_subplot(111, projection="mollweide")#, fig.add_subplot(122, projection="mollweide")]

            box_plot_position = [[1.], [1.2]]
            fc_colors = ['gray', 'red']
            marker_ = ['o', 'v']

            data_path = "/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_8_orientation/correct_b0/3x3/arbitrary_rotation_repeated/"
            
            gt_fa = np.ones([10,10])*0.8
            gt_md = np.ones([10,10])*0.8
            
            gt_evals = get_eigen_val_rotation_exp()
            diffusion_tensor = get_diffusion_tensor_from_fa_md(gt_fa, gt_md, gt_evals)
            tensor_mat = convert_tensor_mat_to_param(diffusion_tensor)
            gt_e_val, gt_e_vec = from_tensor_to_eig_v(tensor_mat)

            for m_, method in enumerate(['/mrtrix', '/dtiRIM_no_background']):

                folder_ = data_path + "correct_b0/3x3/arbitrary_rotation" + method
                list_dir_estimates = [f for f in (os.listdir(folder_)) if f.endswith(".pkl") and ('matrix') not in f]
                list_dir_rotations = [f for f in (os.listdir(folder_)) if f.endswith(".pkl") and ('matrix') in f]

                estimated_evec = []
                rotated_evec_gt = []
                angle_error_list = []
                for f_est, f_rot in zip(list_dir_estimates, list_dir_rotations):
                    path_file_est = folder_ + "/" + f_est
                    path_file_rot = folder_ + "/" + f_rot

                    est_fa, est_md, est_tensor = get_results_data(path_file_est, method)
                    estimated_e_val, estimated_e_vec = from_tensor_to_eig_v(est_tensor.detach().numpy())

                    with open(path_file_rot, 'rb') as f:
                        rotation_matrix = pickle.load(f)

                    ##### This is to apply same rotation to gt tensors
                    rotated_gt_evec = rotate_eigen_vals(gt_e_vec.transpose(-1,-2,0,1)[0], rotation_matrix)
                    estimated_e_vec[4,4,:,0] *= np.sign(estimated_e_vec[4,4,:,0]/rotated_gt_evec[:,4,4])

                    angle_bias = get_angle_between_vectors((estimated_e_vec[4,4,:,0]), (rotated_gt_evec[:,4,4]))
                    angle_error_list.append(angle_bias)


                    estimated_evec.append(estimated_e_vec[4,4,:,0])
                    rotated_evec_gt.append(rotated_gt_evec[:,4,4])
                
                mean_angle_bias = np.mean(angle_error_list)
                std_angle_bias = np.sort(angle_error_list)[94]


                print("Mean: {}, 95th percentile: {}".format(mean_angle_bias, std_angle_bias))

                ax_.errorbar(box_plot_position[m_], mean_angle_bias, yerr=std_angle_bias, marker='x', linestyle='', color=fc_colors[m_], alpha=1)
                ax_.axhline(0.0, linestyle='--', color='black', linewidth=0.3)
                ax_.set_xticks([1.0, 1.2])
                ax_.set_xticklabels(['', ''])
                ax_.set_ylabel(r"Error in estimation of fiber orientation ($^{\circ}$)")

                # MOLLWEIDE
                all_phi_gt, all_rho_gt = [],[]
                phi_qspace, rho_qspace = plot_mollweide_distribution_qspace(np.array(rotated_evec_gt), ax, all_phi_gt, all_rho_gt)
                ax.scatter(phi_qspace, rho_qspace, c='black', s=20, alpha=0.3, marker='x')

                all_phi, all_rho = [],[]
                phi_qspace, rho_qspace = plot_mollweide_distribution_qspace(np.array(estimated_evec), ax, all_phi, all_rho)
                im= ax.scatter(phi_qspace, rho_qspace, c=angle_error_list, s=np.array(angle_error_list)*60, cmap='hot', alpha=0.5, marker=marker_[m_])
                ax.grid(color='gray', linestyle='--', linewidth=0.35, alpha=0.35)





            mrtrix_patch = mpatches.Patch(facecolor='gray', edgecolor='black', label=r'$\mathit{\mathbf{IRLLS}}$')
            aRIM_patch = mpatches.Patch(facecolor='red', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$')
            plt.legend(handles=[mrtrix_patch, aRIM_patch])#, bbox_to_anchor=(0.5, -0.55), loc='center')

            plt.show()
