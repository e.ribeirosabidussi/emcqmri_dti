import utilities

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import os
import pickle
import sys

matplotlib.rc('font', family='serif', serif='cm10')
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']
sys.path.insert(0, '../../')



if __name__ == '__main__':
    data_path = "/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_5/"
    fa_list = np.array([0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.0])
    md_list = np.array([0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.0])

    eig_val = utilities.get_gt_eigen_val()
    gt_fa = fa_list.copy()
    gt_md = fa_list.copy()
    gt_fa_fixed = 0.8
    gt_md_fixed = 0.8
    
    hr_list = np.arange(0.05,1,0.005)
    ad_list = []
    rd_list = []
    for hr in hr_list:
        gt_ad, gt_rd = utilities.get_gtound_truth_ad_rd_from_eigen_vals(hr, gt_md_fixed, eig_val)
        ad_list.append(gt_ad)
        rd_list.append(gt_rd)

    colors = ['gray', 'red', 'purple']

    # Plot FA first
    fig, ax = plt.subplots(2, 2, figsize=(8,8))
    fig.subplots_adjust(
        top=0.9,
        bottom=0.14,
        left=0.09,
        right=0.945,
        hspace=0.2,
        wspace=0.355
    )

    for m_, method in enumerate(['/dtiRIM_no_background', '/dtiRIM_no_background_new_loss']):
        mean_bias_fa_fa_ = []
        std_bias_fa_fa_ = []

        mean_bias_md_fa_ = []
        std_bias_md_fa_ = []

        mean_bias_ad_fa_ = []
        std_bias_ad_fa_ = []

        mean_bias_rd_fa_ = []
        std_bias_rd_fa_ = []
        sn_ = 0

        for fa_gt in fa_list:
            folder_ = data_path + "fa/correct_b0/fa_" + str(fa_gt) + method
            
            list_dir = [f for f in (os.listdir(folder_)) if f.endswith(".pkl")]

            total_error_fa = []
            total_error_md = []

            total_error_ad = []
            total_error_rd = []
            for fi, f_ in enumerate(list_dir):

                if fi ==100:
                    break

                path_file = folder_ + "/" + f_
                print("read file: {}".format(path_file))
                path_file = folder_ + "/" + f_
                print("read file: {}".format(path_file))
                est_fa, est_md, est_ad, est_rd, est_ev = utilities.get_results_data(path_file, 4, 4)
                if method == "/mrtrix":
                    est_md *= 1000
                    est_ev *= 1000
                    est_ad *= 1000
                    est_rd *= 1000

                total_error_fa.append(est_fa)
                total_error_md.append(est_md)
                total_error_ad.append(est_ad)
                total_error_rd.append(est_rd)

            mean_bias_fa_fa_.append(np.mean(total_error_fa, 0))
            std_bias_fa_fa_.append(np.std(total_error_fa,0))

            mean_bias_md_fa_.append(np.mean(total_error_md, 0))
            std_bias_md_fa_.append(np.std(total_error_md,0))

            mean_bias_ad_fa_.append(np.mean(total_error_ad, 0))
            std_bias_ad_fa_.append(np.std(total_error_ad,0))

            mean_bias_rd_fa_.append(np.mean(total_error_rd, 0))
            std_bias_rd_fa_.append(np.std(total_error_rd,0))

        # How FA varies with FA
        utilities.plot_comparison(fa_list, mean_bias_fa_fa_, std_bias_fa_fa_, colors[m_], 
                                  0.2, -1, fig, ax[0,0], r"FA of the sample", r"Estimated FA", 
                                  fa_list[::3], "FA dependency on sample FA")
        ax[0,0].plot(fa_list, fa_list, linestyle='--', color='black', linewidth=1)

        # How MD varies with FA
        utilities.plot_comparison(fa_list, mean_bias_md_fa_, std_bias_md_fa_, colors[m_], 
                                  0.2, gt_fa_fixed, fig, ax[0,1], r"FA of the sample", r"Estimated MD $[mm^2/ms]$", 
                                  fa_list[::3], "MD dependency on sample FA")

        # How AD varies with FA
        # utilities.plot_comparison(fa_list, mean_bias_ad_fa_, std_bias_ad_fa_, colors[m_], 
        #                           0.2, -1, fig, ax[0,2], r"FA of the sample", r"Estimated AD $[mm^2/ms]$", 
        #                           fa_list[::3], "AD dependency on sample FA")
        # ax[0,2].plot(hr_list, ad_list, linestyle='--', color='black', linewidth=1)

        # # How RD varies with FA
        # utilities.plot_comparison(fa_list, mean_bias_rd_fa_, std_bias_rd_fa_, colors[m_], 
        #                           0.2, -1, fig, ax[0,3], r"FA of the sample", r"Estimated RD $[mm^2/ms]$", 
        #                           fa_list[::3], "RD dependency on sample FA")
        # ax[0,3].plot(hr_list, rd_list, linestyle='--', color='black', linewidth=1)
        


    ad_list = []
    rd_list = []
    for hr in hr_list:
        gt_ad, gt_rd = utilities.get_gtound_truth_ad_rd_from_eigen_vals(gt_fa_fixed, hr, eig_val)
        ad_list.append(gt_ad)
        rd_list.append(gt_rd)

    for m_, method in enumerate(['/dtiRIM_no_background', '/dtiRIM_no_background_new_loss']):
        mean_bias_fa_md_ = []
        std_bias_fa_md_ = []
        mean_bias_md_md_ = []
        std_bias_md_md_ = []
        mean_bias_ad_md_ = []
        std_bias_ad_md_ = []
        mean_bias_rd_md_ = []
        std_bias_rd_md_ = []
        sn_ = 0

        for md_gt in md_list:
            folder_ = data_path + "md/correct_b0/md_" + str(md_gt) + method
            list_dir = [f for f in (os.listdir(folder_)) if f.endswith(".pkl")]

            total_error_fa = []
            total_error_md = []
            total_error_ad = []
            total_error_rd = []
            for fi, f_ in enumerate(list_dir):

                if fi ==100:
                    break

                path_file = folder_ + "/" + f_
                print("read file: {}".format(path_file))
                est_fa, est_md, est_ad, est_rd, est_ev = utilities.get_results_data(path_file, 4, 4)
                if method == "/mrtrix":
                    est_md *= 1000
                    est_ev *= 1000
                    est_ad *= 1000
                    est_rd *= 1000     

                total_error_fa.append(est_fa)
                total_error_md.append(est_md)
                total_error_ad.append(est_ad)
                total_error_rd.append(est_rd)

            mean_bias_fa_md_.append(np.mean(total_error_fa, 0))
            std_bias_fa_md_.append(np.std(total_error_fa,0))

            mean_bias_md_md_.append(np.mean(total_error_md, 0))
            std_bias_md_md_.append(np.std(total_error_md,0))

            mean_bias_ad_md_.append(np.mean(total_error_ad, 0))
            std_bias_ad_md_.append(np.std(total_error_ad,0))

            mean_bias_rd_md_.append(np.mean(total_error_rd, 0))
            std_bias_rd_md_.append(np.std(total_error_rd,0))


        # How FA varies with MD
        utilities.plot_comparison(md_list, mean_bias_fa_md_, std_bias_fa_md_, colors[m_], 
                                0.2, 0.8, fig, ax[1,0], r"MD of the sample", r"Estimated FA", 
                                md_list[::3], "FA dependency on sample MD")
        
        # How MD varies with MD
        utilities.plot_comparison(md_list, mean_bias_md_md_, std_bias_md_md_, colors[m_], 
                                0.2, -1, fig, ax[1,1], r"MD of the sample", r"Estimated MD $[mm^2/ms]$", 
                                md_list[::3], "MD dependency on sample MD")
        ax[1,1].plot(md_list, md_list, linestyle='--', color='black', linewidth=1)

        # How AD varies with MD
        # utilities.plot_comparison(md_list, mean_bias_ad_md_, std_bias_ad_md_, colors[m_], 
        #                           0.2, -1, fig, ax[1,2], r"MD of the sample", r"Estimated AD $[mm^2/ms]$", 
        #                           md_list[::3], "AD dependency on sample MD")
        # ax[1,2].plot(hr_list, ad_list, linestyle='--', color='black', linewidth=1)

        # # How RD varies with MD
        # utilities.plot_comparison(md_list, mean_bias_rd_md_, std_bias_rd_md_, colors[m_], 
        #                           0.2, -1, fig, ax[1,3], r"MD of the sample", r"Estimated RD $[mm^2/ms]$", 
        #                           md_list[::3], "RD dependency on sample MD")
        # ax[1,3].plot(hr_list, rd_list, linestyle='--', color='black', linewidth=1)


    mrtrix_patch = mpatches.Patch(facecolor='gray', edgecolor='black', label=r'$\mathit{\mathbf{IRLLS}}$')
    aRIM_patch = mpatches.Patch(facecolor='red', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$')
    plt.legend(handles=[mrtrix_patch, aRIM_patch])

    plt.show()






