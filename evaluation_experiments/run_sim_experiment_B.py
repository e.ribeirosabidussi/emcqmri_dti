import utilities

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import os
import pickle

matplotlib.rc('font', family='serif', serif='cm10')
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']

def get_results_data(file_path):
    
    with open(file_path, 'rb') as f:
        data = pickle.load(f)
    estimated_tensor = data["diffusion_tensors"]#[:,5,5]

    ind_x = 3
    ind_y = 5

    e_val, e_vec = utilities.from_tensor_to_eig_v(estimated_tensor[1:])
    estimated_ad_map = utilities.axial_diffusivity_(e_val)[3, 3]
    estimated_rd_map = utilities.radial_diffusivity_(e_val)[ind_x, ind_y]
    estimated_md_map = utilities.mean_diffusivity_(e_val)[4, 3]
    estimated_fa_map = utilities.fractional_anisotropy_(e_val)[ind_x, ind_y]

    return estimated_fa_map, estimated_md_map, estimated_ad_map, estimated_rd_map, np.array(estimated_tensor[:, ind_x, ind_y].detach().numpy())


if __name__ == '__main__':
    data_path = "/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_4/"
    bval_list = [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2]

    eig_val = utilities.get_gt_eigen_val()
    gt_fa = 0.8
    gt_md = 0.8
    gt_ad, gt_rd = utilities.get_gtound_truth_ad_rd_from_eigen_vals(gt_fa, gt_md, eig_val)

    colors = ['gray', 'red', 'purple']

    fig, ax = plt.subplots(1, 2, figsize=(8,4))
    # fig.suptitle("Experiment 4")
    fig.subplots_adjust(
        top=0.9,
        bottom=0.14,
        left=0.09,
        right=0.945,
        hspace=0.2,
        wspace=0.355
    )

    for m_, method in enumerate(['/mrtrix', '/dtiRIM_no_background_new_loss']):
        mean_bias_fa_x_ = []
        std_bias_fa_x_ = []
        mean_bias_md_x_ = []
        std_bias_md_x_ = []
        mean_bias_ad_x_ = []
        std_bias_ad_x_ = []
        mean_bias_rd_x_ = []
        std_bias_rd_x_ = []

        for bval in bval_list:
            folder_ = data_path + "correct_b0/bval_" + str(bval) + method
            list_dir = [f for f in (os.listdir(folder_)) if f.endswith(".pkl")]


            total_error_fa = []
            total_error_md = []
            total_error_ad = []
            total_error_rd = []
            for fi, f_ in enumerate(list_dir):
                if fi ==100:
                    break
                
                path_file = folder_ + "/" + f_
                print("read file: {}".format(path_file))
                path_file = folder_ + "/" + f_
                print("read file: {}".format(path_file))
                est_fa, est_md, est_ad, est_rd, est_ev = get_results_data(path_file)
                if method == "/mrtrix":
                    est_md *= 1000
                    est_ev *= 1000
                    est_ad *= 1000
                    est_rd *= 1000

                fa_error = est_fa
                md_error = est_md
                ad_error = est_ad
                rd_error = est_rd

                total_error_fa.append(fa_error)
                total_error_md.append(md_error)
                total_error_ad.append(ad_error)
                total_error_rd.append(rd_error)
            
            mean_bias_fa_x_.append(np.mean(total_error_fa, 0))
            std_bias_fa_x_.append(np.std(total_error_fa,0))
            mean_bias_md_x_.append(np.mean(total_error_md, 0))
            std_bias_md_x_.append(np.std(total_error_md,0))
            mean_bias_ad_x_.append(np.mean(total_error_ad, 0))
            std_bias_ad_x_.append(np.std(total_error_ad,0))
            mean_bias_rd_x_.append(np.mean(total_error_rd, 0))
            std_bias_rd_x_.append(np.std(total_error_rd,0))

        utilities.plot_comparison(bval_list, mean_bias_fa_x_, std_bias_fa_x_, colors[m_], 
                                  0.2, gt_fa, fig, ax[0], r"$b$ $[s/mm^2]$", r"Estimated FA", 
                                  bval_list, r"FA dependency on gradient strength $b$")

        utilities.plot_comparison(bval_list, mean_bias_md_x_, std_bias_md_x_, colors[m_], 
                                  0.2, gt_md, fig, ax[1], r"$b$ $[s/mm^2]$", r"Estimated MD $[mm^2/ms]$", 
                                  bval_list, r"MD dependency on gradient strength $b$")

        # utilities.plot_comparison(bval_list, mean_bias_ad_x_, std_bias_ad_x_, colors[m_], 
        #                           0.2, gt_ad, fig, ax[1,0], r"$b$ $[s/mm^2]$", r"Estimated AD $[mm^2/ms]$", 
        #                           bval_list, r"AD dependency on gradient strength $b$")

        # utilities.plot_comparison(bval_list, mean_bias_rd_x_, std_bias_rd_x_, colors[m_], 
        #                           0.2, gt_rd, fig, ax[1,1], r"$b$ $[s/mm^2]$", r"Estimated RD $[mm^2/ms]$", 
        #                           bval_list, r"RD dependency on gradient strength $b$")

        # ax[0].plot(bval_list, mean_bias_fa_bval, color=colors[m_], marker = 'x', linewidth=1.25, markersize=3.5)
        # ax[0].fill_between(bval_list, (np.array(mean_bias_fa_bval)-np.array(std_bias_fa_bval)), (np.array(mean_bias_fa_bval)+np.array(std_bias_fa_bval)), color=colors[m_], alpha=.2)
        # ax[0].axhline(0.8, linestyle='--', color='black', linewidth=1)

        # ax[1].plot(bval_list, mean_bias_md_bval, color=colors[m_], marker = 'x', linewidth=1.25, markersize=3.5)
        # ax[1].fill_between(bval_list, (np.array(mean_bias_md_bval)-np.array(std_bias_md_bval)), (np.array(mean_bias_md_bval)+np.array(std_bias_md_bval)), color=colors[m_], alpha=.2)
        # ax[1].axhline(0.8, linestyle='--', color='black', linewidth=1)


        # ax[0].set_xlabel(r"$b$ $[s/mm^2]$")
        # ax[0].set_ylabel(r"Estimated FA")
        # ax[0].set_xticks(bval_list)
        # ax[0].title.set_text(r"FA dependency on gradient strength $b$")
        # ax[1].set_xlabel(r"$b$ $[s/mm^2]$")
        # ax[1].set_ylabel(r"Estimated MD $[mm^2/ms]$")
        # ax[1].set_xticks(bval_list)
        # ax[1].title.set_text(r"MD dependency on gradient strength $b$")

    mrtrix_patch = mpatches.Patch(facecolor='gray', edgecolor='black', label=r'$\mathit{\mathbf{IRLLS}}$')
    aRIM_patch = mpatches.Patch(facecolor='red', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$')
    plt.legend(handles=[mrtrix_patch, aRIM_patch])

    plt.show()






