

from dipy.reconst.dti import lower_triangular, eig_from_lo_tri
from dipy.reconst.dti import fractional_anisotropy, color_fa, mean_diffusivity

import math as m
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import numpy.linalg as linalg
import os
import pickle
from scipy.spatial.transform import Rotation as R
import sys

matplotlib.rc('font', family='serif', serif='cm10')
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']


def convert_tensor_param_to_mat(tensor_param):
    tensor_ = tensor_param
    reconstructed_tensor = np.zeros([10,10,3,3])
    reconstructed_tensor[:,:,0,0] = tensor_[0]#.detach().numpy()
    reconstructed_tensor[:,:,0,1] = tensor_[1]#.detach().numpy()
    reconstructed_tensor[:,:,0,2] = tensor_[2]#.detach().numpy()
    reconstructed_tensor[:,:,1,0] = tensor_[1]#.detach().numpy()
    reconstructed_tensor[:,:,1,1] = tensor_[3]#.detach().numpy()
    reconstructed_tensor[:,:,1,2] = tensor_[4]#.detach().numpy()
    reconstructed_tensor[:,:,2,0] = tensor_[2]#.detach().numpy()
    reconstructed_tensor[:,:,2,1] = tensor_[4]#.detach().numpy()
    reconstructed_tensor[:,:,2,2] = tensor_[5]#.detach().numpy()

    return reconstructed_tensor


def convert_tensor_mat_to_param(tensor_mat, voxel_first = False):
    if voxel_first:
        compressed_tensor = [tensor_mat[..., 0, 0], tensor_mat[..., 0, 1], tensor_mat[..., 0, 2], tensor_mat[..., 1, 1], tensor_mat[..., 1, 2], tensor_mat[..., 2, 2]]
    else:
        compressed_tensor = [tensor_mat[0, 0], tensor_mat[0, 1], tensor_mat[0, 2], tensor_mat[1, 1], tensor_mat[1, 2], tensor_mat[2, 2]]
    
    return np.array(compressed_tensor)



def rotate_tensor(tensor, angle):

    tensor_ = tensor[1:]
    reconstructed_tensor = convert_tensor_param_to_mat(tensor)

    rotation_degrees_z = angle
    rotation_radians = np.radians(rotation_degrees_z)
    rotation_axis = np.array([0, 0, 1])
    rotation_vector = rotation_radians * rotation_axis
    rotation = R.from_rotvec(rotation_vector)

    rotated_tensor = np.zeros([10,10,3,3])
    for ti, tensor_i in enumerate(reconstructed_tensor):
        for tj, tensor_j in enumerate(tensor_i):
            rotated_tensor[ti, tj] = np.matmul(rotation.as_matrix(), np.matmul(tensor_j,np.transpose(rotation.as_matrix())))

    return rotated_tensor
    # return convert_tensor_mat_to_param(rotated_tensor, voxel_first=True)


def rotate_matrix(mat, angle):
    
    rotation_degrees_z = angle
    rotation_radians = np.radians(rotation_degrees_z)
    rotation_axis = np.array([0, 0, 1])
    rotation_vector = rotation_radians * rotation_axis
    rotation = R.from_rotvec(rotation_vector)

    rotated_tensor = np.zeros([10,10,3,3])
    for ti, tensor_i in enumerate(mat):
        for tj, tensor_j in enumerate(tensor_i):
            rotated_tensor[ti, tj] = np.matmul(rotation.as_matrix(), np.matmul(tensor_j,np.transpose(rotation.as_matrix())))

    return rotated_tensor


def rotate_vector(vec, angle):
    rotation_degrees_z = angle
    rotation_radians = np.radians(rotation_degrees_z)
    rotation_axis = np.array([0, 0, 1])
    rotation_vector = rotation_radians * rotation_axis
    rotation = R.from_rotvec(rotation_vector)

    rotated_vec = np.matmul(vec, rotation.as_matrix())
    return rotated_vec



def build_d_mat(d_tensors):
    d_mat = np.array([[d_tensors[0], d_tensors[1], d_tensors[2]],
                     [d_tensors[1], d_tensors[3], d_tensors[4]],
                     [d_tensors[2], d_tensors[4], d_tensors[5]]])

    d_mat_perm = np.transpose(d_mat, (-2, -1, 0, 1))
    return d_mat_perm

def from_tensor_to_eig_v(tensors):
    d_mat = build_d_mat(np.stack(tensors))
    l_triang = lower_triangular(d_mat)
    eig_v = eig_from_lo_tri(l_triang, min_diffusivity=1e-6)
    e_val = eig_v[...,:3]
    e_vec = eig_v[...,3:12]
    sh_tensor = tensors.shape[-2:]
    e_vec = e_vec.reshape(*sh_tensor, 3, 3)

    return e_val, e_vec


def get_diffusion_tensor_from_fa_md(fa, md, eigen_vals):

    a = fa / np.sqrt(3-2 * fa * fa)
    dr = md * (1.0 - a)
    dp = md * (1.0 + 2.0 * a)

    tensor = np.zeros([3, 3, 10, 10])
    
    # print(((dp - dr) * eigen_vals[0]* eigen_vals[0]).shape, dp.shape, eigen_vals[0].shape)
    tensor[0][0] = dr + (dp - dr) * eigen_vals[0] * eigen_vals[0]
    tensor[1][1] = dr + (dp - dr) * eigen_vals[1] * eigen_vals[1]
    tensor[2][2] = dr + (dp - dr) * eigen_vals[2] * eigen_vals[2]
    tensor[0][1] = tensor[1][0] = (dp - dr) * eigen_vals[0] * eigen_vals[1]
    tensor[0][2] = tensor[2][0] = (dp - dr) * eigen_vals[0] * eigen_vals[2]
    tensor[1][2] = tensor[2][1] = (dp - dr) * eigen_vals[1] * eigen_vals[2]

    return tensor

def get_eigen_val_rotation_exp():
    eig = np.zeros([3, 10, 10])
    eig[0,:,:] = 1
    eig[1,:,:] = 0
    eig[2,:,:] = 0

    # eig[0,4,:] = 0
    # eig[1,4,:] = 1
    # eig[2,4,:] = 0

    # eig[0,5,:] = 0
    # eig[1,5,:] = 1
    # eig[2,5,:] = 0

    return eig


def viz_direction_map(e_vecs, fa):
    color_bar = color_fa(fa, e_vecs)
    plt.figure()
    plt.imshow(fa, origin='lower', cmap='gray', alpha=0.8)
    plt.imshow(color_bar, origin='lower', alpha=0.8)
    plt.show()


def viz_fa_direction(fa_map, e_vec):
    viz_direction_map(e_vec, fa_map)


def create_plot(fig, ax, data):
    pass


def compute_error(data, gt):
    error = gt - data
    return error


def get_results_data(file_path, size_):
    
    with open(file_path, 'rb') as f:
        data = pickle.load(f)

    estimated_1_fa = data['fa_map']#[4,4]
    estimated_1_md = data['md_map']#[4,4]
    estimated_tensors = data['diffusion_tensors'][1:]

    return estimated_1_fa, estimated_1_md, estimated_tensors


def box_plot(data, edge_color, fill_color, positions_, ax):
    bp = ax.boxplot(data, positions=positions_,patch_artist=True, showfliers=False)
    
    for patch in bp['boxes']:
        patch.set(facecolor=fill_color, alpha=0.3)    
        
    for element in ['whiskers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)
   
        
    return bp


def determine_euler_angles(tensor):

    tol = sys.float_info.epsilon * 10
  
    if abs(tensor[0,0])< tol and abs(tensor[1,0]) < tol:
        eul1 = 0
        eul2 = m.atan2(-tensor[2,0], tensor[0,0])
        eul3 = m.atan2(-tensor[1,2], tensor[1,1])
    else:   
        eul1 = m.atan2(tensor[1,0],tensor[0,0])
        sp = m.sin(eul1)
        cp = m.cos(eul1)
        eul2 = m.atan2(-tensor[2,0],cp*tensor[0,0]+sp*tensor[1,0])
        eul3 = m.atan2(sp*tensor[0,2]-cp*tensor[1,2],cp*tensor[1,1]-sp*tensor[0,1])
    

    return np.degrees(eul1), np.degrees(eul2), np.degrees(eul3)


def plot_elipsoid(tensor=[], ax=[], color_=[], alpha_=[], type_=[]):
    A = tensor.copy()

    # find the rotation matrix and radii of the axes
    U, s, rotation = linalg.svd(A)
    radii = 1.0/np.sqrt(s)
    # rotation = A

    # now carry on with EOL's answer
    u = np.linspace(0.0, 2.0 * np.pi, 100)
    v = np.linspace(0.0, np.pi, 100)
    x = radii[0] * np.outer(np.cos(u), np.sin(v))
    y = radii[1] * np.outer(np.sin(u), np.sin(v))
    z = radii[2] * np.outer(np.ones_like(u), np.cos(v))
    for i in range(len(x)):
        for j in range(len(x)):
            [x[i,j],y[i,j],z[i,j]] = np.dot([x[i,j],y[i,j],z[i,j]], rotation)

    # plot
    if type_=='surface':
        ax.plot_surface(x, y, z,  rstride=4, cstride=4, color=color_, alpha=alpha_)
    elif type_=='wire':
        ax.plot_wireframe(x, y, z,  rstride=4, cstride=4, color=color_, alpha=alpha_)

    del x, y, z

    # Adjustment of the axes, so that they all have the same span:
    max_radius = 1.5
    for axis in 'xyz':
        getattr(ax, 'set_{}lim'.format(axis))((-max_radius, max_radius))


def asSpherical(xyz):
    #takes list xyz (single coord)
    x       = xyz[0]
    y       = xyz[1]
    z       = xyz[2]
    r       =  m.sqrt(x*x + y*y + z*z)
    theta   =  m.acos(z/r)*180/ m.pi #to degrees
    phi     =  m.atan2(y,x)*180/ m.pi
    return [r,theta,phi]


def get_spherical_coordinates_from_eigenvector(eig_v):
    main_eig_v = eig_v[:,0]
    r, theta, phi = asSpherical(main_eig_v)

    return theta, phi


def to_dyadic_form(eigenvector):
    main_eig_vec = eigenvector[None]
    dyadic_tensor = np.matmul(main_eig_vec.T, main_eig_vec)
    return dyadic_tensor


def get_angle_between_vectors(vec1, vec2):

    # print(vec1)
    # print(vec2)
    # print('')

    norm_vec1 = (vec1 / np.linalg.norm(vec1))
    norm_vec2 = (vec2 / np.linalg.norm(vec2))
    # print(norm_vec1, norm_vec2)
    delta_angle = np.arccos(np.dot(norm_vec1, norm_vec2))
    delta_angle_degree = np.degrees(np.real(delta_angle))
    return delta_angle_degree


def get_eig_from_matrix(mat):
    return linalg.eig(mat)



def rotate_eigen_vals(eig_vals, rotation_matrix):
    x_ind = [3,4,5]
    y_ind = [3,4,5]
    rotated_ev = eig_vals.copy()
    for x in x_ind:
        for y in y_ind:
            rotated_ev[:,x,y] = np.matmul(eig_vals[:,x,y], rotation_matrix)

    return rotated_ev


if __name__ == '__main__':
    data_path = "/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_9_rotational_invariance/rotation/"
    
    gt_fa = np.ones([10,10])*0.8
    gt_md = np.ones([10,10])*0.8
    
    gt_evals = get_eigen_val_rotation_exp()
    diffusion_tensor = get_diffusion_tensor_from_fa_md(gt_fa, gt_md, gt_evals)
    tensor_mat = convert_tensor_mat_to_param(diffusion_tensor)
    gt_e_val, gt_e_vec = from_tensor_to_eig_v(tensor_mat)

    print(gt_e_val.shape, gt_e_vec.shape)
    colors = ['gray', 'red']

    fig, ax = plt.subplots(1, 1, figsize=(8,4))
    # fig.suptitle(r"\textbf{Dependency of fiber direction on rotation of gradient table}")
    fig.subplots_adjust(
        top=0.9,
        bottom=0.14,
        left=0.09,
        right=0.945,
        hspace=0.2,
        wspace=0.355
    )
    fc_colors = ['gray', 'red']
    box_plot_position = [[0.475], [0.525]]
    for m_, method in enumerate(['/mrtrix', '/dtiRIM_no_background']):
        mean_bias_theta_bval = []
        std_bias_theta_bval = []
        mean_bias_phi_bval = []
        std_bias_phi_bval = []


        folder_ = data_path + "correct_b0/arbitrary_rotation" + method
        list_dir_estimates = [f for f in (os.listdir(folder_)) if f.endswith(".pkl") and ('matrix') not in f]
        list_dir_rotations = [f for f in (os.listdir(folder_)) if f.endswith(".pkl") and ('matrix') in f]

        dyadic_tensors_j = []

        for f_est in list_dir_estimates:
            path_file_est = folder_ + "/" + f_est

            est_fa, est_md, est_tensor = get_results_data(path_file_est, method)
            estimated_e_val, estimated_e_vec = from_tensor_to_eig_v(est_tensor.detach().numpy())

            dyadic_eig_tensor = to_dyadic_form(estimated_e_vec[4,4,:,0])
            dyadic_tensors_j.append(dyadic_eig_tensor)
            

        mean_dyadic_tensor_est = np.mean(dyadic_tensors_j, 0)
        mean_dyadic_eigval_est, mean_dyadic_eigvec_est = get_eig_from_matrix(mean_dyadic_tensor_est)
        angle_bias = get_angle_between_vectors((mean_dyadic_eigvec_est[:,0]), (gt_e_vec[4,4][:,0]))


        all_angle_std = []
        for d_j in dyadic_tensors_j:
            eig_val, eig_dyat = get_eig_from_matrix(d_j)
            an_ = get_angle_between_vectors( np.abs(eig_dyat[:,np.argmax(eig_val)]), np.abs(mean_dyadic_eigvec_est[:,0]))
            all_angle_std.append(an_)

        std_estim = np.sqrt(np.mean(np.array(all_angle_std)))
        sorted_diff = np.sort(all_angle_std)
        print("Mean: {}, 95th percentile: {}".format(angle_bias, std_estim))



        ax.errorbar(box_plot_position[m_], angle_bias, yerr=std_estim, marker='x', linestyle='', color=fc_colors[m_], alpha=1)
        ax.axhline(0.0, linestyle='--', color='black', linewidth=0.3)
        ax.set_xlim([0,1])
        ax.set_xticks([])
        ax.grid(color='gray', linestyle='--', linewidth=0.35, alpha=0.75)
        ax.set_ylabel(r"Bias in fiber orientation estimation $(^{\circ})$")




    #     ax.plot(angle_list[:len(mean_bias_theta_bval)], mean_bias_theta_bval, color=colors[m_], marker = 'x', linewidth=1.25, markersize=3.5)
    #     ax.fill_between(angle_list[:len(mean_bias_theta_bval)], (np.array(mean_bias_theta_bval)-np.array(std_bias_theta_bval)), (np.array(mean_bias_theta_bval)+np.array(std_bias_theta_bval)), color=colors[m_], alpha=.2)
    #     ax.axhline(0.0, linestyle='--', color='black', linewidth=1)
    #     # ax[0].plot(angle_list, angle_list, linestyle='--', color='black', linewidth=1)

    #     ax.set_xlabel(r"$\vartheta_{\epsilon}$ ($^{\circ}$)")
    #     ax.set_ylabel(r"Orientation error $\epsilon$")
    #     ax.set_xticks(angle_list)
    #     ax.title.set_text(r"Dependency of $\varphi_{\epsilon}$ on eigenvector orientation")
       


    #     # ax[1].plot(angle_list[:len(mean_bias_phi_bval)], mean_bias_phi_bval, color=colors[m_], marker = 'o', linewidth=1.25, markersize=2.5)
    #     # ax[1].fill_between(angle_list[:len(mean_bias_phi_bval)], (np.array(mean_bias_phi_bval)-np.array(std_bias_phi_bval)), (np.array(mean_bias_phi_bval)+np.array(std_bias_phi_bval)), color=colors[m_], alpha=.2)
    #     # # ax[1].axhline(0.0, linestyle='--', color='black', linewidth=1)
    #     # ax[1].plot(angle_list, angle_list, linestyle='--', color='black', linewidth=1)

    #     # ax[1].set_xlabel(r"$\vartheta_{\epsilon}$ ($^{\circ}$)")
    #     # ax[1].set_ylabel(r"$\hat{\vartheta}_{\epsilon}$ ($^{\circ}$)")
    #     # ax[1].set_xticks(angle_list)
    #     # ax[1].title.set_text(r"Dependency of $\vartheta_{\epsilon}$ on eigenvector orientation")

    mrtrix_patch = mpatches.Patch(facecolor='gray', edgecolor='black', label=r'$\mathit{\mathbf{IRLLS}}$')
    aRIM_patch = mpatches.Patch(facecolor='red', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$')
    plt.legend(handles=[mrtrix_patch, aRIM_patch])#, bbox_to_anchor=(0.5, -0.55), loc='center')

    plt.show()






