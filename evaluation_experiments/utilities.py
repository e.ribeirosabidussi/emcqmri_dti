import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import (inset_axes, TransformedBbox,
                                                   BboxPatch, BboxConnector)
                                                
import matplotlib.ticker as mtick
from skimage.measure import label  
import pickle
import scipy.stats as stats

from dipy.reconst.dti import fractional_anisotropy, color_fa, mean_diffusivity, radial_diffusivity, axial_diffusivity
from dipy.reconst.dti import lower_triangular, eig_from_lo_tri

import torch




def get_results_data(file_path, ind_x, ind_y):
    
    with open(file_path, 'rb') as f:
        data = pickle.load(f)
    estimated_tensor = data["diffusion_tensors"]

    e_val, e_vec = from_tensor_to_eig_v(estimated_tensor[1:])
    estimated_ad_map = axial_diffusivity(e_val)[ind_x, ind_y]
    estimated_rd_map = radial_diffusivity(e_val)[ind_x, ind_y]
    estimated_md_map = mean_diffusivity(e_val)[ind_x, ind_y]
    estimated_fa_map = fractional_anisotropy(e_val)[ind_x, ind_y]

    return estimated_fa_map, estimated_md_map, estimated_ad_map, estimated_rd_map, np.array(estimated_tensor[:, ind_x, ind_y].detach().numpy())




def get_gt_eigen_val():
    eig = np.zeros([3, 10, 10])
    eig[0,:,:] = 1
    eig[1,:,:] = 0
    eig[2,:,:] = 0

    return eig


def get_gtound_truth_ad_rd_from_eigen_vals(fa, md, eig_vals):
    tensor = get_diffusion_tensor_from_fa_md(fa, md, eig_vals)
    param_tensor = convert_tensor_mat_to_param(tensor)
    e_val, e_vec = from_tensor_to_eig_v(param_tensor)
    ad_val = axial_diffusivity(e_val)[4,4]
    rd_val = radial_diffusivity(e_val)[4,4]

    return ad_val, rd_val


def get_diffusion_tensor_from_fa_md(fa, md, eigen_vals):

    a = fa / np.sqrt(3-2 * fa * fa)
    dr = md * (1.0 - a)
    dp = md * (1.0 + 2.0 * a)

    tensor = np.zeros([3, 3, 10, 10])
    
    # print(((dp - dr) * eigen_vals[0]* eigen_vals[0]).shape, dp.shape, eigen_vals[0].shape)
    tensor[0][0] = dr + (dp - dr) * eigen_vals[0] * eigen_vals[0]
    tensor[1][1] = dr + (dp - dr) * eigen_vals[1] * eigen_vals[1]
    tensor[2][2] = dr + (dp - dr) * eigen_vals[2] * eigen_vals[2]
    tensor[0][1] = tensor[1][0] = (dp - dr) * eigen_vals[0] * eigen_vals[1]
    tensor[0][2] = tensor[2][0] = (dp - dr) * eigen_vals[0] * eigen_vals[2]
    tensor[1][2] = tensor[2][1] = (dp - dr) * eigen_vals[1] * eigen_vals[2]

    return tensor



def convert_tensor_mat_to_param(tensor_mat, voxel_first = False):
    if voxel_first:
        compressed_tensor = [tensor_mat[..., 0, 0], tensor_mat[..., 0, 1], tensor_mat[..., 0, 2], tensor_mat[..., 1, 1], tensor_mat[..., 1, 2], tensor_mat[..., 2, 2]]
    else:
        compressed_tensor = [tensor_mat[0, 0], tensor_mat[0, 1], tensor_mat[0, 2], tensor_mat[1, 1], tensor_mat[1, 2], tensor_mat[2, 2]]
    return torch.from_numpy(np.array(compressed_tensor))




def plot_comparison(x_list, mean_metric, std_metric, color, alpha, axhline_, fig, ax, label_x, label_y, x_ticks, title):
    ax.plot(x_list, mean_metric, color=color, marker = 'x', linewidth=1.25, markersize=3.5)
    ax.fill_between(x_list, (np.array(mean_metric)-np.array(std_metric)), (np.array(mean_metric)+np.array(std_metric)), color=color, alpha=alpha)
    if axhline_>=0:
        ax.axhline(axhline_, linestyle='--', color='black', linewidth=1)
    ax.set_xlabel(label_x)
    ax.set_ylabel(label_y)
    ax.set_xticks(x_ticks)
    ax.title.set_text(title)
        

def build_d_mat(d_tensors):
    d_mat = np.array([[d_tensors[0], d_tensors[1], d_tensors[2]],
                    [d_tensors[1], d_tensors[3], d_tensors[4]],
                    [d_tensors[2], d_tensors[4], d_tensors[5]]])

    d_mat_perm = np.transpose(d_mat, (-2, -1, 0, 1))
    return d_mat_perm


def from_tensor_to_eig_v(tensors):
    d_mat = build_d_mat(np.stack(tensors.detach().numpy()))
    l_triang = lower_triangular(d_mat)
    eig_v = eig_from_lo_tri(l_triang, min_diffusivity=1e-6)
    e_val = eig_v[...,:3]
    e_vec = eig_v[...,3:12]
    sh_tensor = tensors.shape[-2:]
    e_vec = e_vec.reshape(*sh_tensor, 3, 3)

    return e_val, e_vec


def radial_diffusivity_(eval_):
    return radial_diffusivity(eval_)


def axial_diffusivity_(eval_):
    return axial_diffusivity(eval_)


def fractional_anisotropy_(eval_):
    return fractional_anisotropy(eval_)


def mean_diffusivity_(eval_):
    return mean_diffusivity(eval_)

def compute_bias_std_signal_residuals(pred_signal, label_signal):

    # Computing Bias
    error_s0_map = []
    error_dwi_map = []
    std_s0_map = []
    std_dwi_map = []
    rmse_s0_map = []
    rmse_dwi_map = []
    mean_estimated = np.mean(pred_signal, 0)

    for prd_signal in pred_signal:
        error_map = prd_signal - label_signal[0]
        error_s0_map.append(error_map[0])
        error_dwi_map.append(error_map[1:])

        rmse_map = (prd_signal - label_signal[0])**2
        rmse_s0_map.append(rmse_map[0])
        rmse_dwi_map.append(rmse_map[1:])

        std_map = (prd_signal - mean_estimated)**2
        std_s0_map.append(std_map[0])
        std_dwi_map.append(std_map[1:])

    bias_s0 = (np.mean(np.array(error_s0_map), 0))
    bias_dwi = (np.mean(np.array(error_dwi_map), 0))

    rmse_s0 = np.sqrt(np.mean(np.array(rmse_s0_map), 0))
    rmse_dwi = np.sqrt(np.mean(np.array(rmse_dwi_map), 0))

    std_s0 = np.sqrt(np.mean(np.array(std_s0_map), 0)) # Replace with np.std directly
    std_dwi = np.sqrt(np.mean(np.array(std_dwi_map), 0))

    return [bias_s0, std_s0, rmse_s0], [bias_dwi, std_dwi, rmse_dwi]


def compute_fa_md_errors(pred_metric, label_signal):

    # Computing Bias
    error_map = []
    std_map = []
    rmse_map = []
    mean_estimated = np.mean(pred_metric, 0)
    for prd_signal in pred_metric:
        error = prd_signal - label_signal
        error_map.append(error)

        rmse = (prd_signal - label_signal)**2
        rmse_map.append(rmse)

        std = (prd_signal - mean_estimated)**2
        std_map.append(std)

    bias_metric = np.mean(np.array(error_map), 0)
    std_metric = np.sqrt(np.mean(np.array(std_map), 0))
    rmse_metric = np.sqrt(np.mean(np.array(rmse_map), 0))

    return bias_metric, std_metric, rmse_metric


def compute_signal_fitting(pred_metric, label_signal):

    error_map = []
    std_map = []
    rmse_map = []
    mean_estimated = np.mean(pred_metric, 0)
    for prd_signal in pred_metric:
        error = prd_signal - label_signal
        error_map.append(error)

        rmse = (prd_signal - label_signal)**2
        rmse_map.append(rmse)

        std = (prd_signal - mean_estimated)**2
        std_map.append(std)

    bias_metric = np.sum(np.array(error_map), 0)
    std_metric = np.sqrt(np.sum(np.array(std_map), 0))
    rmse_metric = np.sqrt(np.sum(np.array(rmse_map), 0))

    return bias_metric, std_metric, rmse_metric


def create_plot(metric, fig, ax, indexes, lims_0, lims_1, lims_2):
    colors = ['white', 'lightgray', 'gray']
    snr_ = ['30', '30', '5']
    d_sigma = ['0', '0.25', '0.5']
    methods = ['mrtrix', 'aRIM', 'mRIM']

    s, m, d, p = indexes

    positions = [1, 1.5, 2,  5, 5.5, 6,  9, 9.5, 10]

    mrtrix_patch = mpatches.Patch(
        facecolor='white', edgecolor='black', label='MRtrix')
    aRIM_patch = mpatches.Patch(
        facecolor='lightgray', edgecolor='black', label='aRIM')
    mRIM_patch = mpatches.Patch(
        facecolor='gray', edgecolor='black', label='mRIM')
    ax[0, 0].legend(handles=[mrtrix_patch, aRIM_patch, mRIM_patch])

    mean_bias = metric[0]
    bias_p = ax[0, s].boxplot(np.reshape(mean_bias, (-1, 1)), positions=[positions[p]], showfliers=False, sym='',
                              patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
    ax[0, s].set_xticks([1.5, 5.5, 9.5])
    ax[0, s].set_xticklabels(['d = 0', 'd = 0.25', 'd = 0.5'])
    ax[0, s].title.set_text('SNR: {}'.format(snr_[s]))
    ax[0, s].axhline(0, color='black', linestyle='-', linewidth=0.1)
    ax[0, s].set_ylim(lims_0)
    ax[0, 0].set_ylabel('Bias')
    ax[0, s].yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

    if len(metric) > 1:
        mean_var = metric[1]
        var_p = ax[1, s].boxplot(np.reshape(mean_var, (-1, 1)), positions=[positions[p]], showfliers=False, sym='',
                                 patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
        ax[1, s].set_xticks([1.5, 5.5, 9.5])
        ax[1, s].set_xticklabels(['d = 0', 'd = 0.25', 'd = 0.5'])
        ax[1, s].axhline(0, color='black', linestyle='-', linewidth=0.1)
        ax[1, s].set_ylim(lims_1)
        ax[1, 0].set_ylabel('Standard deviation')
        ax[1, s].yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

    if len(metric) > 2:
        mean_rmse = metric[2]
        rmse_p = ax[2, s].boxplot(np.reshape(mean_rmse, (-1, 1)), positions=[positions[p]], showfliers=False, sym='',
                                  patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
        ax[2, s].set_xticks([1.5, 5.5, 9.5])
        ax[2, s].set_xticklabels(['d = 0', 'd = 0.25', 'd = 0.5'])
        ax[2, 0].set_ylabel('RMSE')
        ax[2, s].axhline(0, color='black', linestyle='-', linewidth=0.1)
        ax[2, s].set_ylim(lims_2)
        ax[2, 0].set_ylabel('RMSE')
        ax[2, s].yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)


def create_one_plot(metric, metric_name, fig, ax, lims, title, xticks, xlabel='', yticklabels=False, positions = []):
    colors = ['white', 'gray']
    d_sigma = ['0', '0.25', '0.5']
    methods = ['mrtrix', 'aRIM']

    data_mrtrix, data_aRIM = metric
    ax.boxplot(list(data_mrtrix), positions=positions[::2], showfliers=False, sym='', patch_artist=True, boxprops=dict(
        facecolor='white', color='black'), medianprops=dict(color='k'), widths=[0.15]*len(list(data_mrtrix)))
    ax.boxplot(list(data_aRIM), positions=positions[1::2], showfliers=False, sym='', patch_artist=True, boxprops=dict(
        facecolor='gray', color='black'), medianprops=dict(color='k'), widths=[0.15]*len(list(data_aRIM)))

    # ax.boxplot(list(data_mRIM), positions=[positions[2], positions[5], positions[8]], showfliers=False, sym='', patch_artist=True, boxprops=dict(
    #     facecolor='gray', color='black'), medianprops=dict(color='k'), widths=[0.15, 0.15, 0.15])

    it_list = [1.25, 3.25, 5.25, 7.25]
    ax.set_xticks(it_list[:len(xticks)])
    ax.set_xticklabels(xticks)
    ax.set_xlabel(xlabel)
    ax.title.set_text(title)
    ax.axhline(0, color='black', linestyle='-', linewidth=0.1)
    ax.set_ylim(lims)
    ax.set_ylabel(metric_name, weight='bold')
    if not yticklabels:
        ax.set_yticklabels("")
    ax.yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

def create_one_plot_with_inset(metric, metric_name, fig, ax, axins, zoom_coord, lims, title, xticks, xlabel='', yticklabels=False):
    colors = ['white', 'gray', 'gray']
    d_sigma = ['30', '24', '18', '12']
    methods = ['mrtrix', 'aRIM']#, 'mRIM']

    positions = [1.15, 1.35,  3.15, 3.35,  5.15, 5.35,  7.15, 7.35]

    data_mrtrix, data_aRIM = metric

    ax.boxplot(list(data_mrtrix), positions=positions[::2], showfliers=False, sym='', patch_artist=True, boxprops=dict(
        facecolor='white', color='black'), medianprops=dict(color='k'), widths=[0.15, 0.15, 0.15, 0.15])
    ax.boxplot(list(data_aRIM), positions=positions[1::2], showfliers=False, sym='', patch_artist=True, boxprops=dict(
        facecolor='gray', color='black'), medianprops=dict(color='k'), widths=[0.15, 0.15, 0.15, 0.15])
    # ax.boxplot(list(data_mRIM), positions=[positions[2], positions[5], positions[8], positions[11]], showfliers=False, sym='', patch_artist=True, boxprops=dict(
    #     facecolor='gray', color='black'), medianprops=dict(color='k'), widths=[0.15, 0.15, 0.15, 0.15])

    ax.set_xticks([1.25, 3.25, 5.25, 7.25])
    ax.set_xticklabels(xticks)
    ax.set_xlabel(xlabel)
    ax.title.set_text(title)
    ax.axhline(0, color='black', linestyle='-', linewidth=0.1)
    ax.set_ylim(lims)
    ax.set_ylabel(metric_name)
    if not yticklabels:
        ax.set_yticklabels("")
    ax.yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

    axins.boxplot(list(data_mrtrix), positions=positions[::2], showfliers=False, sym='', patch_artist=True, boxprops=dict(
        facecolor='white', color='black'), medianprops=dict(color='k'), widths=[0.15, 0.15, 0.15, 0.15])
    axins.boxplot(list(data_aRIM), positions=positions[1::2], showfliers=False, sym='', patch_artist=True, boxprops=dict(
        facecolor='lightgray', color='black'), medianprops=dict(color='k'), widths=[0.15, 0.15, 0.15, 0.15])
    # axins.boxplot(list(data_mRIM), positions=[positions[2], positions[5], positions[8], positions[11]], showfliers=False, sym='', patch_artist=True, boxprops=dict(
    #     facecolor='gray', color='black'), medianprops=dict(color='k'), widths=[0.15, 0.15, 0.15, 0.15])

    x1, x2, y1, y2 = zoom_coord
    axins.set_xlim(x1, x2)
    axins.axhline(0, color='black', linestyle='-', linewidth=0.2)
    axins.set_ylim(y1, y2)
    axins.set_xticklabels('')
    axins.set_xticks([])
    axins.yaxis.tick_right()
    axins.yaxis.set_major_formatter(MathTextSciFormatter("%1.1e"))
    axins.tick_params(axis='y', which='major', labelsize=8)
    ax.indicate_inset_zoom(axins, edgecolor="black")


def create_plot_v2(metric, fig, ax, axins, indexes, lims_0, lims_1, lims_2, zoom_coord):
    colors = ['white', 'lightgray', 'gray']
    snr_ = ['30', '30', '5']
    d_sigma = ['0', '0.25', '0.5']
    methods = ['mrtrix', 'aRIM', 'mRIM']

    s, m, d, p = indexes

    positions = [1, 1.25, 1.5,  3, 3.25, 3.5,  5, 5.25, 5.5,  7, 7.25, 7.5]

    mrtrix_patch = mpatches.Patch(
        facecolor='white', edgecolor='black', label='MRtrix')
    aRIM_patch = mpatches.Patch(
        facecolor='lightgray', edgecolor='black', label='aRIM')
    mRIM_patch = mpatches.Patch(
        facecolor='gray', edgecolor='black', label='mRIM')
    ax[2].legend(handles=[mrtrix_patch, aRIM_patch, mRIM_patch],
                 bbox_to_anchor=(0.5, -0.4), loc='center')

    # flierprops=dict( sym='x', color='gray', size=3, alpha=0.4)

    mean_bias = metric[0]
    bias_p = ax[0].boxplot(np.reshape(mean_bias, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                           patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
    ax[0].set_xticks([1.25, 3.25, 5.25])
    ax[0].set_xticklabels([])
    ax[0].title.set_text('SNR: {}'.format(snr_[s]))
    ax[0].axhline(0, color='black', linestyle='-', linewidth=0.1)
    ax[0].set_ylim(lims_0)
    ax[0].set_ylabel('Bias')
    ax[0].yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

    if d == 0:
        axins[0].boxplot(np.reshape(mean_bias, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                         patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
        x1, x2, y1, y2 = zoom_coord
        axins[0].set_xlim(x1, x2)
        axins[0].axhline(0, color='black', linestyle='-', linewidth=0.2)
        axins[0].set_ylim(y1, y2)
        axins[0].set_xticklabels('')
        axins[0].yaxis.tick_right()
        axins[0].yaxis.set_major_formatter(MathTextSciFormatter("%1.1e"))
        axins[0].tick_params(axis='y', which='major', labelsize=8)
        ax[0].indicate_inset_zoom(axins[0], edgecolor="black")

    if len(metric) > 1:
        mean_var = metric[1]
        var_p = ax[1].boxplot(np.reshape(mean_var, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                              patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
        ax[1].set_xticks([1.25, 3.25, 5.25])
        ax[1].set_xticklabels([])
        ax[1].axhline(0, color='black', linestyle='-', linewidth=0.1)
        ax[1].set_ylim(lims_1)
        ax[1].set_ylabel('STD')
        ax[1].yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

        if d == 0:
            axins[1].boxplot(np.reshape(mean_var, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                             patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
            x1, x2, y1, y2 = 0.75, 1.75, -0.00005, 0.0003
            axins[1].axhline(0, color='black', linestyle='-', linewidth=0.2)
            axins[1].set_xlim(x1, x2)
            axins[1].set_ylim(y1, y2)
            axins[1].set_xticklabels('')
            axins[1].yaxis.tick_right()
            axins[1].yaxis.set_major_formatter(MathTextSciFormatter("%1.1e"))
            axins[1].tick_params(axis='y', which='major', labelsize=8)
            ax[1].indicate_inset_zoom(axins[1], edgecolor="black")

    if len(metric) > 2:
        mean_rmse = metric[2]
        rmse_p = ax[2].boxplot(np.reshape(mean_rmse, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                               patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
        ax[2].set_xticks([1.25, 3.25, 5.25])
        ax[2].set_xticklabels(['d = 0', 'd = 0.25', 'd = 0.5'])
        ax[2].set_ylabel('RMSE')
        ax[2].axhline(0, color='black', linestyle='-', linewidth=0.1)
        ax[2].set_ylim(lims_2)
        ax[2].set_ylabel('RMSE')
        ax[2].yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

        if d == 0:
            axins[2].boxplot(np.reshape(mean_rmse, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                             patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
            x1, x2, y1, y2 = 0.75, 1.75, -0.00005, 0.0003
            axins[2].axhline(0, color='black', linestyle='-', linewidth=0.2)
            axins[2].set_xlim(x1, x2)
            axins[2].set_ylim(y1, y2)
            axins[2].set_xticklabels('')
            axins[2].yaxis.tick_right()
            axins[2].yaxis.set_major_formatter(MathTextSciFormatter("%1.1e"))
            axins[2].tick_params(axis='y', which='major', labelsize=8)
            ax[2].indicate_inset_zoom(axins[2], edgecolor="black")




def create_plot_v3(metric, fig, ax, axins, indexes, lims_0, lims_1, lims_2, zoom_coord):
    colors = ['white', 'gray']#, 'gray']
    snr_ = ['30']
    # d_sigma = ['30', '24', '18', '12'] 
    d_sigma = ['13', '19', '25', '31']
    methods = ['mrtrix', 'aRIM']#, 'mRIM']

    s, m, d, p = indexes

    positions = [1.15, 1.35,  3.15, 3.35,  5.15, 5.35,  7.15, 7.35]

    mrtrix_patch = mpatches.Patch(
        facecolor='white', edgecolor='black', label='MRtrix')
    aRIM_patch = mpatches.Patch(
        facecolor='gray', edgecolor='black', label='aRIM')
    # mRIM_patch = mpatches.Patch(
    #     facecolor='gray', edgecolor='black', label='mRIM')
    ax[2].legend(handles=[mrtrix_patch, aRIM_patch],
                 bbox_to_anchor=(0.5, -0.4), loc='center')

    # flierprops=dict( sym='x', color='gray', size=3, alpha=0.4)

    mean_bias = metric[0]
    bias_p = ax[0].boxplot(np.reshape(mean_bias, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                           patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
    ax[0].set_xticks([1.25, 3.25, 5.25, 7.25])
    ax[0].set_xticklabels([])
    ax[0].title.set_text('SNR: {}'.format(snr_[s]))
    ax[0].axhline(0, color='black', linestyle='-', linewidth=0.1)
    ax[0].set_ylim(lims_0)
    ax[0].set_ylabel('Bias')
    ax[0].yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

    # if d == 0:
    #     axins[0].boxplot(np.reshape(mean_bias, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
    #                      patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
    #     x1, x2, y1, y2 = zoom_coord
    #     axins[0].set_xlim(x1, x2)
    #     axins[0].axhline(0, color='black', linestyle='-', linewidth=0.2)
    #     axins[0].set_ylim(y1, y2)
    #     axins[0].set_xticklabels('')
    #     axins[0].yaxis.tick_right()
    #     axins[0].yaxis.set_major_formatter(MathTextSciFormatter("%1.1e"))
    #     axins[0].tick_params(axis='y', which='major', labelsize=8)
    #     ax[0].indicate_inset_zoom(axins[0], edgecolor="black")

    if len(metric) > 1:
        mean_var = metric[1]
        var_p = ax[1].boxplot(np.reshape(mean_var, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                              patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
        ax[1].set_xticks([1.25, 3.25, 5.25, 7.25])
        ax[1].set_xticklabels([])
        ax[1].axhline(0, color='black', linestyle='-', linewidth=0.1)
        ax[1].set_ylim(lims_1)
        ax[1].set_ylabel('STD')
        ax[1].yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

        if d == 0:
            axins[1].boxplot(np.reshape(mean_var, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                             patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
            x1, x2, y1, y2 = 0.75, 1.75, -0.00005, 0.0003
            axins[1].axhline(0, color='black', linestyle='-', linewidth=0.2)
            axins[1].set_xlim(x1, x2)
            axins[1].set_ylim(y1, y2)
            axins[1].set_xticklabels('')
            axins[1].yaxis.tick_right()
            axins[1].yaxis.set_major_formatter(MathTextSciFormatter("%1.1e"))
            axins[1].tick_params(axis='y', which='major', labelsize=8)
            ax[1].indicate_inset_zoom(axins[1], edgecolor="black")

    if len(metric) > 2:
        mean_rmse = metric[2]
        rmse_p = ax[2].boxplot(np.reshape(mean_rmse, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
                               patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
        ax[2].set_xticks([1.25, 3.25, 5.25, 7.25])
        ax[2].set_xticklabels([r'N = 13', r'N = 19', r'N = 25', r'N = 31'])
        ax[2].set_ylabel('RMSE')
        ax[2].axhline(0, color='black', linestyle='-', linewidth=0.1)
        ax[2].set_ylim(lims_2)
        ax[2].set_ylabel('RMSE')
        ax[2].yaxis.grid(True, color='gray', linestyle='--', linewidth=0.4)

        # if d == 0:
        #     axins[2].boxplot(np.reshape(mean_rmse, (-1, 1)), positions=[positions[p]], flierprops=dict(marker='', color='blue', markersize=1, alpha=0.2),
        #                      patch_artist=True, boxprops=dict(facecolor=colors[m], color='black'), medianprops=dict(color='k'))
        #     x1, x2, y1, y2 = 0.75, 1.75, -0.00005, 0.0003
        #     axins[2].axhline(0, color='black', linestyle='-', linewidth=0.2)
        #     axins[2].set_xlim(x1, x2)
        #     axins[2].set_ylim(y1, y2)
        #     axins[2].set_xticklabels('')
        #     axins[2].yaxis.tick_right()
        #     axins[2].yaxis.set_major_formatter(MathTextSciFormatter("%1.1e"))
        #     axins[2].tick_params(axis='y', which='major', labelsize=8)
        #     ax[2].indicate_inset_zoom(axins[2], edgecolor="black")


def create_plot_std(std_, fig, ax, indexes, lims_0):
    colors = ['orange', 'turquoise', 'royalblue']
    snr_ = ['60', '30', '5']
    d_sigma = ['0', '0.25', '0.5']
    methods = ['mrtrix', 'aRIM', 'mRIM']
    s, m, d, p = indexes

    positions = [1, 1.5, 2,  5, 5.5, 6,  9, 9.5, 10]

    ax[0].set_ylabel('RMSE')
    mrtrix_patch = mpatches.Patch(color='orange', label='MRtrix')
    aRIM_patch = mpatches.Patch(color='turquoise', label='aRIM')
    mRIM_patch = mpatches.Patch(color='royalblue', label='mRIM')
    ax[0].legend(handles=[mrtrix_patch, aRIM_patch, mRIM_patch])

    var_p = ax[s].boxplot(np.reshape(std_, (-1, 1)), positions=[positions[p]], showfliers=False, sym='',
                          patch_artist=True, boxprops=dict(facecolor=colors[m], color=colors[m], alpha=0.6), medianprops=dict(color='k'))
    ax[s].title.set_text('SNR: {}'.format(snr_[s]))
    ax[s].set_xticks([1.5, 5.5, 9.5])
    ax[s].set_xticklabels(['d = 0', 'd = 0.25', 'd = 0.5'])

    ax[s].set_ylim(lims_0)
    ax[s].axhline(0, color='gray', linestyle='--', linewidth=0.2)


def write_metric(metric, method, metric_name, disp, file):
    with open(file + metric_name + method + '_d' + disp + '.txt', 'w') as f_:
        for b in metric:
            f_.write(str(b) + ' ')
        f_.close()
        print("Data exported to file")

def write_map(map_, method, metric_name, disp, file):
    with open(file + metric_name + method + '_d' + disp + '.pkl', 'wb') as f_:
        pickle.dump(map_, f_)
        print("Data exported to file")

def read_map(method, metric_name, disp, file):
    print(file + metric_name + method + '_d' + disp + '.pkl')
    with open(file + metric_name + method + '_d' + disp + '.pkl', 'rb') as f_:
        data = pickle.load(f_)
        print("File loaded")
    return data


def viz_residual(signal, predicted_signal):
    residual = signal - predicted_signal

    # std_sig = self.get_std_signal(signal)
    # std_pred = self.get_std_signal(predicted_signal)

    sos_residuals = np.sum(residual**2, 0)

    dwi = 5
    fig, ax = plt.subplots(1, 3)
    ax[0].imshow(signal[dwi], cmap='gray', vmin=0.0, vmax=0.75)
    ax[1].imshow(predicted_signal[dwi], cmap='gray', vmin=0.0, vmax=0.75)
    ax_res = ax[2].imshow(residual[dwi], cmap='seismic', vmin=-.02, vmax=.02)

    divider = make_axes_locatable(ax[2])
    cax = divider.append_axes('right', size='5%', pad=0.05)

    fig.colorbar(ax_res, cax=cax)
    plt.show()

    dwi = 0
    fig, ax = plt.subplots(1, 3)
    ax[0].imshow(signal[dwi], cmap='gray', vmin=0.0, vmax=1.25)
    ax[1].imshow(predicted_signal[dwi], cmap='gray', vmin=0.0, vmax=1.25)
    ax_res = ax[2].imshow(residual[dwi], cmap='seismic', vmin=-.05, vmax=0.05)

    divider = make_axes_locatable(ax[2])
    cax = divider.append_axes('right', size='5%', pad=0.05)

    fig.colorbar(ax_res, cax=cax)
    plt.show()


class MathTextSciFormatter(mtick.Formatter):
    def __init__(self, fmt="%1.2e"):
        self.fmt = fmt
    def __call__(self, x, pos=None):
        s = self.fmt % x
        decimal_point = '.'
        positive_sign = '+'
        tup = s.split('e')
        significand = tup[0].rstrip(decimal_point)
        sign = tup[1][0].replace(positive_sign, '')
        exponent = tup[1][1:].lstrip('0')
        if exponent:
            exponent = '10^{%s%s}' % (sign, exponent)
        if significand and exponent:
            s =  r'%s{\times}%s' % (significand, exponent)
        else:
            s =  r'%s%s' % (significand, exponent)
        return "${}$".format(s)


def my_mark_inset(parent_axes, inset_axes, loc1a=1, loc1b=1, loc2a=2, loc2b=2, **kwargs):
    rect = TransformedBbox(inset_axes.viewLim, parent_axes.transData)
    pp = BboxPatch(rect, fill=False, **kwargs)
    parent_axes.add_patch(pp)
    p1 = BboxConnector(inset_axes.bbox, rect, loc1=loc1a, loc2=loc1b, **kwargs)
    inset_axes.add_patch(p1)
    p1.set_clip_on(False)
    p2 = BboxConnector(inset_axes.bbox, rect, loc1=loc2a, loc2=loc2b, **kwargs)
    inset_axes.add_patch(p2)
    p2.set_clip_on(False)

    return pp, p1, p2



def getLargestCC(segmentation):
    labels = label(segmentation)
    assert( labels.max() != 0 )
    largestCC = labels == np.argmax(np.bincount(labels.flat)[1:])+1
    return largestCC


def perform_unpaired_t_test(data_m1, data_m2, name_m1, name_m2, metric, experiment):
    t_stat, p_val = stats.ttest_ind(data_m1, data_m2)
    print("M1: {}, M2: {} - metric/exp: {}/{} == t_stat: {}, p_val: {}".format(name_m1, name_m2, metric, experiment, t_stat, p_val))

# perform_unpaired_t_test(stat_test_mrtrix_bias, stat_test_aRIM_bias, 'mrtrix', 'aRIM', d)


def AnnoMe(x1, x2, ARRAY, TXT, ax, mult, off=0):
    y, col = max(np.percentile(ARRAY[0], 95), np.percentile(ARRAY[1], 95)), 'k'

    # print(np.percentile(ARRAY[0], 95), np.percentile(ARRAY[1], 95))

    ymin, ymax = ax.get_ylim()
    ysiz = ymax-ymin
    y += (ysiz/mult) + off
    h = ysiz/40

    # print(y, h)

    ax.plot([x1, x1, x2, x2], [y, y+h, y+h, y], lw=1.5, c=col)
    ax.text((x1+x2)*.5, y+h, TXT, ha='center', va='bottom', color=col)