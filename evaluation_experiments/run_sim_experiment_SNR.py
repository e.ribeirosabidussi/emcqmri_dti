
import utilities

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import os
import pickle
import sys

matplotlib.rc('font', family='serif', serif='cm10')
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']
sys.path.insert(0, '../../')


if __name__ == '__main__':
    data_path = "/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_1/"
    snr_list = np.linspace(4,30,14)
    eig_val = utilities.get_gt_eigen_val()
    gt_fa = 0.8
    gt_md = 0.8
    gt_ad, gt_rd = utilities.get_gtound_truth_ad_rd_from_eigen_vals(gt_fa, gt_md, eig_val)

    colors = ['gray', 'red', 'purple']

    fig, ax = plt.subplots(1, 2, figsize=(8,4))
    fig.subplots_adjust(
        top=0.9,
        bottom=0.14,
        left=0.09,
        right=0.945,
        hspace=0.2,
        wspace=0.355
    )

    alpha_method = [0.2, 0.2, 0.2]
    for m_, method in enumerate(['/mrtrix', '/dtiRIM_no_background_new_loss']):
        mean_bias_fa_x_ = []
        std_bias_fa_x_ = []
        mean_bias_md_x_ = []
        std_bias_md_x_ = []
        mean_bias_ad_x_ = []
        std_bias_ad_x_ = []
        mean_bias_rd_x_ = []
        std_bias_rd_x_ = []
        sn_ = 0
        for snr_ in snr_list:
            folder_ = data_path + "correct_b0/snr_" + str(int(snr_)) + method#"/dti_rim"
            list_dir = [f for f in (os.listdir(folder_)) if f.endswith(".pkl")]

            total_error_fa = []
            total_error_md = []
            total_error_ad = []
            total_error_rd = []
            for fi, f_ in enumerate(list_dir):

                if fi ==100:
                    break

                path_file = folder_ + "/" + f_
                print("read file: {}".format(path_file))
                est_fa, est_md, est_ad, est_rd, est_ev = utilities.get_results_data(path_file, 5, 5)
                if method == "/mrtrix":
                    est_md *= 1000
                    est_ev *= 1000
                    est_ad *= 1000
                    est_rd *= 1000

                # If want to plot with respect to the actual parameter value.
                fa_error = est_fa
                md_error = est_md
                ad_error = est_ad
                rd_error = est_rd

                total_error_fa.append(fa_error)
                total_error_md.append(md_error)
                total_error_ad.append(ad_error)
                total_error_rd.append(rd_error)
            
            mean_bias_fa_x_.append(np.mean(total_error_fa, 0))
            std_bias_fa_x_.append(np.std(total_error_fa,0))
            mean_bias_md_x_.append(np.mean(total_error_md, 0))
            std_bias_md_x_.append(np.std(total_error_md,0))
            mean_bias_ad_x_.append(np.mean(total_error_ad, 0))
            std_bias_ad_x_.append(np.std(total_error_ad,0))
            mean_bias_rd_x_.append(np.mean(total_error_rd, 0))
            std_bias_rd_x_.append(np.std(total_error_rd,0))


        utilities.plot_comparison(snr_list, mean_bias_fa_x_, std_bias_fa_x_, colors[m_], 
                                  alpha_method[m_], gt_fa, fig, ax[0], "SNR", r"Estimated FA", 
                                  snr_list, "FA dependency on SNR")

        utilities.plot_comparison(snr_list, mean_bias_md_x_, std_bias_md_x_, colors[m_], 
                                  alpha_method[m_], gt_md, fig, ax[1], "SNR", r"Estimated MD $[mm^2/ms]$", 
                                  snr_list, "MD dependency on SNR")

        # utilities.plot_comparison(snr_list, mean_bias_ad_x_, std_bias_ad_x_, colors[m_], 
        #                           alpha_method[m_], gt_ad, fig, ax[1,0], "SNR", r"Estimated AD $[mm^2/ms]$", 
        #                           snr_list, "AD dependency on SNR")

        # utilities.plot_comparison(snr_list, mean_bias_rd_x_, std_bias_rd_x_, colors[m_], 
        #                           alpha_method[m_], gt_rd, fig, ax[1,1], "SNR", r"Estimated RD $[mm^2/ms]$", 
        #                           snr_list, "RD dependency on SNR")


    mrtrix_patch = mpatches.Patch(facecolor='gray', edgecolor='black', label=r'$\mathit{\mathbf{IRLLS}}$')
    aRIM_patch = mpatches.Patch(facecolor='red', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$')
    plt.legend(handles=[mrtrix_patch, aRIM_patch])

    plt.show()






