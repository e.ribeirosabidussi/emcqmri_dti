import utilities

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import os
import pickle


def box_plot(data, edge_color, fill_color, positions_, ax):
    bp = ax.boxplot(data, positions=positions_,patch_artist=True, showfliers=False)
    
    for patch in bp['boxes']:
        patch.set(facecolor=fill_color, alpha=0.3)    
        
    for element in ['whiskers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)
   
        
    return bp



if __name__ == '__main__':
    data_path = "/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_3/"
    grad_list = ["gradient_scheme_1/", "gradient_scheme_2/", "gradient_scheme_3/", "gradient_scheme_4/"]

    box_plot_position = [[1.075, 3.075, 5.075, 7.075], [1.425, 3.425, 5.425, 7.425], [1.875, 3.875, 5.875, 7.875]]

    eig_val = utilities.get_gt_eigen_val()
    gt_fa = 0.8
    gt_md = 0.8
    gt_ad, gt_rd = utilities.get_gtound_truth_ad_rd_from_eigen_vals(gt_fa, gt_md, eig_val)

    fc_colors = ['gray', 'red', 'purple']

    fig, ax = plt.subplots(1, 2, figsize=(8,4))
    # fig.suptitle("Experiment 3")
    fig.subplots_adjust(
        top=0.9,
        bottom=0.14,
        left=0.09,
        right=0.945,
        hspace=0.2,
        wspace=0.355
    )

    markers = ['x']
    for m_, method in enumerate(['/mrtrix', '/dtiRIM_new_dataset_correct_bvec_rotation']):
        mean_bias_fa_x_ = []
        std_bias_fa_x_ = []
        mean_bias_md_x_ = []
        std_bias_md_x_ = []
        mean_bias_ad_x_ = []
        std_bias_ad_x_ = []
        mean_bias_rd_x_ = []
        std_bias_rd_x_ = []

        for grad_ in grad_list:
            folder_ = data_path + "correct_b0/" + grad_ + method

            list_dir = [f for f in (os.listdir(folder_)) if f.endswith(".pkl")]

            
            total_error_fa = []
            total_error_md = []
            total_error_ad = []
            total_error_rd = []
            for fi, f_ in enumerate(list_dir):
                if fi ==100:
                    break

                path_file = folder_ + "/" + f_
                print("read file: {}".format(path_file))
                est_fa, est_md, est_ad, est_rd, est_ev = utilities.get_results_data(path_file, 3, 4)
                if method == "/mrtrix":
                    est_md *= 1000
                    est_ev *= 1000
                    est_ad *= 1000
                    est_rd *= 1000
                
                fa_error = est_fa
                md_error = est_md
                ad_error = est_ad
                rd_error = est_rd

                total_error_fa.append(fa_error)
                total_error_md.append(md_error)
                total_error_ad.append(ad_error)
                total_error_rd.append(rd_error)
            
            mean_bias_fa_x_.append(np.mean(total_error_fa, 0))
            std_bias_fa_x_.append(np.std(total_error_fa,0))
            mean_bias_md_x_.append(np.mean(total_error_md, 0))
            std_bias_md_x_.append(np.std(total_error_md,0))
            mean_bias_ad_x_.append(np.mean(total_error_ad, 0))
            std_bias_ad_x_.append(np.std(total_error_ad,0))
            mean_bias_rd_x_.append(np.mean(total_error_rd, 0))
            std_bias_rd_x_.append(np.std(total_error_rd,0))


        # ax[0,0].errorbar(box_plot_position[m_], mean_bias_fa_x_, yerr=std_bias_fa_x_, marker='x', linestyle='', color=fc_colors[m_], alpha=1)
        # ax[0,1].errorbar(box_plot_position[m_], mean_bias_md_x_, yerr=std_bias_md_x_, marker='x', linestyle='', color=fc_colors[m_], alpha=1)
        # ax[1,0].errorbar(box_plot_position[m_], mean_bias_ad_x_, yerr=std_bias_ad_x_, marker='x', linestyle='', color=fc_colors[m_], alpha=1)
        # ax[1,1].errorbar(box_plot_position[m_], mean_bias_rd_x_, yerr=std_bias_rd_x_, marker='x', linestyle='', color=fc_colors[m_], alpha=1)
        # ax[0,0].axhline(gt_fa, linestyle='--', color='black', linewidth=0.3)
        # ax[0,1].axhline(gt_md, linestyle='--', color='black', linewidth=0.3)
        # ax[1,0].axhline(gt_ad, linestyle='--', color='black', linewidth=0.3)
        # ax[1,1].axhline(gt_rd, linestyle='--', color='black', linewidth=0.3)
        # ax[0,0].set_ylabel(r"Estimated FA")
        # ax[0,0].set_xticks([1.25, 3.25, 5.25, 7.25])
        # ax[0,0].set_xticklabels(["$\zeta_{t1}$", "$\zeta_{t2}$", "$\zeta_{t3}$", "$\zeta_{t4}$"])
        # ax[0,0].title.set_text(r"FA dependency on testing q-space $\zeta_{t}$")
        # ax[0,1].set_ylabel(r"Estimated MD $[mm^2/ms]$")
        # ax[0,1].set_xticks([1.25, 3.25, 5.25, 7.25])
        # ax[0,1].set_xticklabels(["$\zeta_{t1}$", "$\zeta_{t2}$", "$\zeta_{t3}$", "$\zeta_{t4}$"])
        # ax[0,1].title.set_text(r"MD dependency on testing q-space $\zeta_{t}$")
        # ax[1,0].set_ylabel(r"Estimated AD $[mm^2/ms]$")
        # ax[1,0].set_xticks([1.25, 3.25, 5.25, 7.25])
        # ax[1,0].set_xticklabels(["$\zeta_{t1}$", "$\zeta_{t2}$", "$\zeta_{t3}$", "$\zeta_{t4}$"])
        # ax[1,0].title.set_text(r"FA dependency on testing q-space $\zeta_{t}$")
        # ax[1,1].set_ylabel(r"Estimated RD $[mm^2/ms]$")
        # ax[1,1].set_xticks([1.25, 3.25, 5.25, 7.25])
        # ax[1,1].set_xticklabels(["$\zeta_{t1}$", "$\zeta_{t2}$", "$\zeta_{t3}$", "$\zeta_{t4}$"])
        # ax[1,1].title.set_text(r"MD dependency on testing q-space $\zeta_{t}$")

        ax[0].errorbar(box_plot_position[m_], mean_bias_fa_x_, yerr=std_bias_fa_x_, marker='x', linestyle='', color=fc_colors[m_], alpha=1)
        ax[1].errorbar(box_plot_position[m_], mean_bias_md_x_, yerr=std_bias_md_x_, marker='x', linestyle='', color=fc_colors[m_], alpha=1)
        ax[0].axhline(gt_fa, linestyle='--', color='black', linewidth=0.3)
        ax[1].axhline(gt_md, linestyle='--', color='black', linewidth=0.3)
        ax[0].set_ylabel(r"Estimated FA")
        ax[0].set_xticks([1.25, 3.25, 5.25, 7.25])
        ax[0].set_xticklabels(["$\zeta_{t1}$", "$\zeta_{t2}$", "$\zeta_{t3}$", "$\zeta_{t4}$"])
        ax[0].title.set_text(r"FA dependency on testing q-space $\zeta_{t}$")
        ax[1].set_ylabel(r"Estimated MD $[mm^2/ms]$")
        ax[1].set_xticks([1.25, 3.25, 5.25, 7.25])
        ax[1].set_xticklabels(["$\zeta_{t1}$", "$\zeta_{t2}$", "$\zeta_{t3}$", "$\zeta_{t4}$"])
        ax[1].title.set_text(r"MD dependency on testing q-space $\zeta_{t}$")
        

    mrtrix_patch = mpatches.Patch(facecolor='gray', edgecolor='black', label=r'$\mathit{\mathbf{IWLLS}}$')
    aRIM_patch = mpatches.Patch(facecolor='red', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$')
    plt.legend(handles=[mrtrix_patch, aRIM_patch], loc='lower center')#, bbox_to_anchor=(0.5, -0.55), loc='center')

    plt.show()






