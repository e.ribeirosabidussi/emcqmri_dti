import utilities

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import os
import pickle

matplotlib.rc('font', family='serif', serif='cm10')
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']


if __name__ == '__main__':
    data_path = "/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_2/"
    n_list = ([7, 13, 20, 27, 34, 40, 47, 54, 60, 68, 75, 81, 88, 95, 102, 108, 115, 122, 129, 135])

    eig_val = utilities.get_gt_eigen_val()
    gt_fa = 0.8
    gt_md = 0.8
    gt_ad, gt_rd = utilities.get_gtound_truth_ad_rd_from_eigen_vals(gt_fa, gt_md, eig_val)

    colors = ['gray', 'red', 'purple']

    fig, ax = plt.subplots(1, 2, figsize=(8,4))
    fig.subplots_adjust(
        top=0.9,
        bottom=0.14,
        left=0.09,
        right=0.945,
        hspace=0.2,
        wspace=0.355
    )

    for m_, method in enumerate(['/mrtrix', '/dtiRIM_no_background_new_loss']):
        mean_bias_fa_x_ = []
        std_bias_fa_x_ = []
        mean_bias_md_x_ = []
        std_bias_md_x_ = []
        mean_bias_ad_x_ = []
        std_bias_ad_x_ = []
        mean_bias_rd_x_ = []
        std_bias_rd_x_ = []
        ni = 0
        for n_ in n_list:
            folder_ = data_path + "correct_b0/n_" + str(n_) + method
            list_dir = [f for f in (os.listdir(folder_)) if f.endswith(".pkl")]
            
            total_error_fa = []
            total_error_md = []
            total_error_ad = []
            total_error_rd = []
            for fi, f_ in enumerate(list_dir):

                if fi ==100:
                    break
                path_file = folder_ + "/" + f_
                print("read file: {}".format(path_file))
                est_fa, est_md, est_ad, est_rd, est_ev = utilities.get_results_data(path_file, 5, 5)
                if method == "/mrtrix":
                    est_md *= 1000
                    est_ev *= 1000
                    est_ad *= 1000
                    est_rd *= 1000

                fa_error = est_fa
                md_error = est_md
                ad_error = est_ad
                rd_error = est_rd

                total_error_fa.append(fa_error)
                total_error_md.append(md_error)
                total_error_ad.append(ad_error)
                total_error_rd.append(rd_error)
            
            mean_bias_fa_x_.append(np.mean(total_error_fa, 0))
            std_bias_fa_x_.append(np.std(total_error_fa,0))
            mean_bias_md_x_.append(np.mean(total_error_md, 0))
            std_bias_md_x_.append(np.std(total_error_md,0))
            mean_bias_ad_x_.append(np.mean(total_error_ad, 0))
            std_bias_ad_x_.append(np.std(total_error_ad,0))
            mean_bias_rd_x_.append(np.mean(total_error_rd, 0))
            std_bias_rd_x_.append(np.std(total_error_rd,0))

        utilities.plot_comparison(n_list, mean_bias_fa_x_, std_bias_fa_x_, colors[m_], 
                                  0.2, gt_fa, fig, ax[0], r"$N$", r"Estimated FA", 
                                  n_list[::3], r"FA dependency on number of images $N$")

        utilities.plot_comparison(n_list, mean_bias_md_x_, std_bias_md_x_, colors[m_], 
                                  0.2, gt_md, fig, ax[1], r"$N$", r"Estimated MD $[mm^2/ms]$", 
                                  n_list[::3], r"MD dependency on number of images $N$")

        # utilities.plot_comparison(n_list, mean_bias_ad_x_, std_bias_ad_x_, colors[m_], 
        #                           0.2, gt_ad, fig, ax[1,0], r"$N$", r"Estimated AD $[mm^2/ms]$", 
        #                           n_list[::3], r"AD dependency on number of images $N$")

        # utilities.plot_comparison(n_list, mean_bias_rd_x_, std_bias_rd_x_, colors[m_], 
        #                           0.2, gt_rd, fig, ax[1,1], r"$N$", r"Estimated RD $[mm^2/ms]$", 
        #                           n_list[::3], r"RD dependency on number of images $N$")

    mrtrix_patch = mpatches.Patch(facecolor='gray', edgecolor='black', label=r'$\mathit{\mathbf{IRLLS}}$')
    aRIM_patch = mpatches.Patch(facecolor='red', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$')
    plt.legend(handles=[mrtrix_patch, aRIM_patch])

    plt.show()






