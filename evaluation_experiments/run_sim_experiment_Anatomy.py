import utilities

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import os
import pickle

matplotlib.rc('font', family='serif', serif='cm10')
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']


def get_results_data(file_path, size_):
    
    with open(file_path, 'rb') as f:
        data = pickle.load(f)
    estimated_tensor = data["diffusion_tensors"]#[:,5,5]

    if size_ == '/2x2':
        ind_x = 4
        ind_y = 5
        e_val, e_vec = utilities.from_tensor_to_eig_v(estimated_tensor[1:])
        estimated_ad_map = utilities.axial_diffusivity_(e_val)[ind_x, ind_y]
        estimated_rd_map = utilities.radial_diffusivity_(e_val)[ind_x, ind_y]
        estimated_md_map = utilities.mean_diffusivity_(e_val)[ind_x, ind_y]
        estimated_fa_map = utilities.fractional_anisotropy_(e_val)[ind_x, ind_y]
    elif size_ == '/3x3':
        ind_x = 3
        ind_y = 4
        e_val, e_vec = utilities.from_tensor_to_eig_v(estimated_tensor[1:])
        estimated_ad_map = utilities.axial_diffusivity_(e_val)[ind_x, ind_y]
        estimated_rd_map = utilities.radial_diffusivity_(e_val)[ind_x, ind_y]
        estimated_md_map = utilities.mean_diffusivity_(e_val)[ind_x, ind_y]
        estimated_fa_map = utilities.fractional_anisotropy_(e_val)[ind_x, ind_y]
    else:
        ind_x = 4
        ind_y = 4
        e_val, e_vec = utilities.from_tensor_to_eig_v(estimated_tensor[1:])
        estimated_ad_map = utilities.axial_diffusivity_(e_val)[ind_x, ind_y]
        estimated_rd_map = utilities.radial_diffusivity_(e_val)[ind_x, ind_y]
        estimated_md_map = utilities.mean_diffusivity_(e_val)[ind_x, ind_y]
        estimated_fa_map = utilities.fractional_anisotropy_(e_val)[ind_x, ind_y]

    return estimated_fa_map, estimated_md_map, estimated_ad_map, estimated_rd_map, np.array(estimated_tensor[:, ind_x, ind_y].detach().numpy())



def box_plot(data, edge_color, fill_color, positions_, ax):
    bp = ax.boxplot(data, positions=positions_,patch_artist=True, showfliers=False)
    
    for patch in bp['boxes']:
        patch.set(facecolor=fill_color, alpha=0.3)    
        
    for element in ['whiskers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)
    return bp


if __name__ == '__main__':
    data_path = "/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_6/"
    fa_list = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    
    eig_val = utilities.get_gt_eigen_val()
    gt_fa = fa_list.copy()
    gt_md = fa_list.copy()
    
    hr_list = np.arange(0.1,1,0.01)
    ad_list = []
    rd_list = []
    for hr in hr_list:
        gt_ad, gt_rd = utilities.get_gtound_truth_ad_rd_from_eigen_vals(hr, hr, eig_val)
        ad_list.append(gt_ad)
        rd_list.append(gt_rd)

    colors = ['gray', 'purple', 'royalblue', 'red']
    alphas = [0.3, 0.1, 0.1, 0.1]

    fig, ax = plt.subplots(1, 2, figsize=(8,4))
    fig.subplots_adjust(
        top=0.9,
        bottom=0.14,
        left=0.09,
        right=0.945,
        hspace=0.2,
        wspace=0.355
    )
    

    for m_, size_ in enumerate(['/1x1/mrtrix', '/1x1', '/2x2', '/3x3']):
        
        mean_bias_fa_x_ = []
        std_bias_fa_x_ = []
        mean_bias_md_x_ = []
        std_bias_md_x_ = []
        mean_bias_ad_x_ = []
        std_bias_ad_x_ = []
        mean_bias_rd_x_ = []
        std_bias_rd_x_ = []
        for fa_ in fa_list:
            if size_=='/1x1/mrtrix':
                method = '/mrtrix'
                size_inst = '/1x1'
            else:
                method = '/dtiRIM_no_background_new_loss'
                size_inst = size_

            folder_ = data_path + "anatomy/correct_b0" + size_inst + "/central_val_" + str(fa_) + method
            list_dir = [f for f in (os.listdir(folder_)) if f.endswith(".pkl")]

            total_error_fa = []
            total_error_md = []
            total_error_ad = []
            total_error_rd = []
            for fi, f_ in enumerate(list_dir):

                path_file = folder_ + "/" + f_
                print("read file: {}".format(path_file))
                est_fa, est_md, est_ad, est_rd, est_ev = get_results_data(path_file, size_inst)
                if method == "/mrtrix":
                    est_md *= 1000
                    est_ev *= 1000
                    est_ad *= 1000
                    est_rd *= 1000
                
                fa_error = est_fa
                md_error = est_md
                ad_error = est_ad
                rd_error = est_rd

                total_error_fa.append(fa_error)
                total_error_md.append(md_error)
                total_error_ad.append(ad_error)
                total_error_rd.append(rd_error)
            
            mean_bias_fa_x_.append(np.mean(total_error_fa, 0))
            std_bias_fa_x_.append(np.std(total_error_fa,0))
            mean_bias_md_x_.append(np.mean(total_error_md, 0))
            std_bias_md_x_.append(np.std(total_error_md,0))
            mean_bias_ad_x_.append(np.mean(total_error_ad, 0))
            std_bias_ad_x_.append(np.std(total_error_ad,0))
            mean_bias_rd_x_.append(np.mean(total_error_rd, 0))
            std_bias_rd_x_.append(np.std(total_error_rd,0))

        utilities.plot_comparison(fa_list, mean_bias_fa_x_, std_bias_fa_x_, colors[m_], 
                                  0.2, -1, fig, ax[0], r"FA and MD of central pixel", r"Estimated FA", 
                                  fa_list, r"FA dependency on anatomy")
        ax[0].axvline(0.5, linestyle='--', color='gray', linewidth=0.5)
        ax[0].plot(fa_list, fa_list, linestyle='--', color='black', linewidth=1)

        utilities.plot_comparison(fa_list, mean_bias_md_x_, std_bias_md_x_, colors[m_], 
                                  0.2, -1, fig, ax[1], r"FA and MD of central pixel", r"Estimated MD $[mm^2/ms]$", 
                                  fa_list, r"MD dependency on anatomy")
        ax[1].axvline(0.5, linestyle='--', color='gray', linewidth=0.5)
        ax[1].plot(fa_list, fa_list, linestyle='--', color='black', linewidth=1)

        # utilities.plot_comparison(fa_list, mean_bias_ad_x_, std_bias_ad_x_, colors[m_], 
        #                           0.2, -1, fig, ax[1,0], r"FA and MD of central pixel", r"Estimated AD $[mm^2/ms]$", 
        #                           fa_list, r"AD dependency on anatomy")
        # ax[1,0].plot(hr_list, ad_list, linestyle='--', color='black', linewidth=1)

        # utilities.plot_comparison(fa_list, mean_bias_rd_x_, std_bias_rd_x_, colors[m_], 
        #                           0.2, -1, fig, ax[1,1], r"FA and MD of central pixel", r"Estimated RD $[mm^2/ms]$", 
        #                           fa_list, r"RD dependency on anatomy")
        # ax[1,1].plot(hr_list, rd_list, linestyle='--', color='black', linewidth=1)


    mrtrix_patch = mpatches.Patch(facecolor='gray', edgecolor='black', label=r'$\mathit{\mathbf{IRLLS}}$')
    aRIM_patch_1x1 = mpatches.Patch(facecolor='purple', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$ - Structure with $1x1$ pixel')
    aRIM_patch_2x2 = mpatches.Patch(facecolor='royalblue', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$ - Structure with $2x2$ pixels')
    aRIM_patch_3x3 = mpatches.Patch(facecolor='red', edgecolor='black', label=r'dti$\mathit{\mathbf{RIM}}$ - Structure with $3x3$ pixels')
    plt.legend(handles=[mrtrix_patch, aRIM_patch_1x1, aRIM_patch_2x2, aRIM_patch_3x3])#, bbox_to_anchor=(0.5, -0.55), loc='center')


    plt.show()
