
import utilities

import cmasher as cmr
from dipy.reconst.dti import lower_triangular, eig_from_lo_tri
from dipy.reconst.dti import fractional_anisotropy, color_fa, mean_diffusivity
from dipy.viz import window, actor
from dipy.data import get_sphere
import dipy.data as dpd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.ticker import FormatStrFormatter
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
import numpy as np
import os
import pickle
from scipy.ndimage.morphology import binary_fill_holes

matplotlib.rc('font', family='serif', serif='cm10')
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r'\boldmath']


def build_d_mat(d_tensors):
    d_mat = np.array([[d_tensors[0], d_tensors[1], d_tensors[2]],
                     [d_tensors[1], d_tensors[3], d_tensors[4]],
                     [d_tensors[2], d_tensors[4], d_tensors[5]]])

    d_mat_perm = np.transpose(d_mat, (-2, -1, 0, 1))
    return d_mat_perm

def from_tensor_to_eig_v(tensors):
    d_mat = build_d_mat(np.stack(tensors))
    l_triang = lower_triangular(d_mat)
    eig_v = eig_from_lo_tri(l_triang, min_diffusivity=1e-6)
    e_val = eig_v[...,:3]
    e_vec = eig_v[...,3:12]
    sh_tensor = tensors.shape[-2:]
    e_vec = e_vec.reshape(*sh_tensor, 3, 3)
    return e_val, e_vec



def mask_array(array_, mask):
    if (len(array_.shape)>2) and (len(array_)>3):
        masked_arr = []
        for arr_ in array_:
            masked_arr.append(np.ma.masked_array(arr_, mask=np.logical_not(mask)))
    else:
        masked_arr = np.ma.masked_array(array_, mask=np.logical_not(mask))
    return masked_arr

def read_gt(path):
    with open(path, 'rb') as f:
        data = pickle.load(f)

    f.close()

    dti_tensor = data['diffusion_tensors'].detach().numpy()
    predicted_signal = data['predicted_signal'].detach().numpy()
    label_signal = data['label'][0][0].detach().numpy()
    fa_map = np.reshape(data['fa_map'], (96, 96))
    md_map = np.reshape(data['md_map'], (96, 96))

    fa_map = np.transpose(fa_map, (1, 0))
    md_map = np.transpose(md_map, (1, 0))
    

    mask = np.zeros_like(label_signal[0])
    mask[label_signal[1]>60] = 1
    mask = utilities.getLargestCC(mask)
    mask = binary_fill_holes(mask)
    mask = np.transpose(mask, (1, 0))

    return label_signal, fa_map, md_map, dti_tensor, mask




def add_inset(map_, ax, vmin_, vmax_, inset_coord, xy_coord, cmap_):
    axins0_fa = ax.inset_axes(inset_coord)
    x1, x2, y1, y2 = xy_coord
    axins0_fa.set_xlim(x1, x2)
    axins0_fa.set_ylim(y1, y2)
    axins0_fa.set_xticklabels('')
    axins0_fa.set_xticks([])
    axins0_fa.set_yticklabels('')
    axins0_fa.set_yticks([])
    axins0_fa.imshow(map_, vmin=vmin_, vmax=vmax_, cmap=cmap_, interpolation='none')
    utilities.my_mark_inset(ax, axins0_fa, loc1a=4, loc1b=2, loc2a=1, loc2b=3, fc="none", ec='red')
    axins0_fa.spines['bottom'].set_color('red')
    axins0_fa.spines['top'].set_color('red') 
    axins0_fa.spines['right'].set_color('red')
    axins0_fa.spines['left'].set_color('red')

def plot_fully_sample_results(fa_gt_mrtrix, md_gt_mrtrix, fa_gt_dtiRIM, md_gt_dtiRIM):

    # plt.style.use('dark_background')
    colors_colormap = ['blue', 'black', 'red']
    cm = LinearSegmentedColormap.from_list('error_map', colors_colormap, N=100)
    fig, ax = plt.subplots(2,2, figsize=(10,10))
    im_fa = ax[0,0].imshow(fa_gt_mrtrix, vmin=0, vmax=1, cmap='gray', interpolation='None')
    im_fa = ax[1,0].imshow(fa_gt_dtiRIM, vmin=0, vmax=1, cmap='gray', interpolation='None')
    # diff_fa = ax[2,0].imshow((fa_gt_mrtrix-fa_gt_dtiRIM), vmin=-0.1, vmax=0.1, cmap=cm, interpolation='None')
    im_md = ax[0,1].imshow(md_gt_mrtrix, vmin=0, vmax=2, cmap='gray', interpolation='None')
    im_md = ax[1,1].imshow(md_gt_dtiRIM, vmin=0, vmax=2, cmap='gray', interpolation='None')
    # diff_md = ax[2,1].imshow((md_gt_mrtrix-md_gt_dtiRIM), vmin=-0.2, vmax=0.2, cmap=cm, interpolation='None')
    ax[0,0].set_title(r'a)', y=0.95, fontsize=15)
    ax[1,0].set_title(r'b)', y=0.95, fontsize=15)
    # ax[2,0].set_title(r'c)', y=0.95, fontsize=15)
    ax[0,1].set_title(r'd)', y=0.95, fontsize=15)
    ax[1,1].set_title(r'e)', y=0.95, fontsize=15)
    # ax[2,1].set_title(r'f)', y=0.95, fontsize=15)
    
    for ax_ in ax.flatten():
        ax_.axis('off')

    add_inset(fa_gt_mrtrix, ax[0,0], 0, 1, [-1.2, 0.15, 1.15, 1.15], [23, 48, 38, 13], 'gray')
    add_inset(md_gt_mrtrix, ax[0,1], 0, 2, [-1.2, 0.15, 1.15, 1.15], [28, 53, 40, 15], 'gray')
    add_inset(fa_gt_dtiRIM, ax[1,0], 0, 1, [-1.2, 0.15, 1.15, 1.15], [23, 48, 38, 13], 'gray')
    add_inset(md_gt_dtiRIM, ax[1,1], 0, 2, [-1.2, 0.15, 1.15, 1.15], [28, 53, 40, 15], 'gray')
    # add_inset(fa_gt_mrtrix-fa_gt_dtiRIM, ax[2,0], -0.1, 0.1, [-1.2, 0.15, 1.15, 1.15], [13, 38, 38, 13], cm)
    # add_inset(md_gt_mrtrix-md_gt_dtiRIM, ax[2,1], -0.2, 0.2, [-1.2, 0.15, 1.15, 1.15], [45, 70, 55, 30], cm)

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.425, 0.05, 0.01, 0.75])
    cb= fig.colorbar(im_fa, cax=cbar_ax, orientation="vertical")
    cb.set_label(label='FA',weight='bold', size=15, labelpad=10)
    cb.ax.tick_params(labelsize=15)
    cb.ax.locator_params(nbins=6)

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.92, 0.05, 0.01, 0.75])
    cb= fig.colorbar(im_md, cax=cbar_ax, orientation="vertical")
    cb.set_label(label=r"MD $[mm^2/ms]$", weight='bold', size=15, labelpad=10)
    cb.ax.tick_params(labelsize=15)
    cb.ax.locator_params(nbins=8)

    # fig.subplots_adjust(right=0.8)
    # cbar_ax = fig.add_axes([0.42, 0.02, 0.01, 0.2])
    # cb= fig.colorbar(diff_fa, cax=cbar_ax, orientation="vertical")
    # cbar_ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    # cb.set_label(label='FA',weight='bold', size=15, labelpad=10)
    # cb.ax.tick_params(labelsize=15)
    # cb.ax.locator_params(nbins=3)

    # fig.subplots_adjust(right=0.8)
    # cbar_ax = fig.add_axes([0.85, 0.02, 0.01, 0.2])
    # cb= fig.colorbar(diff_md, cax=cbar_ax, orientation="vertical")
    # cbar_ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    # cb.set_label(label=r"MD $[mm^2/ms]$",weight='bold', size=15, labelpad=10)
    # cb.ax.tick_params(labelsize=15)
    # cb.ax.locator_params(nbins=5)

    fig.tight_layout()
    fig.subplots_adjust(
        top=0.943,
        bottom=0.019,
        left=0.132,
        right=0.988,
        hspace=0.10,
        wspace=0.0
    )
    # plt.show()
# 

def plot_repeatability():
    pass


def viz_odf(e_val, e_vec, fa_map, mask):
    fa_map *= mask
    fa_map[fa_map<0.15]=0
    color_bar = color_fa(fa_map, e_vec)
    

    e_val = np.flip(np.transpose(e_val[:,:,None,...], (1, 0, 2, 3)), axis=1)
    e_vec = np.flip(np.transpose(e_vec[:,:,None,...], (1, 0, 2, 3, 4)), axis=1)
    color_bar = np.flip(np.transpose(color_bar[:,:,None,...], (1, 0, 2, 3)), axis=1)


    sphere = get_sphere('repulsion724')
    interactive = False
    cfa = color_bar
    cfa /= cfa.max()

    fa_map = np.flip(np.transpose(fa_map[:,:,None], (1, 0, 2)), axis=1)
    fa_map_actor = actor.slicer(fa_map, affine=np.eye(4), interpolation='nearest')
    slicer_opacity = 0.3
    fa_map_actor.opacity(slicer_opacity)

    ren = window.Scene()
    ren.add(fa_map_actor)
    ren.add(actor.tensor_slicer(e_val, e_vec, scalar_colors=cfa, sphere=sphere,
                                scale=0.55))

    return ren


if __name__ == '__main__':
    
    gt_path_mrtrix = '/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/in-vivo_exp_3/original/mrtrix/dwipatch_0.pkl'
    gt_path_dtiRIM = '/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/in-vivo_exp_3/original/dtiRIM_no_background/dwipatch_0.pkl'
    label_signal, fa_gt_mrtrix, md_gt_mrtrix, est_tensor_mrtrix, mask = read_gt(gt_path_mrtrix)
    label_dtiRIM, fa_gt_dtiRIM, md_gt_dtiRIM, est_tensor_dtiRIM, mask = read_gt(gt_path_dtiRIM)
    md_gt_mrtrix *= 1000
    fa_gt_mrtrix = mask_array(fa_gt_mrtrix, mask)
    md_gt_mrtrix = mask_array(md_gt_mrtrix,mask)
    fa_gt_dtiRIM = mask_array(fa_gt_dtiRIM,mask)
    md_gt_dtiRIM = mask_array(md_gt_dtiRIM,mask)

    plot_fully_sample_results(fa_gt_mrtrix, md_gt_mrtrix, fa_gt_dtiRIM, md_gt_dtiRIM)


    fa_gt_list = [fa_gt_mrtrix, fa_gt_dtiRIM]
    md_gt_list = [md_gt_mrtrix, md_gt_dtiRIM]

    mask_fa = mask.copy()
    mask_md = mask.copy()

    base_path = '/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/in-vivo_exp_3/g_'
    grad_dir = ['31', '25', '19', '13'] 
    methods = ['mrtrix', 'dtiRIM_no_background']
    alt_method = [r'$\mathit{\mathbf{IRLLS}}$', r'dti$\mathit{\mathbf{RIM}}$']
   
    titles = [r'$\mathbf{\zeta}_{31}$', r'$\mathbf{\zeta}_{25}$', r'$\mathbf{\zeta}_{19}$', r'$\mathbf{\zeta}_{13}$']
    titles = [[r'a)', r'b)', r'c)', r'd)'],[r'e)', r'f)', r'g)', r'h)']]

    colors = ['gray', 'red']
    # fig, ax = plt.subplots(1, 2, figsize=(8,4))
    # fig.subplots_adjust(
    #     top=0.9,
    #     bottom=0.14,
    #     left=0.09,
    #     right=0.945,
    #     hspace=0.2,
    #     wspace=0.355
    # )

    im_fig, im_ax = plt.subplots(2, 4, figsize=(15, 12))
    im_fig_md, im_ax_md = plt.subplots(2, 4, figsize=(15, 12))
    im_fig.subplots_adjust(
        top=0.93,
        bottom=0.005,
        left=0.205,
        right=0.8,
        hspace=0.0,
        wspace=0.0
    )
    im_fig_md.subplots_adjust(
        top=0.93,
        bottom=0.005,
        left=0.205,
        right=0.8,
        hspace=0.0,
        wspace=0.0
    )


    for m_, method in enumerate(methods):
        
        mean_bias_fa_snr_ = []
        std_bias_fa_snr_ = []
        mean_bias_md_snr_ = []
        std_bias_md_snr_ = []

        for g, grad in enumerate(grad_dir):
            predicted_signal_data = []
            label_signal_data = []
            fa_pred = []
            md_pred = []

            dti_ten_list = []

            path = base_path + grad + '/' + method
            list_files = os.listdir(path)
            for f_, file_ in enumerate(list_files):
                with open(path + "/" + file_, 'rb') as f:
                    data = pickle.load(f)
                    f.close()

                    dti_tensor = data['diffusion_tensors'].detach().numpy()
                    predicted_signal = data['predicted_signal'].detach().numpy()
                    label_signal = data['label'][0][0].detach().numpy()

                    fa_map = np.transpose(np.reshape(data['fa_map'], (96, 96)), (1, 0))
                    md_map = np.transpose(np.reshape(data['md_map'], (96, 96)), (1, 0))

                    if method == 'mrtrix':
                        md_map = md_map*1000

                    predicted_signal_data.append(predicted_signal)
                    label_signal_data.append(label_signal)

                    fa_pred.append(fa_map)
                    md_pred.append(md_map)
                    dti_ten_list.append(dti_tensor)
            
            # e_val, e_vec = from_tensor_to_eig_v(dti_ten_list[0][1:])
            # e_val = np.transpose(e_val, (1,0,2))
            # e_vec = np.transpose(e_vec, (1,0,2,3))
            # ren = viz_odf(e_val[5:35,25:55], e_vec[5:35,25:55], fa_pred[0][5:35,25:55], mask[5:35,25:55])
            # ren = viz_odf(e_val, e_vec, fa_pred[0], mask)
            # name_tensor_plot = method + '_' + grad 
            # window.record(ren, out_path='../../data/in_vivo_experiments/test_plot_tensor/' + name_tensor_plot + '_entire_image.png', magnification=3)
            # window.show(ren)

            
            pred_data = np.stack(predicted_signal_data)
            label_data = np.stack(label_signal_data)
            fa_data = np.stack(fa_pred)
            md_data = np.stack(md_pred)

            bias_fa, std_fa, rmse_fa = utilities.compute_fa_md_errors(fa_data, fa_gt_list[m_])
            bias_md, std_md, rmse_md = utilities.compute_fa_md_errors(md_data, md_gt_list[m_])

            for x in im_ax:
                for x_ in x:
                    x_.set_frame_on(False)
                    x_.set_xticklabels([])
                    x_.set_yticklabels([])
                    x_.set_xticks([])
                    x_.set_yticks([])
            
            for x in im_ax_md:
                for x_ in x:
                    x_.set_frame_on(False)
                    x_.set_xticklabels([])
                    x_.set_yticklabels([])
                    x_.set_xticks([])
                    x_.set_yticks([])

            bias_fa_masked = mask_array(bias_fa, mask)
            std_fa_masked = mask_array(std_fa, mask)
            rmse_fa_masked = mask_array(rmse_fa, mask)

            bias_md_masked = mask_array(bias_md, mask)
            std_md_masked = mask_array(std_md, mask)
            rmse_md_masked = mask_array(rmse_md, mask)


            im = im_ax[m_,g].imshow(mask_array(rmse_fa, mask), vmin=0, vmax=0.3, cmap=cmr.wildfire_2, interpolation='none')
            im_ax[m_,g].set_title(titles[m_][g], y=0.95, fontsize=15)
            im_md = im_ax_md[m_,g].imshow(mask_array(rmse_md, mask), vmin=0, vmax=0.2, cmap=cmr.wildfire_2, interpolation='none')
            im_ax_md[m_,g].set_title(titles[m_][g], y=0.95, fontsize=15)


            mean_bias = np.mean(bias_fa_masked)
            std_bias = np.mean(std_fa_masked)
            mean_bias_fa_snr_.append(mean_bias)
            std_bias_fa_snr_.append(std_bias)

            mean_bias = np.mean(bias_md_masked)
            std_bias = np.mean(std_md_masked)
            mean_bias_md_snr_.append(mean_bias)
            std_bias_md_snr_.append(std_bias)

            
        # ax[0].fill_between(grad_dir, (np.array(mean_bias_fa_snr_)-np.array(std_bias_fa_snr_)), (np.array(mean_bias_fa_snr_)+np.array(std_bias_fa_snr_)), color=colors[m_], alpha=.2)
        # ax[0].plot(grad_dir, mean_bias_fa_snr_, color=colors[m_], marker = 'o', linewidth=1.25, markersize=2.5)
        # ax[0].axhline(0.0, linestyle='--', color='black', linewidth=1)

        # ax[1].fill_between(grad_dir, (np.array(mean_bias_md_snr_)-np.array(std_bias_md_snr_)), (np.array(mean_bias_md_snr_)+np.array(std_bias_md_snr_)), color=colors[m_], alpha=.2)
        # ax[1].plot(grad_dir, mean_bias_md_snr_, color=colors[m_], marker = 'o', linewidth=1.25, markersize=2.5)
        # ax[1].axhline(0.0, linestyle='--', color='black', linewidth=1)
        
        # ax[0].set_xlabel(r"q-space")
        # ax[0].set_ylabel("Error FA")
        # ax[0].set_xticks(grad_dir)
        # ax[0].set_xticklabels([r"$gi_1$ $(N=31)$", r"$gi_1$ $(N=25)$", r"$gi_2$ $(N=19)$", r"$gi_3$ $(N=13)$"])
        # ax[0].title.set_text("In-vivo FA dependency on $N$")
        # ax[1].set_xlabel(r"q-space")
        # ax[1].set_ylabel(r"Error MD $[mm^2/ms]$")
        # ax[1].set_xticks(grad_dir)
        # ax[1].set_xticklabels([r"$gi_1$ $(N=31)$", r"$gi_1$ $(N=25)$", r"$gi_2$ $(N=19)$", r"$gi_3$ $(N=13)$"])
        # ax[1].title.set_text("In-vivo MD dependency on $N$")
    
    im_fig.subplots_adjust(right=0.8)
    cbar_ax = im_fig.add_axes([0.85, 0.08, 0.01, 0.8])
    cbar_ax.tick_params(labelsize=15)
    cb= im_fig.colorbar(im, cax=cbar_ax, orientation="vertical")
    cb.set_label(label='RMSE FA',weight='bold', size=15, labelpad=10)

    im_fig_md.subplots_adjust(right=0.8)
    cbar_ax = im_fig_md.add_axes([0.85, 0.08, 0.01, 0.8])
    cbar_ax.tick_params(labelsize=15)
    cb= im_fig_md.colorbar(im_md, cax=cbar_ax, orientation="vertical")
    cb.set_label(label=r"RMSE MD $[mm^2/ms]$",weight='bold', size=15, labelpad=10)





    # Plot the overlay of FA and tensor maps.
    # fig_ov, ax_ov = plt.subplots(2, 4, figsize=(15, 12))
    # fig_ov.subplots_adjust(
    #     top=0.93,
    #     bottom=0.005,
    #     left=0.205,
    #     right=0.8,
    #     hspace=0.0,
    #     wspace=0.0
    # )

    plt.show()

# fa_gt_mrtrix, md_gt_mrtrix, est_tensor_mrtrix

    # Get metrics
    # estimated_tensors = est_tensor_dtiRIM[1:]
    # e_val, e_vec = from_tensor_to_eig_v(estimated_tensors)
    # e_val = np.transpose(e_val, (1,0,2))
    # e_vec = np.transpose(e_vec, (1,0,2,3))
    # ren = viz_odf(e_val, e_vec, fa_gt_dtiRIM, mask_fa)

    # window.show(ren)