

method=custom_rim
save_method=dtiRIM_new_dataset_correct_bvec_rotation
locationGradient=./data/simulation/gradients_bval_experiment_correct_s0/training_bvecs_g60_b1000.txt
checkpointPath=./pre_trained/new_dataset/custom_rim_epoch_142_experiment_bigger_voxel_dataset_noBackground_newLoss.pth
fixed_SNR=30

### Experiment SNR
# for snr_value in $(seq 4 2 30); do
# save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_1/correct_b0/snr_$snr_value/$save_method
# data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_1/correct_b0/snr_$snr_value
# python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $locationGradient -SNR $snr_value -loadCheckpointPath $checkpointPath;
# done

# ## Experiment N
# n_value_list=("7" "13" "20" "27" "34" "40" "47" "54" "60" "68" "75" "81" "88" "95" "102" "108" "115" "122" "129" "135")
# n_value_list2=("6" "12" "18" "24" "30" "36" "42" "48" "54" "60" "66" "72" "78" "84" "90" "96" "102" "108" "114" "120")
# for nvalue nvalue2 (${n_value_list:^n_value_list2}); do
#     save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_2/correct_b0/n_$nvalue/$save_method
#     data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_2/correct_b0/n_$nvalue
#     grad_path=./data/simulation/gradient_files_correct_b0/gradient_info_n"$nvalue2"_b1000.txt
#     python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $grad_path -SNR $fixed_SNR -loadCheckpointPath $checkpointPath
# done

### Experiment G
g_list=("gradient_scheme_1" "gradient_scheme_2" "gradient_scheme_3" "gradient_scheme_4")
for g_value in $g_list; do
save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_3/correct_b0/$g_value/$save_method
data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_3/correct_b0/$g_value
grad_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_3/gradient_files/$g_value.txt
python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $grad_path -SNR $fixed_SNR -loadCheckpointPath $checkpointPath;
done

# ### Experiment B
# b_list=("0.4" "0.5" "0.6" "0.7" "0.8" "0.9" "1.0" "1.1" "1.2")
# b_list2=("400" "500" "600" "700" "800" "900" "1000" "1100" "1200")
# for bvalue bvalue2 (${b_list:^b_list2}); do
#     save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_4/correct_b0/bval_$bvalue/$save_method
#     data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_4/correct_b0/bval_$bvalue
#     grad_path=./data/simulation/gradients_bval_experiment_correct_s0/training_bvecs_g60_b$bvalue2.txt
#     echo --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $grad_path -SNR $fixed_SNR -loadCheckpointPath $checkpointPath;
#     python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $grad_path -SNR $fixed_SNR -loadCheckpointPath $checkpointPath;
# done


# ### Experiment FA
# fa_list=("0.05" "0.1" "0.15" "0.2" "0.25" "0.3" "0.35" "0.4" "0.45" "0.5" "0.55" "0.6" "0.65" "0.7" "0.75" "0.8" "0.85" "0.9" "0.95" "1.0")
# for fa_value in $fa_list; do
# save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_5/fa/correct_b0/fa_$fa_value/$save_method
# data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_5/correct_b0/fa_$fa_value
# python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $locationGradient -SNR $fixed_SNR -loadCheckpointPath $checkpointPath;
# done
# ### Experiment MD
# md_list=("0.05" "0.1" "0.15" "0.2" "0.25" "0.3" "0.35" "0.4" "0.45" "0.5" "0.55" "0.6" "0.65" "0.7" "0.75" "0.8" "0.85" "0.9" "0.95" "1.0")
# for md_value in $md_list; do
# save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_5/md/correct_b0/md_$md_value/$save_method
# data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_5/correct_b0/md_$md_value
# python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $locationGradient -SNR $fixed_SNR -loadCheckpointPath $checkpointPath;
# done

# ### Experiment Anatomy
# central_list=("0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" "0.8" "0.9" "1.0")
# # 1x1
# for central_value in $central_list; do
# save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_6/anatomy/correct_b0/1x1/central_val_$central_value/$save_method
# data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_6/correct_b0/anatomy/1x1/central_val_$central_value
# python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $locationGradient -SNR $fixed_SNR -loadCheckpointPath $checkpointPath;
# done
# # 2x2
# for central_value in $central_list; do
# save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_6/anatomy/correct_b0/2x2/central_val_$central_value/$save_method
# data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_6/correct_b0/anatomy/2x2/central_val_$central_value
# python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $locationGradient -SNR $fixed_SNR -loadCheckpointPath $checkpointPath;
# done
# # 3x3
# for central_value in $central_list; do
# save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_6/anatomy/correct_b0/3x3/central_val_$central_value/$save_method
# data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_6/correct_b0/anatomy/3x3/central_val_$central_value
# python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $locationGradient -SNR $fixed_SNR -loadCheckpointPath $checkpointPath;
# done


# ### Experiment Fiber Orientation
# # for orientation_index in $(seq 1 100); do
# # save_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/sim_exp_8_orientation/correct_b0/3x3/arbitrary_rotation_repeated/rotation_$orientation_index/$save_method
# # data_path=/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/simulation_experiment_8_orientation/correct_b0/3x3/arbitrary_rotation_repeated/rotation_$orientation_index
# # python3 estimate_dti.py --configurationFile custom_configuration/engine_configuration.txt -inferenceModel $method -testingDataPath $data_path -saveResultsPath $save_path -locationGradient $locationGradient -SNR $fixed_SNR -loadCheckpointPath $checkpointPath;
# # done


# ### Experiment Gradient Orientation
# # save_path=/