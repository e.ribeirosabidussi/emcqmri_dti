#!/bin/bash

export experiment_name=$1
export path_folder="/media/emanoel/HD/Emanoel_Documents/EMC/DTI_datasets/NeuroImage/results/"
export experiment="/correct_b0/"

declare -a SNR_LIST=("snr_4/" "snr_6/" "snr_8/" "snr_10/" "snr_12/" "snr_14/" "snr_16/" "snr_18/" "snr_20/" "snr_22/" "snr_24/" "snr_26/" "snr_28/" "snr_30/");
export sim_exp="/sim_exp_1/"
for snr_dir in ${SNR_LIST[@]}; do
    mkdir ${path_folder}${sim_exp}${experiment}${snr_dir}${experiment_name}
    echo "Created folder "${experiment_name} "at "${path_folder}${sim_exp}${experiment}${snr_dir}
done

declare -a N_LIST=(n_7/ n_13/ n_20/ n_27/ n_34/ n_40/ n_47/ n_54/ n_60/ n_68/ n_75/ n_81/ n_88/ n_95/ n_102/ n_108/ n_115/ n_122/ n_129/ n_135/);
export sim_exp="/sim_exp_2/"
for n_dir in ${N_LIST[@]}; do
    mkdir ${path_folder}${sim_exp}${experiment}${n_dir}${experiment_name}
    echo "Created folder "${experiment_name} "at "${path_folder}${sim_exp}${experiment}${n_dir}
done


declare -a Grad_LIST=(gradient_scheme_1/ gradient_scheme_2/ gradient_scheme_3/ gradient_scheme_4/);
export sim_exp="/sim_exp_3/"
for grad_dir in ${Grad_LIST[@]}; do
    mkdir ${path_folder}${sim_exp}${experiment}${grad_dir}${experiment_name}
    echo "Created folder "${experiment_name} "at "${path_folder}${sim_exp}${experiment}${grad_dir}
done


declare -a bval_LIST=(bval_0.4/ bval_0.5/ bval_0.6/ bval_0.7/ bval_0.8/ bval_0.9/ bval_1.0/ bval_1.1/ bval_1.2/);
export sim_exp="/sim_exp_4/"
for bval_dir in ${bval_LIST[@]}; do
    mkdir ${path_folder}${sim_exp}${experiment}${bval_dir}${experiment_name}
    echo "Created folder "${experiment_name} "at "${path_folder}${sim_exp}${experiment}${bval_dir}
done


declare -a fa_LIST=(fa_0.05/ fa_0.1/ fa_0.15/ fa_0.2/ fa_0.25/ fa_0.3/ fa_0.35/ fa_0.4/ fa_0.45/ fa_0.5/ fa_0.55/ fa_0.6/ fa_0.65/ fa_0.7/ fa_0.75/ fa_0.8/ fa_0.85/ fa_0.9/ fa_0.95/ fa_1.0/);
export sim_exp="/sim_exp_5/fa/"
for fa_dir in ${fa_LIST[@]}; do
    mkdir ${path_folder}${sim_exp}${experiment}${fa_dir}${experiment_name}
    echo "Created folder "${experiment_name} "at "${path_folder}${sim_exp}${experiment}${fa_dir}
done
declare -a md_LIST=(md_0.05/ md_0.1/ md_0.15/ md_0.2/ md_0.25/ md_0.3/ md_0.35/ md_0.4/ md_0.45/ md_0.5/ md_0.55/ md_0.6/ md_0.65/ md_0.7/ md_0.75/ md_0.8/ md_0.85/ md_0.9/ md_0.95/ md_1.0/);
export sim_exp="/sim_exp_5/md/"
for md_dir in ${md_LIST[@]}; do
    mkdir ${path_folder}${sim_exp}${experiment}${md_dir}${experiment_name}
    echo "Created folder "${experiment_name} "at "${path_folder}${sim_exp}${experiment}${md_dir}
done


declare -a anatomy_LIST=(central_val_0.1/ central_val_0.2/ central_val_0.3/ central_val_0.4/ central_val_0.5/ central_val_0.6/ central_val_0.7/ central_val_0.8/ central_val_0.9/ central_val_1.0/);
export sim_exp="/sim_exp_6/anatomy/"
for anatomy_dir in ${anatomy_LIST[@]}; do
    mkdir ${path_folder}${sim_exp}${experiment}"1x1/"${anatomy_dir}${experiment_name}
    mkdir ${path_folder}${sim_exp}${experiment}"2x2/"${anatomy_dir}${experiment_name}
    mkdir ${path_folder}${sim_exp}${experiment}"3x3/"${anatomy_dir}${experiment_name}
    echo "Created folder "${experiment_name} "at "${path_folder}${sim_exp}${experiment}${bval_dir}
done




declare -a anatomy_LIST=(orientation_0/ orientation_15/ orientation_30/ orientation_45/ orientation_60/ orientation_75/ orientation_90/);
export sim_exp="/sim_exp_8_orientation/"
for anatomy_dir in ${anatomy_LIST[@]}; do
    mkdir ${path_folder}${sim_exp}${experiment}"1x1/"${anatomy_dir}${experiment_name}
    mkdir ${path_folder}${sim_exp}${experiment}"2x2/"${anatomy_dir}${experiment_name}
    mkdir ${path_folder}${sim_exp}${experiment}"3x3/"${anatomy_dir}${experiment_name}
    # echo "Created folder "${experiment_name} "at "${path_folder}${sim_exp}${experiment}${bval_dir}
done



declare -a anatomy_LIST=(rotation_0/ rotation_15/ rotation_30/ rotation_45/ rotation_60/ rotation_75/ rotation_90/);
export sim_exp="/sim_exp_9_rotational_invariance/rotation/"
for anatomy_dir in ${anatomy_LIST[@]}; do
    mkdir ${path_folder}${sim_exp}${experiment}${anatomy_dir}${experiment_name}
    echo "Created folder "${experiment_name} "at "${path_folder}${sim_exp}${experiment}${anatomy_dir}
done