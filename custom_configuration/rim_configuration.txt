{ "method-configuration": {
    "inference-model": {
        "inputChannels": 14,
        "outputChannelsLayer1": 128,
        "outputChannelsLayer2": 128,
        "outputChannelsLayer3": 128,
        "outputChannels": 7,
        "inferenceSteps": 12
    }
}
}
