from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
from EMCqMRI.core.utilities import configuration_utilities


class Configuration(object):
    def __init__(self, configObject):
        self.configObject = configObject
        self.required = False

    def parse_configuration(self):
        group_task = self.configObject.parser.add_argument_group('dataloader_config')
        group_task.add_argument('-load_fixed_grad_dirs', required=self.required, type=configuration_utilities.str2bool, help='If true, location of gradient table is set in -locationGradient. If false, the gradient table must be placed in the same folder as the data, with similar name + "_grad_dir.txt"')
        group_task.add_argument('-locationGradient', required=self.required, type=str, help='Required if -load_fixed_grad_dirs is true')
        group_task.add_argument('-locationBvals', required=self.required, type=str, help='Required for FSL')
        group_task.add_argument('-locationBvecs', required=self.required, type=str, help='Required for FSL')
        group_task.add_argument('-slice_data', required=self.required, type=configuration_utilities.str2bool, help='Must be set to true if data is 3D.')
        group_task.add_argument('-z_slice', required=self.required, type=int, help='Specifies the selected slice if -slice_data is true')
        group_task.add_argument('-applyMotion', required=self.required, type=configuration_utilities.str2bool, help='If true, apply rigid deformations to the weighted images')
        group_task.add_argument('-resizeFactor', required=self.required, type=float, help='Values > or < than 1 will resize the images proportionally')
        group_task.add_argument('-cutoffFrequency', required=self.required, type=float, help='Fourier filtering. Only applied if -applyMotion is true')
        group_task.add_argument('-cropSize', required=self.required, type=int, help='Values > 0 will crop the data by equivalent number pixels')
        group_task.add_argument('-stdTranslation', required=self.required, type=float, help='Standard deviation of the xy translation parameters. Only applied if -applyMotion is true')
        group_task.add_argument('-stdRotation', required=self.required, type=float, help='Standard deviation of the rotation parameter. Only applied if -applyMotion is true')
        group_task.add_argument('-applyNoise', required=self.required, type=configuration_utilities.str2bool, help='If true, applies noise to the weighted images')
        group_task.add_argument('-SNR', required=self.required, type=float, help='(Only used during inference). Set the known SNR of the data. If SNR<0 the algorithms tries to estimate.')
        group_task.add_argument('-normalize', required=self.required, type=bool, help='If true, applies the normalization s/u, where s is the data, and u the standard deviation of the noise')
        
        config_args, _ = self.configObject.parser.parse_known_args()
        configuration = configuration_utilities.convert_argparse_to_attr(config_args)
        return configuration

