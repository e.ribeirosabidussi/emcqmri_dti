import torch
class dwiLoss(object):
    def __init__(self, config_object):
        self.args = config_object.args
        self.__name__ = "Dwi Loss"
        self.loss_ = torch.nn.MSELoss(reduction='sum')


    def __call__(self, estimate_, label):
        if self.args.engine.inference_model.__name__ == "RIM":
            estimate = torch.movedim(estimate_, 0, 1)   #First iterate through batches.
            loss_batch = 0
            for b, (batch_est, batch_lab) in enumerate(zip(estimate, label)):
                loss_inf_stps = 0
                for s, stp_est in enumerate(batch_est):
                    stp_dwi = self.args.engine.signal_model.forward(stp_est)

                    if self.args.dataset.normalize:
                        stp_dwi = self.args.engine.dataset_model.denormalize_data(stp_dwi)

                    loss_inf_stps = self.loss_(batch_lab, stp_dwi)
                loss_batch += loss_inf_stps/(s+1)
            loss = loss_batch/(b+1)

            return loss
        else:
            return torch.Tensor([0])