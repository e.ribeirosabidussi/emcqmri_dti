from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from EMCqMRI.core.utilities.image_utilities import unpad
from EMCqMRI.core.utilities.image_utilities import pad

import logging
import math
import numpy as np
import os
from scipy.ndimage import affine_transform
import sys
import torch
import torch.nn.functional as F
from torchvision.transforms import Resize

sys.path.insert(0, '../')


class BsplineTransform(object):
    def __init__(self, args):
        self.args = args

    def get_bspline_basis(self):
        pass


class DenseDeformationField(object):
    def __init__(self, configObj):
        self.args = configObj.args
    
    def convertAffineToGrid(self, image_data, affine_parameters_):
        size_data = [len(image_data), 1, *image_data[0].shape]
        affine_parameters = self.__convert_param_to_affine__(affine_parameters_, size_data[2])
        
        grid_per_patch = []    
        affine_matrix = self.get_affine_matrix(size_data, affine_parameters)
        grid = self.__create_field_from_affine__(size_data, affine_matrix=affine_matrix).to(self.args.engine.device)
        identity_grid = self.get_identity_grid(size_data)

        deformation_grid = grid - identity_grid
        grid_per_patch.append(deformation_grid)
        grid_per_patch = torch.stack(grid_per_patch)

        return grid_per_patch.movedim(-1,2).reshape([1, len(image_data)*2, *image_data[0].shape])

    def __convert_param_to_affine__(self, affine_parameters, data_size):
        aff_rotation = []
        aff_translation = []
        aff_param_copy = torch.zeros_like(affine_parameters)
        for ap, aff_p in enumerate(affine_parameters):
            div = aff_p[0]/(int(data_size)/2)
            aff_param_copy[ap, 0] = torch.atan(div) # This is relative to the image size.
            aff_param_copy[ap, 1:] = (aff_p[1:])/(data_size-1)
        return aff_param_copy

    def get_identity_grid(self, data_shape):
        motion_params = torch.zeros([data_shape[1], 6])
        affine_parameters = torch.zeros([data_shape[0], 3])
        affine_matrix = self.get_affine_matrix(data_shape, affine_parameters)
        grid_ = self.__create_field_from_affine__(data_shape, affine_matrix=affine_matrix)
        return grid_.to(self.args.engine.device)

    def get_affine_matrix(self, data_shape, motion_parameters):
        affine_matrix = torch.zeros([data_shape[0], 2, 3])

        for p, param_ in enumerate(motion_parameters):
            affine_matrix[p, 0, 2] = param_[1]
            affine_matrix[p, 1, 2] = param_[2]
            affine_matrix[p, 0, 0] = torch.cos(param_[0])
            affine_matrix[p, 1, 1] = torch.cos(param_[0])
            affine_matrix[p, 0, 1] = -torch.sin(param_[0])
            affine_matrix[p, 1, 0] = torch.sin(param_[0])
        return affine_matrix

    def __create_field_from_affine__(self, shape, affine_matrix):
        return F.affine_grid(affine_matrix, shape, align_corners=True).float()

    def __create_interp_grid__(self, shape, offset):
        x_s = shape[-2]
        y_s = shape[-1]
        x_coord = torch.arange(0, x_s, 1) + offset
        y_coord = torch.arange(0, y_s, 1) + offset

        return x_coord, y_coord

    def apply_deformation(self, image, kappa):
        data = image.unsqueeze(0).unsqueeze(0)
        deformed_data = self.__apply_deformation__(data, grid=kappa.unsqueeze(0))
        return deformed_data.squeeze()

    def __apply_deformation__(self, image, grid):
        grid_ = grid + self.get_identity_grid(image.shape)
        output = F.grid_sample(image.float(), grid_, mode='bspline', order=3, padding_mode="zeros", align_corners=True)
        return output

    def __filter__(self, image, type="fft", cutoff_frequency=1):
        if type == "fft":
            pass1 = torch.abs(torch.fft.rfftfreq(image.shape[-1])) < cutoff_frequency
            pass2 = torch.abs(torch.fft.fftfreq(image.shape[-2])) < cutoff_frequency
            kernel = torch.outer(pass2, pass1)

            fft_input = torch.fft.rfft2(image)
            output = torch.fft.irfft2(fft_input * kernel, s=image.shape[-2:]) # This has to be modified so it crops, instead of masking. Then the FFT filtering and image downsampling are performed in the same step.
        else:
            print("Wrong filter type. Only fft available")

        return output

    def undersample(self, image, factor=1):
        shape_image = image.shape[2:]   
        new_shape = [int(s/factor) for s in shape_image]
        if factor != 1:
            scaling_mat = torch.Tensor([[factor,0],[0,factor]])
            resize_im = []
            for im_ in image[0]:
                im_ = self.__filter__(im_, type='fft', cutoff_frequency=self.args.cutoffFrequency) # FFT Filter before downsampling
                resized_ = affine_transform(im_, scaling_mat, output_shape=new_shape, order=3, prefilter=True)
                resize_im.append(resized_)
            return torch.from_numpy(np.stack(resize_im)).unsqueeze(0)
        else:
            return image
    
    def __apply_inverse_deformation__(self, image, affine_parameters, grid=[], inverse=False):
        if inverse:
            affine_parameters = torch.multiply(affine_parameters, -1)
            grid = self.args.engine.signal_model.__convertAffineToGrid__(image[0], affine_parameters)
        if isinstance(grid, list):
            grid = torch.stack(grid)
        grid = grid.reshape([len(self.args.task.tau), 2,
                            *image[0].shape]).movedim(1, -1)
        weightedImages = []
        for t in range(len(self.args.task.tau)):
            data = image[t].unsqueeze(0).unsqueeze(0)
            deformed_w_images = self.apply_deformation(data, grid[t])
            weightedImages.append(deformed_w_images)

        return torch.stack(weightedImages).squeeze()


class Linear_Motion_Operator(object):
    def __init__(self, pad_size, args):
        self.pad_size = pad_size
        self.args = args

    def apply_rigid_transform(self, input_series, motion_params):
        deformed_w_images = []
        for w, wimage in enumerate(input_series):
            motion_params_translation = motion_params[w][0:3]
            motion_params_rotation = motion_params[w][3:6]

            theta = torch.zeros(1, 2, 3)
            theta[:, :2, :2] = torch.tensor([[np.cos(motion_params_rotation[0]), -1.0*np.sin(motion_params_rotation[0])],
                                            [np.sin(motion_params_rotation[0]), np.cos(motion_params_rotation[0])]])

            theta[:, 0, 2] = motion_params_translation[0]
            theta[:, 1, 2] = motion_params_translation[1]
            wimage = wimage.unsqueeze(0)
            wimage = wimage.unsqueeze(0)

            grid = F.affine_grid(theta, wimage.size()).to(self.args.device)
            deformed_ = F.grid_sample(wimage, grid)
            deformed_w_images.append(deformed_[0, 0])
        return torch.stack(deformed_w_images)


class Motion_Operator(object):
    def __init__(self, voxel_size, pad_size):
        self.voxel_size = voxel_size
        self.pad_size = pad_size

    def apply_rigid_transform(self, input_series, motion_params):
        motion_corrupt_series = []
        indx = 0
        if len(np.shape(input_series)) > 3:
            translation_dim = len(np.shape(input_series)) - 1
        else:
            translation_dim = len(np.shape(input_series))
        rotation_dim = len(np.shape(input_series)) - 2
        for weighted_img in input_series:
            print('Processing motion: ', indx)
            motion_params_translation = motion_params[indx][0:3]
            motion_params_rotation = motion_params[indx][3:6]
            padded_input_image, original_padding_matrix = pad(
                weighted_img, self.pad_size)

            rotated_image = self.__perform_motion_rotation__(
                padded_input_image, motion_params_rotation, rotation_dim)
            translated_image = self.__perform_motion_translation__(
                rotated_image, motion_params_translation, translation_dim)

            # unpad(translated_image.squeeze(), original_padding_matrix)
            unpadded_image = (translated_image.squeeze())
            motion_corrupt_series.append(unpadded_image.squeeze())
            indx += 1

        motion_corrupt_series = torch.stack(motion_corrupt_series)
        return motion_corrupt_series

    def __perform_motion_translation__(self, input_image, motion_params, img_dimension):
        shift_dim = np.divide(motion_params, self.voxel_size)

        N = np.shape(input_image)
        N_prim = np.add(N, 1)

        K = []
        for dim_indx in range(np.shape(N)[0]):
            K_f = np.linspace(
                0.0, ((N_prim[dim_indx] / 2) - 1), int((N_prim[dim_indx] / 2) + 1)) / N[dim_indx]
            K_s_a = np.linspace((N_prim[dim_indx] / 2), (N[dim_indx] - 1),
                                int(np.ceil((N[dim_indx] - 1) -
                                    (N_prim[dim_indx] / 2)))
                                )
            K_s = (K_s_a - N[dim_indx]) / N[dim_indx]
            K.append(np.concatenate((K_f, K_s), axis=0))

        K_mesh = np.meshgrid(*[x for x in K])
        phase_arg = 0

        for dim_indx in range(np.shape(N)[0]):
            phase_arg = phase_arg + \
                ((-1) * (2 * math.pi * shift_dim[dim_indx] * K_mesh[dim_indx]))

        phase_arg_matrix = torch.from_numpy(phase_arg).unsqueeze(-1)
        real_arg = torch.zeros_like(phase_arg_matrix)
        im_arg_th = torch.cat((real_arg, phase_arg_matrix), -1)

        if not len(np.shape(input_image)) > 2:
            input_image = input_image.unsqueeze(-1)
            im_arg_th = im_arg_th.unsqueeze(-2)

        input_matrix = input_image.unsqueeze(-1)
        zero_aux = torch.zeros_like(input_matrix)
        complex_input_image = torch.cat((input_matrix, zero_aux), -1)

        U_t = torch.fft(complex_input_image, img_dimension)

        Diag = torch.zeros_like(im_arg_th)
        phase_shifted_im = torch.zeros_like(U_t)

        for rz in range(np.shape(U_t)[2]):
            for ry in range(np.shape(U_t)[1]):
                for rx in range(np.shape(U_t)[0]):
                    Diag[rx, ry, rz, 0] = torch.exp(
                        im_arg_th[rx, ry, rz, 0])*(torch.cos(im_arg_th[rx, ry, rz, 1]))
                    Diag[rx, ry, rz, 1] = torch.exp(
                        im_arg_th[rx, ry, rz, 0])*(torch.sin(im_arg_th[rx, ry, rz, 1]))
                    phase_shifted_im[rx, ry, rz, 0] = (
                        (U_t[rx, ry, rz, 0] * Diag[rx, ry, rz, 0]) - (U_t[rx, ry, rz, 1] * Diag[rx, ry, rz, 1]))
                    phase_shifted_im[rx, ry, rz, 1] = (
                        U_t[rx, ry, rz, 0] * Diag[rx, ry, rz, 1]) + (U_t[rx, ry, rz, 1] * Diag[rx, ry, rz, 0])

        translated_im = torch.ifft(phase_shifted_im, img_dimension)

        return translated_im[..., 0]

    def __perform_motion_rotation__(self, input_image, motion_params, img_dimension):
        N = np.shape(input_image)
        N_prim = np.add(N, 1)
        delta_indx = np.arange(np.shape(self.voxel_size)[0])
        internal_delta = delta_indx

        if isinstance(input_image, torch.Tensor):
            aux_image = input_image
        else:
            aux_image = torch.from_numpy(input_image)

        vec_r = []
        K = []
        for dim_indx in range(np.shape(N)[0]):
            vec_us = np.linspace(-(N[dim_indx]) / 2,
                                 (N[dim_indx] - 1) / 2, int(N[dim_indx]))
            vec_ = np.fft.fftshift(vec_us)
            vec_r.append(np.reshape(vec_, [1, np.size(vec_)]))

            K_f = np.linspace(
                0.0, (N_prim[dim_indx] / 2) - 1, int(N_prim[dim_indx] / 2)) / N[dim_indx]
            K_s_a = np.linspace(
                (N_prim[dim_indx] / 2), N[dim_indx] - 0.5, int(N_prim[dim_indx] / 2))
            K_s = (K_s_a - N[dim_indx]) / N[dim_indx]
            K_a = np.concatenate((K_f, K_s), axis=0)

            K_ = np.fft.fftshift(K_a)
            K.append(np.reshape(K_, [1, np.size(K_)]))

        for dim_indx in range(np.shape(N)[0]):
            if (np.shape(N)[0] > 2):
                aux_image = torch.transpose(aux_image, int(
                    internal_delta[0]), int(internal_delta[1]))
                # Have to check if motion_params[dim_indx] is correct
                ang_param = self.__get_angle_params__(
                    internal_delta, motion_params[dim_indx])
                aux_image = self.__shearFun__(
                    aux_image, ang_param, internal_delta, K, vec_r, img_dimension)
            else:
                # Have to check if motion_params[0] is correct
                ang_param = self.__get_angle_params__(
                    internal_delta, motion_params[0])
                aux_image = self.__shearFun__(
                    aux_image, ang_param, internal_delta, K, vec_r, img_dimension)
                break

            if dim_indx % 2 == 0:
                internal_delta = np.flip(internal_delta)

            internal_delta = np.roll(internal_delta, dim_indx + 1)
        return aux_image

    def __shearFun__(self, input_image, ang_param, delta_vox_indx, K, vec_r, img_dim):
        input_image_matrix = input_image.unsqueeze(-1)

        zero_aux = torch.zeros_like(input_image_matrix)
        input_image_complex = torch.cat((input_image_matrix, zero_aux), -1)

        vec_r_1 = torch.from_numpy(vec_r[delta_vox_indx[0]][..., np.newaxis])
        vec_r_2 = torch.from_numpy(vec_r[delta_vox_indx[1]][..., np.newaxis])

        zero_vec_aux = torch.zeros_like(vec_r_1)
        vec_r_complex_a = torch.cat((vec_r_1, zero_vec_aux), -1)
        vec_r_complex_b = torch.cat((vec_r_2, zero_vec_aux), -1)

        fft_data_a = torch.transpose(
            torch.fft(input_image_complex, img_dim), 0, 1)

        phase_a = -1 * (2 * math.pi * ang_param[0] * K[delta_vox_indx[0]])

        phase_a_matrix = torch.from_numpy(phase_a).unsqueeze(-1)
        real_arg_a = torch.zeros_like(phase_a_matrix)
        phase_a_complex = torch.cat((real_arg_a, phase_a_matrix), -1).squeeze()
        vec_r_complex_a_th = (torch.transpose(vec_r_complex_a, 0, 1)).squeeze()

        shear_fft_data_a, phase_a_exp = self.__perform_complex_multiplication__(
            vec_r_complex_a_th, phase_a_complex, fft_data_a, "a")
        G_a = torch.transpose(torch.ifft(torch.transpose(
            shear_fft_data_a, 0, 1), img_dim), 0, 1)

        fft_data_b = torch.fft(G_a, img_dim)
        phase_b = -1 * \
            (2 * math.pi * (ang_param[1]) * np.transpose(K[delta_vox_indx[1]]))
        phase_b_matrix = torch.from_numpy(phase_b).unsqueeze(-1)
        real_arg_b = torch.zeros_like(phase_b_matrix)
        phase_b_complex = torch.cat((real_arg_b, phase_b_matrix), -1).squeeze()
        vec_r_complex_b_th = (torch.transpose(vec_r_complex_b, 0, 1)).squeeze()

        shear_fft_data_b, _ = self.__perform_complex_multiplication__(
            phase_b_complex, vec_r_complex_b_th, fft_data_b, "b")

        G_b = torch.transpose(torch.ifft(shear_fft_data_b, img_dim), 0, 1)
        fft_data_c = torch.transpose(torch.fft(G_b, img_dim), 0, 1)

        shear_fft_data_c, _ = self.__perform_complex_multiplication__(
            phase_a_exp, None, fft_data_c, "c")
        G_c = torch.ifft(torch.transpose(shear_fft_data_c, 0, 1), img_dim)
        G_c_real = G_c[..., 0]

        return G_c_real

    def __perform_complex_multiplication__(self, data_a, data_b, fft_data_, dimension):
        if not len(np.shape(fft_data_)) > 3:
            fft_data = fft_data_.unsqueeze(-2)
            data_a_sq = data_a.unsqueeze(-2)
        else:
            fft_data = fft_data_
            data_a_sq = data_a

        phase_mult = torch.zeros_like(fft_data)
        phase_exp = torch.zeros_like(fft_data)
        shear_fft_data = torch.zeros_like(fft_data)

        if not dimension == "c":
            for rx in range(np.shape(data_a)[0]):
                for ry in range(np.shape(data_b)[0]):
                    for rz in range(np.shape(fft_data)[2]):
                        phase_mult[rx, ry, rz, 0] = data_a[rx, 0] * \
                            data_b[ry, 0] - data_a[rx, 1] * data_b[ry, 1]
                        phase_mult[rx, ry, rz, 1] = data_a[rx, 0] * \
                            data_b[ry, 1] + data_a[rx, 1] * data_b[ry, 0]

                        phase_exp[rx, ry, rz, 0] = torch.exp(
                            phase_mult[rx, ry, rz, 0])*(torch.cos(phase_mult[rx, ry, rz, 1]))
                        phase_exp[rx, ry, rz, 1] = torch.exp(
                            phase_mult[rx, ry, rz, 0])*(torch.sin(phase_mult[rx, ry, rz, 1]))

                        shear_fft_data[rx, ry, rz, 0] = phase_exp[rx, ry, rz, 0]*fft_data[rx,
                                                                                          ry, rz, 0] - phase_exp[rx, ry, rz, 1]*fft_data[rx, ry, rz, 1]
                        shear_fft_data[rx, ry, rz, 1] = phase_exp[rx, ry, rz, 0]*fft_data[rx,
                                                                                          ry, rz, 1] + phase_exp[rx, ry, rz, 1]*fft_data[rx, ry, rz, 0]

        else:
            for rx in range(np.shape(data_a)[0]):
                for ry in range(np.shape(data_a)[1]):
                    for rz in range(np.shape(fft_data)[2]):
                        shear_fft_data[rx, ry, rz, 0] = data_a_sq[rx, ry, rz, 0]*fft_data[rx,
                                                                                          ry, rz, 0] - data_a_sq[rx, ry, rz, 1]*fft_data[rx, ry, rz, 1]
                        shear_fft_data[rx, ry, rz, 1] = data_a_sq[rx, ry, rz, 0]*fft_data[rx,
                                                                                          ry, rz, 1] + data_a_sq[rx, ry, rz, 1]*fft_data[rx, ry, rz, 0]

        shear_fft_data = shear_fft_data.squeeze()
        phase_exp = phase_exp.squeeze()

        return shear_fft_data, phase_exp

    def __get_angle_params__(self, delta_vox_indx, theta):
        arg_neg = 1 / 2
        math_fun = [math.tan, math.sin]
        arg_angle = []
        delta_arg = [self.voxel_size[delta_vox_indx[0]],
                     self.voxel_size[delta_vox_indx[1]]]

        for plane_rot in range(2):
            ang = arg_neg * (delta_arg[0] / delta_arg[1]) * \
                math_fun[plane_rot](math.radians(theta))
            arg_angle.append(ang)
            arg_neg = arg_neg * (-2)
            delta_arg = np.roll(delta_arg, 1)

        return arg_angle
