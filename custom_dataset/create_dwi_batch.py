from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from generate_dwi import generate_dwi
from utilities import convert_b_format

import numpy as np
import os
import random
import subprocess


def convert_quaternion_to_rotation(quaternion):
    a = quaternion[0]
    b = quaternion[1]
    c = quaternion[2]
    d = quaternion[3]
    rotation_matrix = [[a**2+b**2-c**2-d**2,  2*b*c - 2*a*d,       2*b*d+2*a*c],
                       [2*b*c+2*a*d,          a**2-b**2+c**2-d**2, 2*c*d-2*a*b],
                       [2*b*d-2*a*c,          2*c*d-2*a*b,         a**2-b**2-c**2+d**2]]
    return rotation_matrix


def get_random_quaternion_rotation():
    # Generate random 4d vector
    vec_q = np.random.uniform(-np.math.pi,np.math.pi,4)
    # Normalize vector
    quaternion = vec_q/np.linalg.norm(vec_q)
    rotation_matrix = convert_quaternion_to_rotation(quaternion)
    return rotation_matrix


def rotate_bvecs_quaternion(gradient_loc):
    data = np.genfromtxt(gradient_loc)
    vec = data[:,:3]
    rotation_matrix = get_random_quaternion_rotation()

    rotated_vec = vec.copy()
    for i, vec_ in enumerate(vec):
        rot_vec = np.dot(rotation_matrix, vec_)
        rotated_vec[i,:] = rot_vec/np.linalg.norm(rot_vec)
    
    return rotated_vec


def save_bvecs(rotated_vec, sample):
    bvec_loc = "../data/training/training_bvecs_bvals/quaternion_rotation/temp_bvec.txt"
    bval_loc = "../data/training/training_bvecs_bvals/quaternion_rotation/temp_bval.txt"
    new_grad_path = "../data/training/training_bvecs_bvals/quaternion_rotation_norm/sample_ " + str(sample) + "_rotated_grad.txt"

    b_vecs_file = open(bvec_loc, 'w')
    b_vals_file = open(bval_loc, 'w')
    for b_index, bvec in enumerate(rotated_vec):
        if b_index==0:
            str_1 = "0"
            str_2 = "0"
            str_3 = "0"
            bval_str = "0"
        elif (b_index % 9 == 0):
            # str_1 += " " + str(bvec[0])
            # str_2 += " " + str(bvec[1])
            # str_3 += " " + str(bvec[2])
            # bval_str += " " + "1000"
            # Include a b=0 image
            str_1 += " " + "0"
            str_2 += " " + "0"
            str_3 += " " + "0"
            bval_str += " " + "0"
        else:
            str_1 += " " + str(bvec[0])
            str_2 += " " + str(bvec[1])
            str_3 += " " + str(bvec[2])
            bval_str += " " + "1000"

    str_ = str_1 + "\n" + str_2 + "\n" + str_3
    b_vecs_file.write(str_)
    b_vecs_file.close()
    b_vals_file.write(bval_str)
    b_vals_file.close()

    num_images = str_1.split()
    convert_b_format(bval_loc, bvec_loc, new_grad_path)
    return new_grad_path, len(num_images)


if __name__ == '__main__':

    use_existing_gtables = True


    n_fa_vols = 40
    n_d_vols = 40
    base_loc= "../data/training/"
    params_loc= "../data/training/parameters/isotropic_regions/"
    simulator= "../phantom_simulation/nfg_1.1.1_s0_noBackground/bin/mri_sim"
    save=True

    dest_experiment = "neuroRIM_noBackground_correct_bvec_rotation"
    experiment = "neuroRIM"
    exp = 1
    num_voxels = 50
    gradient_loc="../data/training/training_bvecs_bvals/quaternion_rotation_norm/"
    grad_dir_list = [f for f in os.listdir(gradient_loc) if f.endswith("txt")]
    # rot_bvec_loc = gradient_loc+"/sub-0001_dwi_sub-0001_run-1_dwi.bvec"



    sample = 0
    for f in range(n_fa_vols):
        for d in range(n_d_vols):

            gradient_file = random.sample(grad_dir_list, 1)

            fa_values = np.random.uniform(0.05, 1.0)
            d_values = np.random.uniform(0.05, 1.2)/1000
            iso_d_values = np.random.uniform(0.8, 4.0)/1000
            pd_values = np.random.uniform(0.1, 5.0)
            z_slice = int(np.random.uniform(5,num_voxels-5))

            # rotated_bvecs = rotate_bvecs_quaternion(gradient_loc+"/"+gradient_file[0])
            # gradient_loc_, len_bvecs = save_bvecs(rotated_bvecs, sample)

            if not use_existing_gtables:
                gradient_loc_, rx, ry, rz, len_bvecs = rotate_and_save_bvecs(gradient_loc+"/"+gradient_file[0])
                pickleFile = "dwi_sample_"+ str(exp) +"_fa_"+str(fa_values)[:6]+"_d_"+str(d_values)[:8]+"_isod_"+str(iso_d_values)[:8]+"_pd_"+str(pd_values)[:6]+"_z_"+str(z_slice)+"_g"+str(len_bvecs)

                associated_gradient_file = pickleFile + "_grad_dir.txt"
                dest_loc = base_loc + "9_mri_sim_py/"+ dest_experiment + "/" + associated_gradient_file
                subprocess.run(['cp', gradient_loc_, dest_loc])

            else:
                indx = int(np.random.uniform(0,len(grad_dir_list)))
                gradient_loc_ = gradient_loc+grad_dir_list[indx]
                pickleFile = "dwi_sample_"+ str(exp) +"_fa_"+str(fa_values)[:6]+"_d_"+str(d_values)[:8]+"_isod_"+str(iso_d_values)[:8]+"_pd_"+str(pd_values)[:6]+"_z_"+str(z_slice)
                
                associated_gradient_file = pickleFile + "_grad_dir.txt"
                dest_loc = base_loc + "9_mri_sim_py/"+ dest_experiment + "/" + associated_gradient_file
                subprocess.run(['cp', gradient_loc_, dest_loc])

            print("Generating sample {}/{}".format(sample, n_d_vols*n_fa_vols))

            override_parameters = {"fa":fa_values, "voxel_size":0.08, "subvoxel_size":0.004, "diffusivity":d_values, "pd":pd_values, "num_voxels":num_voxels, "isotropic_diffusivity":iso_d_values, "z_slice":z_slice, "sphere_r":2}
            generate_dwi(base_loc, params_loc, gradient_loc_, experiment, dest_experiment, simulator, override_parameters, save, pickleFile)

            sample += 1
            
