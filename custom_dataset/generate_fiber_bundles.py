from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import utilities

import logging
import os
import sys
import subprocess


def generate_fiber_bundle(argv):
    base_folder = '/home/emanoel/Documents/EMC/development/DTI_RIM/data/training/'
    loc_parameter_files = '/home/emanoel/Documents/EMC/development/DTI_RIM/data/training/parameters/isotropic_regions/'
    experiment = argv[2]

    if argv[1]=='clean':
        clean_folder_structure(base_folder, experiment)

    elif argv[1]=='create':
        prepare_folder_structure(base_folder, experiment)

        param_overrides = {"sphere_r": 1.5, "control_point_freq": 50}
        run_1_rand_init(base_folder, experiment, loc_parameter_files, param_overrides)

        param_overrides = {"fov": 1.6, "grid_reference_size": 10, "max_iterations": 200}
        run_2_optimise(base_folder, experiment, loc_parameter_files, param_overrides)

        param_overrides = {"new_sphere_r": 1.25}
        run_3_trim(base_folder, experiment, loc_parameter_files, param_overrides)

        param_overrides = {}
        run_4_subdiv(base_folder, experiment, loc_parameter_files, param_overrides)

        param_overrides = {}
        run_5_resample(base_folder, experiment, loc_parameter_files, param_overrides)

        param_overrides = {"fov": 1.3, "grid_reference_size": 50, "max_iterations": 200}
        run_6_optimise(base_folder, experiment, loc_parameter_files, param_overrides)

        param_overrides = {"new_sphere_r": 1.0}
        run_7_trim(base_folder, experiment, loc_parameter_files)


def clean_folder_structure(base_folder, experiment):
    operations = ["1_rand_init", "2_optimise", "3_trim", "4_subdiv", "5_resample", "6_optimise", "7_trim", "8_mri_sim", "9_mri_sim_py"]
    for op in operations:
        print(base_folder + op + '/' + experiment)
        dir_ = os.listdir(base_folder + op + '/' + experiment)
        if dir_:
            logging.warning("Removing all files and DIR: {}".format(base_folder + op + '/' + experiment))
            subprocess.run(["rm", "-r", base_folder + op + '/' + experiment])
        else:
            logging.warning("Removing DIR: {}".format(base_folder + op + '/' + experiment))
            subprocess.run(["rmdir", base_folder + op + '/' + experiment])


def prepare_folder_structure(base_folder, experiment):
    operations = ["1_rand_init", "2_optimise", "3_trim", "4_subdiv", "5_resample", "6_optimise", "7_trim", "8_mri_sim", "9_mri_sim_py"]
    for op in operations:
        subprocess.run(["mkdir", base_folder + op])
        subprocess.run(["mkdir", base_folder + op + "/" + experiment])
        logging.warning("Creating DIR: {}".format(base_folder + op + '/' + experiment))


def run_1_rand_init(base_folder, experiment, loc_parameter_files, param_overrides = {}):
    output_dir = base_folder + "1_rand_init/" + experiment
    loc_parameter_file = loc_parameter_files + "1_rand_init_param.txt"

    if param_overrides:
        utilities.override_param_file(loc_parameter_file, param_overrides)

    logging.warning("Starting to run random initialization of fibers")
    subprocess.run(["../phantom_simulation/nfg_1.1.1_s0/bin/rand_init", output_dir, loc_parameter_file])
    logging.warning("Saved strand file to {}".format(output_dir))


def run_2_optimise(base_folder, experiment, loc_parameter_files, param_overrides = {}):
    input_dir = base_folder + "1_rand_init/" + experiment
    output_dir = base_folder + "2_optimise/" + experiment
    loc_parameter_file = loc_parameter_files + "2_optimise_param.txt"

    if param_overrides:
        utilities.override_param_file(loc_parameter_file, param_overrides)

    logging.warning("Starting to run optimisation")
    subprocess.run(["../phantom_simulation/nfg_1.1.1_s0/bin/optimise", input_dir, output_dir, loc_parameter_file])
    logging.warning("Saved optimised strands to {}".format(output_dir))


def run_3_trim(base_folder, experiment, loc_parameter_files, param_overrides = {}):
    input_dir = base_folder + "2_optimise/" + experiment
    output_dir = base_folder + "3_trim/" + experiment
    loc_parameter_file = loc_parameter_files + "3_trim_param.txt"

    if param_overrides:
        utilities.override_param_file(loc_parameter_file, param_overrides)

    logging.warning("Starting to trim")
    subprocess.run(["../phantom_simulation/nfg_1.1.1_s0/bin/trim", input_dir, output_dir, loc_parameter_file])
    logging.warning("Saved trimmed strands to {}".format(output_dir))


def run_4_subdiv(base_folder, experiment, loc_parameter_files, param_overrides = {}):
    input_dir = base_folder + "3_trim/" + experiment
    output_dir = base_folder + "4_subdiv/" + experiment
    loc_parameter_file = loc_parameter_files + "4_subdiv_param.txt"

    if param_overrides:
        utilities.override_param_file(loc_parameter_file, param_overrides)

    logging.warning("Starting to subdiv")
    subprocess.run(["../phantom_simulation/nfg_1.1.1_s0/bin/subdiv", input_dir, output_dir, loc_parameter_file])
    logging.warning("Saved subdivided strands to {}".format(output_dir))


def run_5_resample(base_folder, experiment, loc_parameter_files, param_overrides = {}):
    input_dir = base_folder + "4_subdiv/" + experiment
    output_dir = base_folder + "5_resample/" + experiment
    loc_parameter_file = loc_parameter_files + "5_resample_param.txt"

    if param_overrides:
        utilities.override_param_file(loc_parameter_file, param_overrides)

    logging.warning("Starting to resample")
    subprocess.run(["../phantom_simulation/nfg_1.1.1_s0/bin/resample", input_dir, output_dir, loc_parameter_file])
    logging.warning("Saved resampled strands to {}".format(output_dir))


def run_6_optimise(base_folder, experiment, loc_parameter_files, param_overrides = {}):
    input_dir = base_folder + "5_resample/" + experiment
    output_dir = base_folder + "6_optimise/" + experiment
    loc_parameter_file = loc_parameter_files + "6_optimise_param.txt"

    if param_overrides:
        utilities.override_param_file(loc_parameter_file, param_overrides)

    logging.warning("Starting to optimise")
    subprocess.run(["../phantom_simulation/nfg_1.1.1_s0/bin/optimise", input_dir, output_dir, loc_parameter_file])
    logging.warning("Saved optimised strands to {}".format(output_dir))


def run_7_trim(base_folder, experiment, loc_parameter_files, param_overrides = {}):
    input_dir = base_folder + "6_optimise/" + experiment
    output_dir = base_folder + "7_trim/" + experiment
    loc_parameter_file = loc_parameter_files + "7_trim_param.txt"

    if param_overrides:
        utilities.override_param_file(loc_parameter_file, param_overrides)

    logging.warning("Starting to trim")
    subprocess.run(["../phantom_simulation/nfg_1.1.1_s0/bin/trim", input_dir, output_dir, loc_parameter_file])
    logging.warning("Saved trimmed strands to {}".format(output_dir))



if __name__ == '__main__':
    argv = sys.argv
    generate_fiber_bundle(argv)
