from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from .utilities import open_pickle_file, open_nii_file, compute_std_from_s0

from dipy.denoise.pca_noise_estimate import pca_noise_estimate
import dipy.denoise.noise_estimate as ne
from EMCqMRI.core.base import base_dataset
from EMCqMRI.core.utilities import dataset_utilities
from EMCqMRI.core.utilities import image_utilities
import logging
import numpy as np
import pickle
import os
from scipy.stats import loguniform
import torch
from torchvision.transforms import CenterCrop

class DatasetModel(base_dataset.Dataset):
    def __init__(self, configObject):
        super(DatasetModel, self).__init__(configObject)
        self.__name__ = 'DTI Dataset'
        self.args = configObject.args
        self.data = []
        self.idx_control = -1
        self.normal_distr_noise = torch.distributions.uniform.Uniform(0.001, 0.05)
        self.normal_distr_snr = torch.distributions.uniform.Uniform(5, 100)
        self.uniform_distr_rotation = torch.distributions.uniform.Uniform(0, 0.25)
        self.uniform_distr_translation = torch.distributions.uniform.Uniform(0, 0.25)

    def get_dwi_data(self, filename):
        if filename.endswith('.pkl'):
            print(filename)
            dwi_data = open_pickle_file(self.path, filename)

        elif filename.endswith('.nii') or filename.endswith('.nii.gz'):
            dwi_data = open_nii_file(self.path, filename)

        if isinstance(dwi_data, dict): # The only case when this happens is during evaluation with simulated data.
            self.data_is_dict = True
            dwi_signal = dwi_data['image']
            dwi_label = dwi_data['label']
        else:
            dwi_signal = dwi_data
            dwi_label = dwi_data.copy()

        if self.args.dataset.slice_data:
            print(dwi_signal.shape)
            slice_index = self.args.dataset.z_slice if self.args.dataset.z_slice>0 else self.get_z_index(dwi_data) 
            dwi_signal = dwi_signal[:, :, :, slice_index]
            dwi_label = dwi_label[:, :, :, slice_index]
        
        self.dwi_signal = torch.abs(torch.Tensor(dwi_signal).to(device=self.args.engine.device).float()/1)
        self.dwi_label = torch.Tensor(dwi_label).to(device=self.args.engine.device).float()/1


    def index_files(self):
        if self.args.engine.mode == 'training':
            file_list = [f for f in os.listdir(self.path) if ("grad_dir") not in f and (f.endswith('.pkl')) or (f.endswith('.nii'))]
        elif self.args.engine.mode == 'testing':
            file_list = [f for f in os.listdir(self.path) if ((f.endswith('.pkl')) or (f.endswith('.nii')) or (f.endswith('.nii.gz'))) and ('mask' not in f)]
        self.filename_list = [str(self.path) + '/' + file_ for file_ in file_list]
        
    def create_grid(self, image_data):
        channels = len(image_data)
        size_data = [channels, 1, *image_data[0].shape]
        grid_per_patch = []
        affine_per_patch = []

        self.args.dataset.pngr_motion = dataset_utilities.get_rand_seed(False)
        pixel_translation = self.args.dataset.pngr_motion.normal(0, next(self.get_std_translation()), [channels, 2])
        self.args.dataset.pngr_motion = dataset_utilities.get_rand_seed(False)
        angle_rotation = self.args.dataset.pngr_motion.normal(0, next(self.get_std_rotation()), [channels])
        zero_mean_rotation = angle_rotation - np.mean(angle_rotation)
        zero_mean_px_translation = np.zeros_like(pixel_translation)
        for c in range(2):
            zero_mean_px_translation[:,c] = pixel_translation[:,c] - np.mean(pixel_translation[:,c])

        affine_parameters_natural_units_ = np.zeros([channels,3])
        affine_parameters_natural_units_[:,0] = zero_mean_rotation
        affine_parameters_natural_units_[:,1:] = zero_mean_px_translation
        affine_parameters_natural_units = torch.from_numpy(affine_parameters_natural_units_).to(device=self.args.engine.device)
        grid = self.args.engine.motion_operator.convertAffineToGrid(image_data, affine_parameters_natural_units)

        grid_per_patch = grid.squeeze(0)
        affine_per_patch = affine_parameters_natural_units

        return affine_per_patch, grid_per_patch

    def apply_deformation(self, images, motion_kappa):
        deformed_data_list = []
        for t, image in enumerate(images):
            deformed_data = self.args.engine.motion_operator.apply_deformation(image, motion_kappa[t])
            deformed_data_list.append(deformed_data)
        deformed_data_ = torch.stack(deformed_data_list)
        return torch.abs(deformed_data_.squeeze())

    def get_z_index(self, data):
        index = np.nonzero(data)
        return index[-1][0]

    def get_random_snr(self):
        while True:
            yield torch.abs(self.normal_distr_snr.sample())

    def get_std_rotation(self):
        while True:
            if self.args.dataset.stdRotation>=0:
                yield self.args.dataset.stdRotation
            else:
                yield self.uniform_distr_rotation.sample()

    def get_std_translation(self):
        while True:
            if self.args.dataset.stdTranslation>=0:
                yield self.args.dataset.stdTranslation
            else:
                yield self.uniform_distr_translation.sample()

    def normalize_input_data(self, data):
        return data/self.args.dataset.noise_std

    def denormalize_data(self, data):
        return data*self.args.dataset.noise_std

    def get_snr_data(self, data):
        noise_std = compute_std_from_s0(data)
        snr = data.mean()/noise_std
        return snr

    def get_signal(self, idx):
        self.filename_sample = self.filename_list[idx]
        self.args.dataset.inst_filename_ = self.filename_sample
        self.get_dwi_data(self.filename_sample) # Gives access to self.dwi_signal and self.dwi_label


        if self.args.dataset.applyMotion:
            affine_parameters, deformation_field = self.create_grid(self.dwi_signal)
            motion_maps = deformation_field.view([len(self.dwi_signal), 2, *self.dwi_signal[0].shape]).movedim(1,-1)
            self.dwi_signal = self.apply_deformation(self.dwi_signal, motion_maps)

        if self.args.dataset.resizeFactor != 1:
            self.dwi_signal = self.args.engine.motion_operator.undersample(self.dwi_signal, self.args.dataset.resizeFactor)

        if self.args.dataset.cropSize > 0:
            self.dwi_signal = CenterCrop(size=self.dwi_signal.shape[-1] - self.args.dataset.cropSize)(self.dwi_signal)

        # print(self.dwi_signal)
        if self.args.engine.mode == 'training':
            snr = next(self.get_random_snr())
            noise_std = self.dwi_signal[0].mean()/snr

        elif self.args.engine.mode == 'testing':
            if self.args.dataset.SNR > 0:
                snr = self.args.dataset.SNR
            else:
                snr = self.get_snr_data(self.dwi_signal[0])  # Should use a better way to estimate sigma
            noise_std = (self.dwi_signal[0].mean())/(snr)
            
        self.args.dataset.noise_std = torch.Tensor([noise_std]).to(device=self.args.engine.device)

        if self.args.dataset.applyNoise:
            self.dwi_signal = self.args.engine.likelihood_model.apply_noise(self.dwi_signal, noise_std)

        if self.args.dataset.normalize:
            self.dwi_signal = self.normalize_input_data(self.dwi_signal)
                        
        self.dwi_signal[self.dwi_signal<=0.01] = 0.01
        # image_utilities.imagebrowse_slider(self.dwi_signal/1, vmax_=2)
        return self.dwi_signal.float()

    def get_label(self, idx):
        if self.args.dataset.resizeFactor != 1:
            self.dwi_label = self.args.engine.motion_operator.undersample(self.dwi_label, self.args.dataset.resizeFactor)

        if self.args.dataset.cropSize > 0:
            self.dwi_label = CenterCrop(size=self.dwi_label.shape[-1] - self.args.dataset.cropSize)(self.dwi_label)
        
        mask = []
        self.dwi_label[self.dwi_label<=0] = 0.0001
        return self.dwi_label, mask
