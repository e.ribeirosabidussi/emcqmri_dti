from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from utilities import override_param_file

import logging
from medpy.io import load
import numpy as np
import os
import pickle
import subprocess
import sys

sys.path.insert(0, "..")

def generate_dwi(base_loc, params_loc, gradient_loc, experiment, dest_experiment, simulator, override_parameters, save, pickleFile):
    base_folder = base_loc #
    loc_parameter_files = params_loc
    loc_gradients = gradient_loc
    experiment = experiment

    call_mri_sim(base_folder, experiment, dest_experiment, loc_gradients, loc_parameter_files, simulator, override_parameters)
    dwi_np, tensor_np = convert_mri_sim_numpy(base_folder, dest_experiment)

    if save:
        convert_mri_sim_pickle(base_folder, dest_experiment, pickleFile, dwi_np, tensor_np)

    return dwi_np

def call_mri_sim(base_folder, experiment, dest_experiment, loc_gradient_file, loc_parameter_files, simulator, param_overrides = {}):
    input_dir = base_folder + "7_trim/" + experiment
    output_dir = base_folder + "8_mri_sim/" + dest_experiment
    subprocess.run(['rm', '-r', output_dir + "/"])
    subprocess.run(['mkdir', output_dir])
    loc_parameter_file = loc_parameter_files + "8_mri_sim_param.txt"

    if param_overrides:
        override_param_file(loc_parameter_file, param_overrides)

    logging.warning("Starting to generate simulated dwi")
    subprocess.run([simulator, input_dir, output_dir, loc_gradient_file, loc_parameter_file])
    logging.warning("Saved Simulated DWI to {}".format(output_dir))


def convert_mri_sim_numpy(base_folder, experiment):
    input_dir = base_folder + "8_mri_sim/" + experiment
    output_dir = base_folder + "9_mri_sim_py/" + experiment
    file_list = np.sort([f for f in os.listdir(input_dir) if f.endswith(".img") and f.startswith("dwi")])

    dwi_data = []
    for file in file_list:
        data, header = load(input_dir + "/" + file)
        dwi_data.append(data)
    dwi_data_np = np.array(dwi_data)

    file_list = np.sort([f for f in os.listdir(input_dir) if f.endswith(".img") and f.startswith("tensor")])

    tensor_data = []
    for file in file_list:
        data, header = load(input_dir + "/" + file)
        tensor_data.append(data)
    tensor_data_np = np.array(tensor_data)

    return dwi_data_np, tensor_data_np


def convert_mri_sim_pickle(base_folder, experiment, pickleFile, dwi_data_np, tensor_data_np):
    input_dir = base_folder + "8_mri_sim/" + experiment
    output_dir = base_folder + "9_mri_sim_py/" + experiment

    with open(output_dir + "/" + pickleFile + ".pkl", "wb") as file:
        pickle.dump(dwi_data_np, file)
        print("Saved file")


if __name__ == '__main__':
    argv = sys.argv

    base_loc= "../data/training_data/"
    params_loc= "../phantom_simulator/python_generated/parameters/"
    gradient_loc= "../phantom_simulator/python_generated/gradient_directions/encoding_dir33-b1000.txt"
    experiment= argv[1]
    simulator= "../phantom_simulator/nfg_1.1.1/bin/mri_sim"
    override_parameters= {}
    save=True
    pickleFile= argv[2]
    override_parameters = {"fa":float(argv[3]), "diffusivity":float(argv[4]), "num_voxels":int(argv[5])}
    generate_dwi(base_loc, params_loc, gradient_loc, experiment, simulator, override_parameters, save, pickleFile)
