
import subprocess
import pickle
import nibabel as nii
import numpy as np
import os

def override_param_file(loc_parameter_file, override_param = {}):
    for key, value in override_param.items():
        loc_parameter_file_clone = loc_parameter_file[:-4] + "_clone.txt"
        subprocess.run(["cp", loc_parameter_file, loc_parameter_file_clone])

        filedata = open(loc_parameter_file, 'w')
        filedata_clone = open(loc_parameter_file_clone, 'r')

        for line in filedata_clone:
            if line.find(key)==0:
                line_split = line.split()
                modified_line_split = line_split
                modified_line_split[1] = str(value)
                modified_line_join = ' '.join(modified_line_split)
                filedata.write(line.replace(line, modified_line_join + '\n'))
            else:
                filedata.write(line)

        filedata.close()
        filedata_clone.close()
        subprocess.run(["rm", loc_parameter_file_clone])


def save_nii_file(data, path, filename):
    nii_data = nii.Nifti1Image(data, affine=np.eye(4))
    try:
        nii.save(nii_data, os.path.join(path, filename)) 
        print("NII file save succesfully")
    except:
        print("Problems when saving NII file.")


def open_nii_file(base_folder, filename):
    file_ = os.path.join(base_folder, filename)
    img = nii.load(file_)
    dwi_data = np.asarray(img.dataobj).squeeze()
    dwi_data = np.moveaxis(dwi_data, -1, 0)
    return dwi_data


def save_pickle_file(data, base_folder, filename):
    file_ = base_folder + filename
    with open(file_, 'wb') as f:
        pickle.dump(data, f)

def open_pickle_file(base_folder, filename):
    file_ = os.path.join(base_folder, filename)
    with open(file_, 'rb') as f:
        dwi_data = pickle.load(f)
    return dwi_data

def open_nii_dwi_grad(base_folder, filename, grad_file):
    file_ = os.path.join(base_folder, filename)
    img = nii.load(file_)
    dwi_data = img.get_fdata()
    dwi_data = np.moveaxis(dwi_data, -1, 0)
    b_vals, b_vecs = getGradientDirectionsBvalue(grad_file)
    return dwi_data, b_vals, b_vecs


def getGradientDirectionsBvalue(path):
    gradFile = open(path, 'r')
    b_vals = []
    b_vecs = []
    for line in gradFile:
        line_split = line.split()
        b_val = float(line_split[-1])
        grad_direction = np.array([float(line_split[0]), float(line_split[1]), float(line_split[2])])
        b_vals.append(b_val/1000)
        b_vecs.append(grad_direction)
    return b_vals, b_vecs

def get_bvals_bvecs(path_bvals, path_bvecs):
    b_vals_file = open(path_bvals, 'r')
    b_vecs_file = open(path_bvecs, 'r')

    if 0:
        b_vals = [int(float(line))/1 for line in b_vals_file]
    else:
        for line in b_vals_file:
            line_split = line.split()
            b_vals = [float(bv)/1 for bv in line_split]
            print("B: ", b_vals)
    b_vecs = []
    lines_ = []
    for line in b_vecs_file:
        line_split = line.split()
        lines_.append([float(bv) for bv in line_split])
    for b1, b2, b3 in zip(lines_[0],lines_[1],lines_[2]):
        b_vecs.append([b1,b2,b3])

    return b_vals, b_vecs


def save_gradient_info(bvec, bval, new_gradient_file):
    new_grad_file = open(new_gradient_file, 'w')
    for bval_, bvec_ in zip(bval, bvec):
        blist = list(bvec_) + [(bval_*1000)]
        str_ = ""
        for el in blist:
            str_ += (" " + str(el))
        str_ += "\n"
        new_grad_file.write(str_)
    new_grad_file.close()


def compute_std_from_s0(data):
    background_patch = data
    mean_intensity = background_patch.mean()
    std_intensity = background_patch.std()

    return std_intensity
    

def convert_b_format(path_bvals, path_bvecs, new_gradient_file):
    b_vals, b_vecs = get_bvals_bvecs(path_bvals, path_bvecs)
    b_val_b_vec = []
    new_file = open(new_gradient_file, 'w')
    for bval, bvec in zip(b_vals, b_vecs):
        blist = bvec + [int(bval)*1]
        str_ = ""
        for el in blist:
            str_ += (" " + str(el))
        str_ += "\n"
        new_file.write(str_)

    new_file.close()
    return b_vals, b_vecs
